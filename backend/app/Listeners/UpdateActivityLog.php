<?php

namespace App\Listeners;

use App\Events\UpdateData;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateActivityLog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateData  $event
     * @return void
     */
    public function handle(UpdateData $event)
    {
    }
	
	public function runLog(UpdateData $event){
		activity(strtolower(class_basename($event->model)))->log($event->log['desc'].' (ID: '.$event->model->id.')');
	}
	
	public function subscribe($events)
    {
        $events->listen(
            'App\Events\UpdateData',
            'App\Listeners\UpdateActivityLog@runLog'
        );
    }
}
