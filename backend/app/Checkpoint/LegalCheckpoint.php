<?php
namespace App\Checkpoint;
use Cartalyst\Sentinel\Checkpoints\CheckpointInterface;
use Cartalyst\Sentinel\Users\UserInterface;

class LegalCheckpoint implements CheckpointInterface
{
    public function login(UserInterface $user)
    {
        //
    }

    public function fail(UserInterface $user = null)
    {
        //
    }

    public function check(UserInterface $user)
    {
        return $this->checkLegality($user);
    }
	
	protected function checkLegality(UserInterface $user){
        if ($user->status == \App\User::BANNED) {
            $exception = new \App\Exceptions\LegalityException('Your account has been banned.');
            $exception->setUser($user);

            throw $exception;
        }else if ($user->status == \App\User::SUSPENDED) {
            $exception = new \App\Exceptions\LegalityException('Your account has been suspended.');
            $exception->setUser($user);

            throw $exception;
        }else if ($user->status == \App\User::PENDING) {
            $exception = new \App\Exceptions\LegalityException('Your account has a pending validation.');
            $exception->setUser($user);

            throw $exception;
        }else if ($user->status == \App\User::INACTIVE) {
            $exception = new \App\Exceptions\LegalityException('Your account has been inactive.');
            $exception->setUser($user);

            throw $exception;
        }
	}
}