<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Chrisbjr\ApiGuard;
use Log;
use ApiGuardAuth;
use Chrisbjr\ApiGuard\Repositories\ApiKeyRepository;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = array('status' => 'error', 'code' => 405, 'data' => 'Not Allowed!');
        try{
            $key = $request->header(config('apiguard.keyName', 'X-Authorization'));

            if (empty($key)) {
                // Try getting the key from elsewhere
                $key = $request->get(config('apiguard.keyName', 'X-Authorization'));
            }
            if($key){
                $apiKeyModel = \App::make(config('apiguard.models.apiKey', 'Chrisbjr\ApiGuard\Models\ApiKey'));

                if ( ! $apiKeyModel instanceof ApiKeyRepository) {
                    Log::error('[ApiGuard] Your ApiKey model should be an instance of ApiKeyRepository.');
                    throw new \Exception("You ApiKey model should be an instance of ApiKeyRepository.");
                }

                $apiKey = $apiKeyModel->getByKey($key);
                if($apiKey){
                    $user = ApiGuardAuth::authenticate($apiKey);
                    if($user){
                        $user = \Sentinel::findById($user->id);
                        $test = \Sentinel::login($user);
                        \Auth::loginUsingId($user->id);
                    }
                }
            }


        }catch(\Exception $e){
            $response['code'] = 401;
            $response['data'] = $e->getMessage();
            return \Response::json($response);
        }


        return $next($request);
    }
}