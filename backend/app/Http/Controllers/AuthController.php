<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Sentinel;

class AuthController extends Controller
{
	public function __construct(){
		// $checkpoint = new \App\Checkpoint\LegalCheckpoint;
		// \Sentinel::addCheckpoint('LegalCheckpoint', $checkpoint);
	}
	
    public function login(Request $request){
		$input = $request->all();
		$resp = Sentinel::authenticateAndRemember($input);
		if(!$resp){
			return redirect()->guest('login')->with('error', 'Wrong email/password combination.');
		}else{
			if ($resp->hasAccess(['admin.login'])){
				activity('auth')->log('login admin');
				return redirect()->route('dashboard');
			}else{
				return redirect()->guest('login')->with('error', 'You do not have access to this page.');
			}
		}
	}
	
	public function logout(){
		Sentinel::logout();
		activity('auth')->log('logout admin');
		return redirect()->guest('login')->with('success', "You've successfully logged out.");
	}
}
