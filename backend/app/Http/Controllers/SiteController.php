<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Session;

class SiteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function login(){
		// if(Session::has('error')){
			// dd(Session::get('error'));die;
		// }
        return view('auth.login');
    }
	
    public function dashboard(){
        return view('home');
    }
}
