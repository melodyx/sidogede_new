<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Spatie\Activitylog\Models\Activity;

class ActivityController extends Controller
{
    public function index(){
		$activities = Activity::all();
		return view('activity.index', [
			'activities' => $activities
		]);
	}
	
	public function create(){
		
	}
	
	public function store(){
		
	}
	
	public function show(){
		
	}
	
	public function edit(){
		
	}
	
	public function update(){
		
	}
	
	public function destroy(){
		
	}
}
