<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		/*
		*  Adding Roles
		*/
		$role = Sentinel::getRoleRepository()->createModel()->create([
			'name' => 'Super Admin',
			'slug' => 'superadmin',
		]);
		$role->permissions =[
			'admin.login' => true,
			'module.change' => true,
			'storage.product' => true,
			'storage.supplier' => true,
			'accountants.general' => true,
			'accountants.cashFlow' => true,
			'accountants.accountList' =>true,
			'accountants.create' => true,
		];
		$role->save();
		
		$role = Sentinel::getRoleRepository()->createModel()->create([
			'name' => 'Admin',
			'slug' => 'admin',
		]);
		$role->permissions =[
			'admin.login' => true,
			'module.change' => false,
			'storage.product' => true,
			'storage.supplier' => true,
			'accountants.general' => true,
			'accountants.cashFlow' => true,
			'accountants.accountList' =>true,
			'accountants.create' => true,
		];
		$role->save();
		
		$role = Sentinel::getRoleRepository()->createModel()->create([
			'name' => 'User',
			'slug' => 'user',
		]);
		$role->permissions =[
			'admin.login' => false,
		];
		$role->save();
		
		/*
		*  Adding Users & Roles
		*/
        $user = Sentinel::registerAndActivate(array(
			'email'    => 'hello@eyesimple.us',
			'first_name' => 'EYESIMPLE',
			'password' => 'eyesimple2016',
			'status' => 'ACTIVE',
		));
		$role = Sentinel::findRoleByName('Super Admin');
		$role->users()->attach($user);
		
		/*
		*  Adding Users & Roles
		*/
        $user = Sentinel::registerAndActivate(array(
			'email'    => 'admin@eyesimple.us',
			'first_name' => 'Admin',
			'password' => '123123',
			'status' => 'ACTIVE',
		));
		$role = Sentinel::findRoleByName('Admin');
		$role->users()->attach($user);
		
		/*
		*  Adding Users & Roles
		*/
        $user = Sentinel::registerAndActivate(array(
			'email'    => 'user@eyesimple.us',
			'first_name' => 'User',
			'password' => '123123',
			'status' => 'ACTIVE',
		));
		$role = Sentinel::findRoleByName('User');
		$role->users()->attach($user);
    }
}
