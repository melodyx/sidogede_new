<?php

namespace Modules\Helloworld\Http\Controllers;

use Nwidart\Modules\Routing\Controller;

class HelloWorldController extends Controller {

	public function index()
	{
		return view('helloworld::index');
	}

}
