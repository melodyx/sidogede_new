<?php

namespace Modules\Users\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersDatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		$role = \Sentinel::getRoleRepository()->createModel()->create([
                'name' => 'customer',
                'slug' => 'customer',
        ]);

        $role->permissions =[
			'admin.login' => true,
			'module.change' => false,
			'storage.product' => false,
			'storage.supplier' => false,
			'accountants.general' => false,
			'accountants.cashFlow' => false,
			'accountants.accountList' =>false,
			'accountants.create' => false,
		];
		$role->save();
		// $this->call("OthersTableSeeder");
	}
}
