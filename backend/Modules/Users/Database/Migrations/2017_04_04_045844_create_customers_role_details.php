<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersRoleDetails extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('user_details', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('address',255);
            $table->integer('phone');
            $table->string('image_path',255);
            $table->integer('reward_point');
            $table->string('status',255);
            $table->longtext('note');
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_details');
    }

}
