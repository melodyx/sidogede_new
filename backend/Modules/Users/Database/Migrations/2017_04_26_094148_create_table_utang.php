<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUtang extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table)
        {

            if (!Schema::hasColumn('users', 'piutang'))
            {
                $table->integer('piutang');
            }

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table)
        {


            if (Schema::hasColumn('users', 'piutang'))
            {
                $table->dropColumn('piutang');
            }
        });
    }

}
