@extends('layouts.app')

@section('header')
    @parent
    <!-- JS Extra for form -->
 	
 	<link href="<?php echo asset('assets/css/extras/animate.min.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo Module::asset('transactions:css/jquery-ui.min.css'); ?>" rel="stylesheet" type="text/css">

	<script type="text/javascript" src="<?php echo Module::asset('transactions:js/jquery-ui.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo Module::asset('transactions:js/pages/edit.js'); ?>"></script>

<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
<?php session()->forget('success'); } ?>
 <style>
 	.clear{clear:both}

 </style>
@endsection
@section('content')
<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Customers</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="{{route('users.indexCustomers')}}">Customers</a></li>
							<li class="active">Create</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<div class="row">
						<div class="panel panel-flat">
							<div class="panel-body">
								@include('users::customers._form', ['action' => '\Modules\Users\Http\Controllers\UsersController@storeCustomer','method' => 'post'])
							</div>
						</div>
					</div>
				</div>
@endsection