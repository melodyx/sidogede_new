@extends('layouts.app')
@section('header')

    @parent
	
    <link rel="stylesheet" type="text/css" href="<?php echo Module::asset('transactions:css/colorbox.css')?>">
    <script type="text/javascript" src="<?php echo Module::asset('transactions:js/jquery.colorbox-min.js'); ?>"></script>

	<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
	<?php session()->forget('success'); } ?>
	
	<script>
	jQuery(document).ready(function($){
		$('table.customer').on('click','a.remove',function(e){
			// var rewardId = $(this).data('id');
			// $.ajax({
			// 	method: 'get',
			// 	url: '<?php echo route('dashboard'); ?>/eventpoints/eventrewarddestroy/'+rewardId
			// });
			var customerId = $(this).data('id');
			var confirmation = confirm('Apakah anda yakin ingin melakukan penghapusan customer ini ?');
			if(confirmation == true){
				window.location.replace("{{route('dashboard')}}/users/destroyCustomer/"+customerId);
			}
		});


	});
	</script>

	<style>
		.clear{clear:both;}
		ul.pagination{float:right; display:inline-block; margin-top:30px;}

		.filter-button{float:left; display:inline-block; margin:20px 0 20px 20px;}

		.search-section{float:right; margin:20px 20px 20px;}
		.search-section input{margin-left:20px;}
	</style>


@endsection
@section('content')
				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i>Customers</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Customers</li>
						</ul>

					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<table class="table datatable-basic customer">
					<thead>
						<tr>
							<th style="text-align:center;">Id</th>
							<th style="text-align:center;">Customer Name</th>
							<th style="text-align:center;">Address</th>
							<th style="text-align:center;">City</th>
							<th style="text-align:center;">Piutang</th>
							<th style="text-align:center;">Action</th>
						</tr>
					</thead>
					<tbody>	
						<?php if(isset($customers)){ ?>
							<?php foreach($customers as $customer){?>
							<tr>
								<td style="text-align:center;"><?php if(isset($customer->id)){echo $customer->id;}else{ echo '-';  }?></td>
								<td style="text-align:center;"><?php if(isset($customer->name)){echo $customer->name;}else{ echo '-'; }?></td>
								<td style="text-align:center;"><?php if(isset($customer->address)){ echo $customer->address; }else{ echo '-';  }?></td>
								<td style="text-align:center;"><?php if(isset($customer->city)){ echo $customer->city; }else{ echo '-';  }?></td>
								<td style="text-align:center;"><?php if(isset($customer->piutang)){ echo $customer->piutang; }else{ echo '-';  } ?></td>
								<td style="text-align:center;">
									<ul class="icons-list">
										<li >
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-cog7"></i>
												<span class="caret"></span>
											</a>
											<ul class="dropdown-menu dropdown-menu-right">
												<li class="dropdown-header">Options</li>
												<li><a href="{{ route('users.editCustomer',$customer->id) }}"><i class="icon-pencil7"></i>Edit Customer</a></li>
												<li><a class="remove" data-id="<?php echo $customer->id ?>"><i class="icon-pencil7"></i>Delete</a></li>
											</ul>
										</li>
									</ul>
								</td>
							</tr>
							<?php } ?>
						<?php } ?>
					</tbody>
					</table>
					<?php /*<ul class="pagination">
						<?php if(isset($checkOutstanding)){?>
							<li><a href="{{route('transactions.paginationOutstanding',1)}}"><<</a></li>
							<?php if(!isset($currentPage)){ $currentPage = 1; } if($totalPage > 10){$maxPage = $currentPage + 9;}else{$maxPage = $totalPage; } for($x = $currentPage; $x <= $maxPage; $x++){ ?>
							<li><a href="{{route('transactions.paginationOutstanding',$x)}}"><?php echo $x; ?></a></li>
							<?php } ?> 
							<li><a href="{{route('transactions.paginationOutstanding',$totalPage)}}">>></a></li>
						<?php }else{ ?>
							<li><a href="{{route('transactions.pagination',array(1,$type))}}"><<</a></li>
							<?php if(!isset($currentPage)){ $currentPage = 1; } if($totalPage > 10){$maxPage = $currentPage + 9;}else{$maxPage = $totalPage; } for($x = $currentPage; $x <= $maxPage; $x++){ ?>
							<li><a href="{{route('transactions.pagination',array($x,$type))}}"><?php echo $x; ?></a></li>
							<?php } ?> 
							<li><a href="{{route('transactions.pagination',array($totalPage,$type))}}">>></a></li>
						<?php } ?>
					</ul> */?>		
					</div>
				</div>

@endsection