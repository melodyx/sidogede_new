@extends('layouts.app')

@section('header')

    @parent
 <style>
 	.clear{clear:both}
 </style>
@endsection

@section('content')
<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i>Customer</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="{{route('users.indexCustomers')}}">Customers</a></li>
							<li class="active">edit</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<div class="content">
				<div class="panel panel-flat">
				<div class="panel-body">
					@include('users::customers._form', ['action' => array('\Modules\Users\Http\Controllers\UsersController@updateCustomer', $customer->id), 'method' => 'PUT'])
				</div>
				</div>
				</div>
@endsection