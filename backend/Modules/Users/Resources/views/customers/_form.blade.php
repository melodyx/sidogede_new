<?php
	use \Illuminate\Support\Facades\Input;
	if(!isset($method)){
		$method = 'post';
	}

?>
<script type="text/javascript" src="<?php echo asset('assets/js/plugins/forms/styling/uniform.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo asset('assets/js/pages/form_layouts.js') ?>"></script>


{{ Form::open(array('action' => $action, 'method' => $method, 'class' => 'form-horizontal')) }}
<fieldset class="content-group">
							<legend class="text-bold"><?php if(isset($customer)){ echo 'Edit Customer'; }else{ echo 'Create Customer';}?></legend>
								<div class="form-group">
									<label class="control-label col-lg-2">Customer name</label>
									<div class="col-lg-10">
										{{Form::text('name', isset($customer->name)?$customer->name:null, array('class' => 'form-control','required' => 'required'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Address</label>
									<div class="col-lg-10">
										{{Form::text('address', isset($customer->address)?$customer->address:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">City</label>
									<div class="col-lg-10">
										{{Form::text('city', isset($customer->city)?$customer->city:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="text-right">
									<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
								</div>
						</fieldset>
{{ Form::close() }}