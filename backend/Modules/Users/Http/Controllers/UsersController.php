<?php

namespace Modules\Users\Http\Controllers;

use Nwidart\Modules\Routing\Controller;
use Modules\Users\Entities\User;
use Modules\Users\Entities\Customer;
use Modules\Debts\Entities\Achievable;
use Modules\Debts\Entities\Payable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class UsersController extends Controller {

	public function index()
	{
		return view('users::index');
	}

	public function getAllUsers(Request $request){
        $term = $request['term'];
        //$users = User::where('first_name', 'like', '%'.$term.'%')->orWhere('last_name', 'like', '%'.$term.'%')->get();
        $users = Customer::where('name', 'like', '%'.$term.'%')->get();
        $return_array = array();
        foreach($users as $user) {
            $return_array[] = array('value' => $user->name , 'data' =>$user );
        }
        return Response::json($return_array);
    }

    public function importCustomers(){
        return view('users::customers.import');
    }

    public function uploadCustomers(Request $request){
        $data = $request->all();
        if(isset($data['csv_file'])){
            $handle = fopen($data['csv_file'], 'r');
        }
        $csvArray = array();
        if(isset($handle)){
            $x = 0;
            while (($csv = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if($x > 0 ){
                    //print_r($csv);
                    $customer = new Customer();
                    $customer->kode_customer = $csv[0];
                    $customer->name = $csv[1];
                    $customer->address = $csv[2];
                    $customer->city = $csv[3];
                    $customer->piutang = $csv[5];
                    $customer->save();

                    $achievable = new Achievable();
                    $achievable->customer_id = $customer->id;
                    $achievable->customer_name = $customer->name;
                    $achievable->value = $csv[5];
                    $achievable->status = Achievable::INDEBT;
                    $achievable->save();
                }
            $x++;
            }
            
        }

        return redirect()->route('dashboard')->with('success','successfully import customer data');
    }

    public function indexCustomers(){
        $customers = Customer::all();
        //dd($customers);
        return view('users::customers.index',['customers' => $customers]);
    }

    public function createCustomer(){
        return view('users::customers.create');
    }

    public function storeCustomer(Request $request){
        $data = $request->all();
        $customer = Customer::saveCustomer(null,$data);

        return redirect()->route('users.indexCustomers')->with('success','successfully add new customer');
    }

    public function editCustomer($id){
        $customer = Customer::find($id);

        return view('users::customers.edit',['customer' => $customer]);
    }

    public function updateCustomer($id, Request $request){
        $data = $request->all();
        $customer = Customer::saveCustomer($id,$data);

        return redirect()->route('users.indexCustomers')->with('success','successfully edit customer data');
    }

    public function destroyCustomer($id){

    }

    public function exportSql(){
        //dd(__FILE__);
        $absulePath = __FILE__;

        define("BACKUP_PATH", "C:/xampp/htdocs/sidogede/sql/");
        $server_name   = "localhost";
        $username      = "root";
        $password      = "";
        $database_name = "sidogede";
        $date_string   = date("Ymd");


        $cmd = "C:/xampp/mysql/bin/mysqldump --routines -h ".$server_name." -u ".$username. ($password?" -p ".$password:'')." ".$database_name." > " . BACKUP_PATH . $date_string."_".$database_name.".sql";

        //dd($cmd);
        exec($cmd);

        //return '';
        return redirect()->route('dashboard')->with('success','successfully add backup for sql');
    }

    public function importSql(){

        return view('users::sql.import');
    }

    public function uploadSql(Request $request){
        $data = $request->all();
        //dd($request->sql->path());

        $restore_file  = $request->sql->path();
        $server_name   = "localhost";
        $username      = "root";
        $password      = "";
        $database_name = "sidogede";

        $cmd = "C:/xampp/mysql/bin/mysql -h ".$server_name." -u ".$username. ($password?" -p ".$password:'')." ".$database_name." < ".$restore_file;
        //dd($cmd);
        exec($cmd);

        return redirect()->route('dashboard')->with('success','successfully import backup from sql');
    }


}
