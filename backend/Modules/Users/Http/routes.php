<?php

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Users\Http\Controllers'], function()
{
	Route::any('users/get/allUsers', 'UsersController@getAllUsers')->name('users.getAllUsers');
	Route::get('users/importCustomers','UsersController@importCustomers')->name('users.importCustomers');
	Route::post('users/uploadCustomers','UsersController@uploadCustomers')->name('users.uploadCustomers');

	Route::get('customers','UsersController@indexCustomers')->name('users.indexCustomers');
	Route::get('customers/create','UsersController@createCustomer')->name('users.createCustomer');
	Route::post('customers/store','UsersController@storeCustomer')->name('users.storeCustomer');
	Route::get('customers/edit/{id}','UsersController@editCustomer')->name('users.editCustomer');
	Route::put('customers/{id}/update','UsersController@updateCustomer')->name('users.updateCustomer');
	Route::get('customers/destroyCustomer/{id}','UsersController@destroyCustomer')->name('users.destroyCustomer');

	Route::get('sql/exportSql','UsersController@exportSql')->name('sql.exportSql');
	Route::get('sql/importSql','UsersController@importSql')->name('sql.importSql');
	Route::post('sql/uploadSql','UsersController@uploadSql')->name('sql.uploadSql');
	//Route::get('/', 'UsersController@index');
});