<?php

namespace Modules\Users\Entities;

use Illuminate\Database\Eloquent\Model;

class user extends \App\User
{
    protected $fillable = [];

    public function storage(){
    	return $this->hasMany('\Modules\Storage\Entities\Storage','author_id');
    }

    public function storageLogs(){
    	return $this->hasMany('\Modules\Storage\Entities\StorageLog','author_id');
    }

    public function details(){
    	return $this->hasMany('\Modules\Users\Entities\Details','user_id');
    }

    public function invoices(){
    	return $this->hasMany('\Modules\Transactions\Entities\Invoice','customer_id');
    }

    public function transactions(){
    	return $this->hasMany('\Modules\Transactions\Entities\Transaction','customer_id');
    }

    /*public function achievable(){
        return $this->hasMany('\Modules\Debts\Entities\Achievable','customer_id');
    }*/
}
