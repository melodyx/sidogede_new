<?php

namespace Modules\Users\Entities;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [];
    protected $table = 'customers';

    public function transactions(){
    	return $this->hasMany('\Modules\Transactions\Entities\Transaction','customer_id');
    }

    public function achievables(){
        return $this->hasMany('\Modules\Debts\Entities\Achievable','customer_id');
    }

    public static function saveCustomer($id,$data){
    	if(is_null($id)){
    		$customer = new Customer();
    		
    	}else{
    		$customer = Customer::find($id);
    	}

    	$customer->name = $data['name'];
		$customer->address = $data['address'];
		$customer->city = $data['city'];
		$customer->save();

		return $customer;
    }
}
