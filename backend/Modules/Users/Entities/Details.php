<?php

namespace Modules\Users\Entities;

use Illuminate\Database\Eloquent\Model;

class Details extends Model
{
    protected $fillable = [];
    protected $table = 'user_details';

    public function user(){
    	return $this->belongsTo('\Modules\Users\Entities\User','user_id');
    }

}
