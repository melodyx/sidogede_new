<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAccountantTransactions extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accountant_transactions', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name',255);
            $table->longtext('desc');
            //$table->integer('storage_id');
            $table->integer('company_id');
            $table->integer('account_id');
            $table->integer('parent_id');
            $table->decimal('value',15,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('accountant_transactions');
    }

}
