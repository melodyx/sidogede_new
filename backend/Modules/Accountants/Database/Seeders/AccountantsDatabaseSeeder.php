<?php

namespace Modules\Accountants\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class AccountantsDatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		\DB::table('accountants')->insert([
			'name' => 'kas',
			'code' => 'k001',
		]);

		\DB::table('accountants')->insert([
			'name' => 'penjualan',
			'code' => 'p001',
		]);

		\DB::table('accountants')->insert([
			'name' => 'pembelian',
			'code' => 'p002',
		]);

		\DB::table('accountants')->insert([
			'name' => 'retur',
			'code' => 'r001',
		]);

		\DB::table('accountants')->insert([
			'name' => 'piutang',
			'code' => 'pt001',
		]);

		\DB::table('accountants')->insert([
			'name' => 'hutang',
			'code' => 'ht001',
		]);


		// $this->call("OthersTableSeeder");
	}
}
