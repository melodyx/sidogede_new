<?php

namespace Modules\Accountants\Entities;

use Illuminate\Database\Eloquent\Model;

class Accountant extends Model
{
    protected $fillable = [];
    protected $table = 'accountants';

    public function accountantTransactions(){
    	return $this->hasMany('\Modules\Accountants\Entities\AccountantTransaction','account_id');
    }

    // public function storage(){
    //     return $this->hasMany('\Modules\Storage\Entities\Storage','account_id');
    // }

    public static function saveAccount($id, $data){
    	\DB::beginTransaction();
    	try {
    		if(is_null($id)){
    			$account = new Accountant();
	    	}else{
	    		$account = Accountant::find($id);
	    	}

	    	$account->name = $data['name'];
	    	$account->code = $data['code'];
            $account->desc = $data['desc'];
	    	$account->save();
    	} catch (\Exception $e) {
    		\DB::rollback();
    		dd($e->getMessage());
    		return redirect()->route('accountants.index')->with('success','failed');
    	}
    	\DB::commit();

		// update activity log
		$current_user = \Sentinel::getUser();
		if(is_null($id)){
			$log = array(
				'desc' => 'Adding new account with account id: '.$account->id.' '
			);
    	}else{
    		$log = array(
				'desc' => 'Editing account with account id: '.$account->id.' '
			);
    	}
		event(new \App\Events\UpdateData($current_user, $log));

    	return $account;
    }
}
