<?php

namespace Modules\Accountants\Entities;

use Illuminate\Database\Eloquent\Model;

class AccountantTransaction extends Model
{
    protected $fillable = [];
    protected $table = 'accountant_transactions';

    public function account(){
    	return $this->belongsTo('\Modules\Accountants\Entities\Accountant','account_id');
    }

    public function transactions(){
        return $this->hasMany('\Modules\Transactions\Entities\Transaction','account_trans_id');
    }
    

    public function transactionChild(){
        return $this->hasMany('\Modules\Accountants\Entities\AccountantTransaction','parent_id');
    }

    public function transactionParent(){
        return $this->belongsTo('\Modules\Accountants\Entities\AccountantTransaction','parent_id');
    }


    /*public function storage(){
    	return $this->belongsTo('\Modules\Storage\Entities\Storage','storage_id');
    }*/

    // public function storageLogs(){
    // 	return $this->hasMany('\Modules\Storage\Entities\StorageLog','transaction_id');
    // }

    // public function invoices(){
    //     return $this->hasMany('\Modules\Accountants\Entities\Invoice','transaction_id');
    // }

}
