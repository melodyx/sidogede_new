@extends('layouts.app')
@section('header')

    @parent
	
	<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
	<?php session()->forget('success'); } ?>
	
	<script>
	jQuery(document).ready(function($){
		$('table.account').on('click','a.remove',function(e){
			// var rewardId = $(this).data('id');
			// $.ajax({
			// 	method: 'get',
			// 	url: '<?php echo route('dashboard'); ?>/eventpoints/eventrewarddestroy/'+rewardId
			// });
			var accountId = $(this).data('id');
			var confirmation = confirm('Apakah anda yakin ingin melakukan penghapusan account ini ?');
			if(confirmation == true){
				window.location.replace("{{route('dashboard')}}/accountants/destroyAccount/"+accountId);
			}
		});
	});
	</script>
@endsection
@section('content')
				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i>Account</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Accounts</li>
						</ul>

					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<div class="panel panel-flat">
					<table class="table datatable-basic account">
					<thead>
						<tr>
							<th style="text-align:center;">Id</th>
							<th style="text-align:center;">Account Name</th>
							<th style="text-align:center;">Account Code</th>
							<th style="text-align:center;">Action</th>
						</tr>
					</thead>
					<tbody>	
						<?php if(isset($accounts)){ ?>
							<?php foreach($accounts as $account){?>
							<tr>
								<td style="text-align:center;"><?php if($account->id){echo $account->id;}else{ echo '-';  }?></td>
								<td style="text-align:center;"><?php if($account->name){echo $account->name;}else{ echo '-';  }?></td>
								<td style="text-align:center;"><?php if($account->code){echo $account->code;}else{ echo '-';  }?></td>
								<td style="text-align:center;">
									<ul class="icons-list">
										<li >
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-cog7"></i>
												<span class="caret"></span>
											</a>
											<ul class="dropdown-menu dropdown-menu-right">
												<li class="dropdown-header">Options</li>
												<li><a href="{{route('accountants.edit',$account->id)}}"><i class="icon-pencil7"></i>Edit entry</a></li>
												<li><a class="remove" data-id="<?php echo $account->id?>"><i class="icon-pencil7"></i>Delete</a></li>
											</ul>
										</li>
									</ul>
								</td>
							</tr>
							<?php } ?>
						<?php } ?>
					</tbody>
					</table>	
					</div>
				</div>

@endsection