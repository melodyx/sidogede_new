<?php $user = \Sentinel::getUser(); ?>

<?php if($user->hasAccess(['accountants.general'])){ ?>
<li><a><i class="icon-pencil3"></i>Account</a>
	<ul>		
		<li><a href="{{route('accountants.index')}}">Account List</a></li>
		<li><a href="{{route('accountants.create')}}">Add New Account</a></li>
	</ul>
</li>
<?php } ?>
<?php if($user->hasAccess(['accountants.cashFlow'])){ ?>
<li><a href="{{route('accountants.cashFlow')}}"><i class="icon-pencil3"></i>Cash Flow</a></li>
<?php } ?>