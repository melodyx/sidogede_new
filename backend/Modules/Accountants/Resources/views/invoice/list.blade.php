@extends('layouts.app')
@section('header')

    @parent
	
	<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
	<?php session()->forget('success'); } ?>
	
	<script>
	jQuery(document).ready(function($){
		$('table.account').on('click','a.remove',function(e){
			// var rewardId = $(this).data('id');
			// $.ajax({
			// 	method: 'get',
			// 	url: '<?php echo route('dashboard'); ?>/eventpoints/eventrewarddestroy/'+rewardId
			// });
			var accountId = $(this).data('id');
			var confirmation = confirm('Apakah anda yakin ingin melakukan penghapusan account ini ?');
			if(confirmation == true){
				window.location.replace("{{route('dashboard')}}/accountants/destroyAccount/"+accountId);
			}
		});
	});
	</script>


@endsection
@section('content')
				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i>Cash Flow</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Cash Flow</li>
						</ul>

					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<div class="panel panel-flat">
					<table class="table datatable-basic">
						<thead>
							<tr>
								<th style="text-align:center">Id</th>
								<th style="text-align:center">Name</th>
								<th style="text-align:center">Value</th>
								<th style="text-align:center">Created Date</th>
							</tr>
						</thead>
							<?php foreach($accountantTransaction as $transaction){ ?>
							<tr>
								<td style="text-align:center"><?php echo $transaction->id?></td>
								<td style="text-align:center"><?php echo $transaction->name.' ';?><?php if($transaction->account->storage->count() > 0){echo 'dari '.$transaction->account->storage[0]->supplier->name;}?></td>
								<td style="text-align:center"><?php echo $transaction->value?></td>
								<td style="text-align:center"><?php echo $transaction->created_at; ?></td>
							</tr>
							<?php } ?>
						<tbody>
						</tbody>
					</table>	
					</div>
				</div>

@endsection