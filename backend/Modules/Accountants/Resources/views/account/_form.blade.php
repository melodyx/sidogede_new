<?php
	use \Illuminate\Support\Facades\Input;
	if(!isset($method)){
		$method = 'post';
	}

?>
<script type="text/javascript" src="<?php echo asset('assets/js/plugins/forms/styling/uniform.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo asset('assets/js/pages/form_layouts.js') ?>"></script>


{{ Form::open(array('action' => $action, 'method' => $method, 'class' => 'form-horizontal')) }}
<fieldset class="content-group">
							<legend class="text-bold"><?php if(isset($account)){ echo 'Edit account'; }else{ echo 'Create account';}?></legend>
								<div class="form-group">
									<label class="control-label col-lg-2">Accountant Name</label>
									<div class="col-lg-10">
										{{Form::text('name', isset($account->name)?$account->name:null, array('class' => 'form-control','required' => 'required'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Accountant Code</label>
									<div class="col-lg-10">
										{{Form::text('code', isset($account->code)?$account->code:null, array('class' => 'form-control','required' => 'required'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Description</label>
									<div class="col-lg-10">
										{{Form::textarea('desc', isset($account->desc)?$account->desc:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="text-right">
									<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
								</div>
						</fieldset>
{{ Form::close() }}