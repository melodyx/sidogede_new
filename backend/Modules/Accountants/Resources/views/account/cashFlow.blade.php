@extends('layouts.app')
@section('header')

    @parent
	
	<style>
		.clear{clear:both;}
	</style>
	<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
	<?php session()->forget('success'); } ?>
	
	<script>
	jQuery(document).ready(function($){
		$('table.account').on('click','a.remove',function(e){
			// var rewardId = $(this).data('id');
			// $.ajax({
			// 	method: 'get',
			// 	url: '<?php echo route('dashboard'); ?>/eventpoints/eventrewarddestroy/'+rewardId
			// });
			var accountId = $(this).data('id');
			var confirmation = confirm('Apakah anda yakin ingin melakukan penghapusan account ini ?');
			if(confirmation == true){
				window.location.replace("{{route('dashboard')}}/accountants/destroyAccount/"+accountId);
			}
		});
	});
	</script>

	<script>
		jQuery(document).ready(function($){
			$('.datatable-basic2').dataTable( {
	    		"order": [[ 0, 'desc' ]]
			});
		});
	</script>
@endsection
@section('content')
				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i>Cash Flow</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Cash Flow</li>
						</ul>

					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<div class="panel panel-flat">
					<div class="row">
						<div class="panel-body">
							{{Form::open(array('action'=>'\Modules\Accountants\Http\Controllers\AccountantsController@cashFlowPeriode','method' => 'post'))}}
							<div class="form-group">
								<label class="control-label col-lg-2">Start Date</label>
								<div class="col-lg-10">
									<input type="date" name="minDate" class="form-control" value="{{isset($minDate)?$minDate:null}}">
								</div>
								<div class="clear"></div>
							</div>
							<div class="form-group">
								<label class="control-label col-lg-2">Max date</label>
								<div class="col-lg-10">
									<input type="date" name="maxDate" class="form-control" value="{{isset($maxDate)?$maxDate:null}}">
								</div>
								<div class="clear"></div>
							</div>
							<div class="text-right">
								<input type="submit" class="btn btn-primary">
							</div>
							{{Form::close()}}
							<div class="form-group">
								<label class="control-label col-lg-2">Total cash</label>
								<div class="col-lg-10">
									Rp 
									<?php
										$currentCash = 0;
										foreach($accountantTransaction as $transaction){
											$currentCash = $currentCash + $transaction->value;
										}
										echo number_format($currentCash,2,',','.');
									?>
								</div>
							</div>
						</div>
					</div>
					<table class="table datatable-basic2">
						<thead>
							<tr>
								<th style="text-align:center">Id</th>
								<th style="text-align:center">Account Code</th>
								<th style="text-align:center">Name</th>
								<th style="text-align:center">Value</th>
								<th style="text-align:center">Created Date</th>
							</tr>
						</thead>
							<?php foreach($accountantTransaction as $transaction){ ?>
							<tr>
								<td style="text-align:center"><?php echo $transaction->id?></td>
								<td style="text-align:center"><?php echo $transaction->account->code;?></td>
								<td style="text-align:center"><?php echo $transaction->transactionParent->name;?></td>
								<td style="text-align:center"><?php echo $transaction->value?></td>
								<td style="text-align:center"><?php echo $transaction->created_at; ?></td>
							</tr>
							<?php } ?>
						<tbody>
						</tbody>
					</table>	
					</div>
				</div>

@endsection