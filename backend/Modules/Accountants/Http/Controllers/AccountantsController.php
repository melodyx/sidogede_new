<?php

namespace Modules\Accountants\Http\Controllers;

use Nwidart\Modules\Routing\Controller;
use Modules\Accountants\Entities\Accountant;
use Modules\Accountants\Entities\AccountantTransaction;
use Modules\Storage\Entities\customProduct;
use Modules\Transactions\Entities\Transaction;
use Modules\Transactions\Entities\TransactionDetail;
use Modules\Accountants\Entities\Invoice;
use Illuminate\Http\Request;

class AccountantsController extends Controller {

	public function index()
	{
		$accounts = array();
		$accounts = Accountant::all();
		return view('accountants::index',['accounts' => $accounts]);
	}

	public function create(){
		return view('accountants::account.create');
	}

	public function store(Request $request){
		$data = $request->all();
		$account = Accountant::saveAccount(null,$data);

		return redirect()->route('accountants.index')->with('success','successfully add new account');
	}

	public function edit($id){
		$account = Accountant::find($id);

		return view('accountants::account.edit',['account' => $account]);
	}

	public function update($id, Request $request){
		$data = $request->all();
		$account = Accountant::saveAccount($id,$data);
		return redirect()->route('accountants.index')->with('success','successfully edit new account');
	}

	public function destroyAccount($id){
		$account = Accountant::find($id);
		$account->delete();
		return redirect()->route('accountants.index')->with('success','successfully delete account');
	}

	public function importPembelian(){
		return view('accountants::account.uploadPembelian');
	}

	public function uploadPembelian(Request $request){
		$data = $request->all();
		$account = Accountant::where('name','=','pembelian')->first();
		$kasAccount = Accountant::where('name','=','kas')->first();
		if(isset($data['csv_file'])){
			$handle = fopen($data['csv_file'], 'r');
		}
		$csvArray = array();
		if(isset($handle)){
			$x = 0;
			while (($csv = fgetcsv($handle, 1000, ",")) !== FALSE) {
				if($x > 0 ){
					//print_r($csv);
					$accountTrans = new AccountantTransaction();
					$accountTrans->name = 'Pembelian '.$csv[1];
					$accountTrans->account_id = $account->id;
					$accountTrans->save();

					$kasTrans = new AccountantTransaction();
					$kasTrans->name = 'Kurangi Kas';
					$kasTrans->account_id = $kasAccount->id;
					$kasTrans->parent_id = $accountTrans->id;
					$kasTrans->save();

					$product = customProduct::with('storage.supplier')->where('name','=',$csv[1])->first();
					if(isset($product) && $product->count() > 0){
						$transaction = new Transaction();
						$transaction->account_trans_id = $accountTrans->id;
						$transaction->transDate = $csv[7];
						$transaction->dueDate = $csv[8];
						$transaction->status = Transaction::PENDING;
						$transaction->save();

						$transactionDetail = new TransactionDetail();
						$transactionDetail->transaction_id = $transaction->id;
						$transactionDetail->storage_id = $product->storage[0]->id;
						$transactionDetail->qty = $csv[13];
						$transactionDetail->value = $csv[4];
						$transactionDetail->disc = $csv[16];
						$transactionDetail->save();
					}

				}
			$x++;
			}
			
		}
	return redirect()->route('dashboard');
	}

	public function cashFlow(){
		//$accountantTransaction = AccountantTransaction::with(array('account.storage.customProduct','account.storage.supplier','storageLogs'))->get();
		//dd($accountantTransaction);
		$kas = Accountant::where('name','=','kas')->first();
		$accountantTransaction = AccountantTransaction::with('account','transactionParent')->whereMonth('created_at','=',date('m'))->where('account_id','=',$kas->id)->get();
		//dd($accountantTransaction[0]);
		return view('accountants::account.cashFlow',['accountantTransaction' => $accountantTransaction]);
	}

	public function cashFlowPeriode(Request $request){
		$data = $request->all();
		//dd($data);
		$kas = Accountant::where('name','=','kas')->first();
		if($data['minDate'] && $data['maxDate']){
			//echo date('Y-m-d',strtotime($minDate));
			$accountantTransaction = AccountantTransaction::with('account','transactionParent')->whereDate('created_at','>=',date('Y-m-d',strtotime($data['minDate'])))->whereDate('created_at','<=',date('Y-m-d',strtotime($data['maxDate'])))->where('account_id','=',$kas->id)->get();
		}elseif($data['minDate']){
			$accountantTransaction = AccountantTransaction::with('account','transactionParent')->whereDate('created_at','>=',date('Y-m-d',strtotime($data['minDate'])))->where('account_id','=',$kas->id)->get();
		}elseif($data['maxDate']){
			$accountantTransaction = AccountantTransaction::with('account','transactionParent')->whereDate('created_at','<=',date('Y-m-d',strtotime($data['maxDate'])))->where('account_id','=',$kas->id)->get();
		}else{
			$accountantTransaction = AccountantTransaction::with('account','transactionParent')->where('account_id','=',$kas->id)->get();
		}

		return view('accountants::account.cashFlow',['accountantTransaction' => $accountantTransaction,'mindate' => $data['minDate'], 'maxDate' => $data['maxDate']]);
	}

	public function invoiceList(){
		/*$invoices = Invoice::with(array('customer','transaction.account.storage.supplier'))->get();
		return view('accountants::invoice.list');*/
	}

}
