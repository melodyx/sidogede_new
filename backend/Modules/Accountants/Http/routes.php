<?php

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Accountants\Http\Controllers'], function()
{
	//Route::get('/', 'AccountantsController@index');
	Route::get('accountants/cashFlow','AccountantsController@cashFlow')->name('accountants.cashFlow');
	Route::post('accountants/cashFlow/periode','AccountantsController@cashFlowPeriode')->name('accountants.cashFlowPeriode');
	Route::get('accountants/invoiceList','AccountantsController@invoiceList')->name('accountants.invoiceList');
	Route::get('accountants/destroyAccount/{id}','AccountantsController@destroyAccount')->name('accountants.destroyAccount');

	Route::get('accountants/importPenjualan','AccountantsController@importPenjualan')->name('accountants.importPenjualan');
	Route::post('accountants/uploadPenjualan','AccountantsController@uploadPenjualan')->name('accountants.uploadPenjualan');
	Route::get('accountants/importPembelian','AccountantsController@importPembelian')->name('accountants.importPembelian');
	Route::post('accountants/uploadPembelian','AccountantsController@uploadPembelian')->name('accountants.uploadPembelian');


	Route::resource('accountants','AccountantsController');
});