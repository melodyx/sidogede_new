<?php

Route::group(['middleware' => 'web', 'prefix' => 'customers', 'namespace' => 'Modules\Customers\Http\Controllers'], function()
{
	Route::get('/', 'CustomersController@index');
});