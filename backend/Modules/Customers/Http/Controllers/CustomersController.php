<?php

namespace Modules\Customers\Http\Controllers;

use Nwidart\Modules\Routing\Controller;

class CustomersController extends Controller {

	public function index()
	{
		return view('customers::index');
	}

}
