<?php

namespace Modules\Debts\Http\Controllers;

use Nwidart\Modules\Routing\Controller;
use Modules\Users\Entities\User;
use Modules\Users\Entities\Customer;
use Modules\Storage\Entities\Supplier;
use Modules\Debts\Entities\Achievable;
use Modules\Debts\Entities\AchievableDetail;
use Modules\Debts\Entities\Payable;
use Modules\Debts\Entities\PayableDetail;
use Modules\Accountants\Entities\Accountant;
use Modules\Accountants\Entities\AccountantTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class DebtsController extends Controller {

	public function index()
	{
		return view('debts::index');
	}


	public function indexAchievable(){
		$achievables = Achievable::with('customer','achievableDetails')->get();
		return view('debts::achievable.index',['achievables' => $achievables]);
	}

	public function createAchievable(){
		return view('debts::achievable.create');
	}

	public function storeAchievable(Request $request){
		$data = $request->all();

		$achievable = Achievable::saveAchievable(null,$data);

		return redirect()->route('debts.indexAchievable')->with('success','successfully add new achievable');
	}

	public function editAchievable($id){
		$achievable = Achievable::with('customer','achievableDetails')->find($id);
		//dd($achievable);
		return view('debts::achievable.edit',['achievable' => $achievable]);
	}


	public function updateAchievable($id, Request $request){
		$data = $request->all();
		//dd($data);

		$achievable = Achievable::with('customer','achievableDetails')->find($id);
		$totalPaid = 0;

		foreach($achievable->achievableDetails as $achievableDetail){
			$totalPaid = $totalPaid + $achievableDetail->paid_value;
		}

		if(isset($data['paid_value'])){
			$account = Accountant::where('name','=','piutang')->first();
			$kas = Accountant::where('name','=','kas')->first();
			foreach($data['paid_value'] as $paid_value){
				$achievableDetail = new AchievableDetail();
				$achievableDetail->achievable_id = $achievable->id;
				$achievableDetail->paid_value = $paid_value;
				$achievableDetail->status = Achievable::PAID;
				$achievableDetail->save();

				$totalPaid = $totalPaid + $paid_value;

				$accountantTransaction = new AccountantTransaction();
				$accountantTransaction->name = 'Pengurangan piutang untung pelanggan dengan nama '.$achievable->customer->name;
				$accountantTransaction->account_id = $account->id;
				$accountantTransaction->value = $achievableDetail->paid_value * -1;
				$accountantTransaction->save();

				$kasAccount = new AccountantTransaction();
				$kasAccount->name = 'Penambahan kas';
				$kasAccount->account_id = $kas->id;
				$kasAccount->value = $achievableDetail->paid_value;
				$kasAccount->parent_id = $accountantTransaction->id;
				$kasAccount->save();
			}
		}

		if($achievable->value <= $totalPaid){
			$achievable->status = Achievable::PAID;
			$achievable->save();
		}

		return redirect()->route('debts.indexAchievable')->with('success','successfully add payment');
	}

	public function indexPayable(){
		$payables = Payable::with('supplier','payableDetails')->get();
		return view('debts::payable.index',['payables' => $payables]);

	}

	public function createPayable(){
		return view('debts::payable.create');
	}

	public function storePayable(Request $request){
		$data = $request->all();
		//dd($data);
		$payable = Payable::savePayable(null,$data);

		return redirect()->route('debts.indexPayable')->with('success','successfully add new payable');
	}

	public function editPayable($id){
		$payable = Payable::with('supplier','payableDetails')->find($id);
		//dd($achievable);
		return view('debts::payable.edit',['payable' => $payable]);
	}


	public function updatePayable($id, Request $request){
		$data = $request->all();
		//dd($data);

		$payable = Payable::with('supplier','payableDetails')->find($id);
		$totalPaid = 0;

		foreach($payable->payableDetails as $payableDetail){
			$totalPaid = $totalPaid + $payableDetail->paid_value;
		}

		if(isset($data['paid_value'])){
			$account = Accountant::where('name','=','hutang')->first();
			$kas = Accountant::where('name','=','kas')->first();
			foreach($data['paid_value'] as $paid_value){
				$payableDetail = new PayableDetail();
				$payableDetail->payable_id = $payable->id;
				$payableDetail->paid_value = $paid_value;
				$payableDetail->status = Payable::PAID;
				$payableDetail->save();

				$totalPaid = $totalPaid + $paid_value;

				$accountantTransaction = new AccountantTransaction();
				$accountantTransaction->name = 'Pengurangan hutang terhadap supplier dengan nama '.$payable->supplier->name;
				$accountantTransaction->account_id = $account->id;
				$accountantTransaction->value = $payableDetail->paid_value;
				$accountantTransaction->save();

				$kasAccount = new AccountantTransaction();
				$kasAccount->name = 'Pengurangan kas';
				$kasAccount->account_id = $kas->id;
				$kasAccount->value = $payableDetail->paid_value * -1;
				$kasAccount->parent_id = $accountantTransaction->id;
				$kasAccount->save();
			}
		}

		if($payable->value <= $totalPaid){
			$payable->status = Payable::PAID;
			$payable->save();
		}

		return redirect()->route('debts.indexPayable')->with('success','successfully add payment');
	}


	public function importPayable(){
		return view('debts::payable.import');
	}

	public function uploadPayable(Request $request){
		$data = $request->all();
		$account = Accountant::where('name','=','hutang')->first();
		$kasAccount = Accountant::where('name','=','kas')->first();
		if(isset($data['csv_file'])){
			$handle = fopen($data['csv_file'], 'r');
		}
		$csvArray = array();
		if(isset($handle)){
			$x = 0;
			while (($csv = fgetcsv($handle, 1000, ",")) !== FALSE) {
				if($x > 0 ){
					//print_r($csv);
					$supplier = Supplier::where('name','=',$csv[1])->first();

					if(isset($supplier)){
						$payable = new Payable();
						$payable->supplier_id = $supplier->id;
						$payable->supplier_name = $supplier->name;
						$payable->value = $csv[3];
						$payable->status = Payable::INDEBT;
						$payable->save();

						if($csv[5]){
							$payableDetail = new PayableDetail();
							$payableDetail->payable_id = $payable->id;
							$payableDetail->paid_value = $csv[5];
							$payableDetail->status = Payable::PAID;
							$payableDetail->save();
						}
					}
				}
			$x++;
			}
			
		}
		return redirect()->route('dashboard')->with('success','successfully import payable');
	}

	/*public function create(){

	}

	public function store(Request $request){

	}

	public function edit($id){

	}*/
}
