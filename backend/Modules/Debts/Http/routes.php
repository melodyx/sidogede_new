<?php

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Debts\Http\Controllers'], function()
{
	//Route::get('/', 'DebtsController@index');

	Route::get('debts/indexAchievable','DebtsController@indexAchievable')->name('debts.indexAchievable');
	Route::get('debts/createAchievable','DebtsController@createAchievable')->name('debts.createAchievable');
	Route::post('debts/storeAchievable','DebtsController@storeAchievable')->name('debts.storeAchievable');
	Route::get('debts/editAchievable/{id}','DebtsController@editAchievable')->name('debts.editAchievable');
	Route::put('debts/{id}/updateAchievable','DebtsController@updateAchievable')->name('debts.updateAchievable');

	Route::get('debts/indexPayable','DebtsController@indexPayable')->name('debts.indexPayable');
	Route::get('debts/createPayable','DebtsController@createPayable')->name('debts.createPayable');
	Route::post('debts/storePayable','DebtsController@storePayable')->name('debts.storePayable');
	Route::get('debts/editPayable/{id}','DebtsController@editPayable')->name('debts.editPayable');
	Route::put('debts/{id}/updatePayable','DebtsController@updatePayable')->name('debts.updatePayable');
	Route::get('debts/importPayable','DebtsController@importPayable')->name('debts.importPayable');
	Route::post('debts/uploadPayable','DebtsController@uploadPayable')->name('debts.uploadPayable');


	//Route::resource('debts','DebtsController');
});