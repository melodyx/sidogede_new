<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePiutang extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('achievables', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('customer_id');
            $table->string('customer_name',255);
            $table->decimal('value',25,2);
            $table->string('jenis_pembayaran',255);
            $table->string('note_number');
            $table->string('status',255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('achievables');
    }

}
