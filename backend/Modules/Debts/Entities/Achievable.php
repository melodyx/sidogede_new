<?php

namespace Modules\Debts\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Users\Entities\Customer;
use Modules\Accountants\Entities\Accountant;
use Modules\Accountants\Entities\AccountantTransaction;

class Achievable extends Model
{
	const INDEBT = 'in debt';
	const PAID = 'paid off';
    protected $fillable = [];

    protected $table = 'achievables';

    public function customer(){
    	return $this->belongsTo('\Modules\Users\Entities\Customer','customer_id');
    }

    public function achievableDetails(){
    	return $this->hasMany('\Modules\Debts\Entities\AchievableDetail','achievable_id');
    }

    public static function saveAchievable($id,$data){

    	$customer = Customer::find($data['user_id']);
    	if(is_null($id)){
    		$achievable = new Achievable();
    	}else{
    		$achievable = Achievable::find($id);
    	}

    	$achievable->customer_id = $customer->id;
    	$achievable->customer_name = $customer->name;
    	$achievable->value = $data['value'];
    	$achievable->status = Achievable::INDEBT;
    	$achievable->save();

    	$account = Accountant::where('name','=','piutang')->first();
    	$accountTransaction = new AccountantTransaction();
    	$accountTransaction->name = 'Penambahan piutang untuk pelanggan dengan nama '.$customer->name;
    	$accountTransaction->account_id = $account->id;
    	$accountTransaction->value = $achievable->value;
    	$accountTransaction->save();

    }
}
