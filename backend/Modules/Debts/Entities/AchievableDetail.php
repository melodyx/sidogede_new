<?php

namespace Modules\Debts\Entities;

use Illuminate\Database\Eloquent\Model;

class AchievableDetail extends Model
{
    protected $fillable = [];
    protected $table = 'achievable_details';

    public function achievable(){
    	return $this->belongsTo('\Modules\Debts\Entities\Achievable','achievable_id');
    }
}
