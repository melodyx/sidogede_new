<?php

namespace Modules\Debts\Entities;

use Illuminate\Database\Eloquent\Model;

class PayableDetail extends Model
{
    protected $fillable = [];

    protected $table = 'payable_details';

    public function payable(){
    	return $this->belongsTo('\Modules\Debts\Entities\Payable','payable_id');
    }
}
