<?php

namespace Modules\Debts\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Storage\Entities\Supplier;
use Modules\Accountants\Entities\Accountant;
use Modules\Accountants\Entities\AccountantTransaction;

class Payable extends Model
{
	const INDEBT = 'in debt';
	const PAID = 'paid off';

    protected $fillable = [];

    protected $table = 'payables';

    public function supplier(){
    	return $this->belongsTo('\Modules\Storage\Entities\Supplier','supplier_id');
    }

    public function payableDetails(){
    	return $this->hasMany('\Modules\Debts\Entities\PayableDetail','payable_id');
    }

    public static function savePayable($id,$data){

    	$supplier = Supplier::find($data['supplier_id']);
    	if(is_null($id)){
    		$payable = new Payable();
    	}else{
    		$payable = Payable::find($id);
    	}

    	$payable->supplier_id = $supplier->id;
    	$payable->supplier_name = $supplier->name;
    	$payable->value = $data['value'];
    	$payable->status = Payable::INDEBT;
    	$payable->save();

    	$account = Accountant::where('name','=','hutang')->first();
    	$accountTransaction = new AccountantTransaction();
    	$accountTransaction->name = 'Penambahan hutang terhadap supplier dengan nama '.$supplier->name;
    	$accountTransaction->account_id = $account->id;
    	$accountTransaction->value = $payable->value;
    	$accountTransaction->save();

    }
}
