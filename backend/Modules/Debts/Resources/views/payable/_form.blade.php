<?php
	use \Illuminate\Support\Facades\Input;
	if(!isset($method)){
		$method = 'post';
	}

?>

{{ Form::open(array('action' => $action, 'method' => $method, 'class' => 'form-vertical')) }}


			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
						<fieldset>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										{{ Form::label('shipping_name', 'Name') }}
	                                    @if(isset($transaction))
										    {{ Form::text('shipping_name', old('shipping_name', isset($transaction->customer->first_name)?$transaction->customer->first_name:null), array('class' => 'form-control', 'disabled' => 'disabled', 'required' => 'required')) }}
											{{ Form::hidden('supplier_id', old('supplier_id', isset($transaction->customer->id)?$transaction->customer->id:null), array('class' => 'form-control', 'disabled' => 'disabled')) }}
	                                    @else
	                                        {{ Form::text('shipping_name', old('shipping_name', isset($data['shipping_name'])?$data['shipping_name']:null), array('class' => 'form-control', 'required' => 'required')) }}
											{{ Form::hidden('supplier_id', old('supplier_id', isset($data['user_id'])?$data['user_id']:null), array('class' => 'form-control')) }}
	                                    @endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
									{{ Form::label('hutang_value', 'Value') }}
									{{ Form::number('value', isset($data['value'])?$data['value']:null, array('class' => 'form-control', 'required' => 'required')) }}
									</div>
								</div>
							</div>
							<div class="text-right">
								<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
    



<script type="text/javascript">
	Number.prototype.formatMoney = function(c, d, t){
		var n = this,
				c = isNaN(c = Math.abs(c)) ? 2 : c,
				d = d == undefined ? "." : d,
				t = t == undefined ? "," : t,
				s = n < 0 ? "-" : "",
				i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
				j = (j = i.length) > 3 ? j % 3 : 0;
		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	};

	$(function() {


        // User Auto Complete
        $("input[name=shipping_name]").autocomplete({
            source: "{{ route('storage.getAllSuppliers') }}",
            minLength: 2,
            select: function( event, ui ) {
            	//console.log(ui);
                $('input[name=supplier_id]').val(ui.item.data.id);

            }
        });

	});


</script>
{{ Form::close() }}