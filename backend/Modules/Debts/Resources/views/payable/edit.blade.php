@extends('layouts.app')

@section('header')
    @parent
    <!-- JS Extra for form -->
 	
 	<link href="<?php echo asset('assets/css/extras/animate.min.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo Module::asset('transactions:css/jquery-ui.min.css'); ?>" rel="stylesheet" type="text/css">

	<script type="text/javascript" src="<?php echo Module::asset('transactions:js/jquery-ui.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo Module::asset('transactions:js/pages/edit.js'); ?>"></script>


 <style>
 	.clear{clear:both}

 </style>
@endsection
@section('content')
<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Achievables</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="{{route('debts.indexPayable')}}">Payables</a></li>
							<li class="active">Edit</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<div class="content">
				<div class="panel panel-flat">
					<div class="row">
						<div class="panel-body">
							<div class="form-group">
								<label class="control-label col-lg-2">Supplier Name</label>
								<div class="col-lg-10">
									{{$payable->supplier->name}}
								</div>
							</div>
							<div class="clear"></div>
							<div class="form-group">
								<label class="control-label col-lg-2">Remaining Payable</label>
								<div class="col-lg-10">
									<?php 
										$remainingPayable = $payable->value;
										if(isset($payable->payableDetails)){
											foreach($payable->payableDetails as $payableDetail){
												$remainingPayable = $remainingPayable - $payableDetail->paid_value;
											}
										}
										echo $remainingPayable;
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-flat">
					{{ Form::open(array('action' => array('\Modules\Debts\Http\Controllers\DebtsController@updatePayable',$payable->id), 'method' => 'put', 'class' => 'form-vertical')) }}
					<div class="row">
						<div class="panel-body">
							<div id="add_product_item_fieldset">
								<div class="form-group">
									<label class="control-label col-lg-2">Paid</label>
									<div class="col-lg-10">
										{{Form::number('value',null,array('class' => 'form-control', 'id'=>'paid-value'))}}
									</div>
									<div class="clear"></div>
								</div>
								<div class="text-right">
									<button type="button" class="btn btn-primary" id="add_order_item_button" {{ isset($order)?'disabled':'' }}>Add Payment <i class="icon-arrow-right14 position-right"></i></button>
								</div>
							</div>
						</div>
						<div class="panel-body">
							<table class="table datatables-basic achievableDetails" id="order-detail-table" style="margin-bottom:30px;">
								<thead>
									<tr>
										<th style="text-align:center">Date</th>
										<th style="text-align:center">Paid value</th>
									</tr>
								</thead>
								<tbody>
									<?php $x = 0; foreach($payable->payableDetails as $payableDetail){?>
									<tr data-row="<?php echo $x; ?>">
										<td style="text-align: center">{{$payableDetail->created_at}}</td>
										<td style="text-align: center">{{$payableDetail->paid_value}}</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
							<div class="clear"></div>
							<div class="text-right">
								<input type="submit" class="btn btn-primary">
							</div>
						</div>
					</div>
					<script>
						$('#add_product_item_fieldset').on('click','#add_order_item_button',function(e){
						e.preventDefault();
							var table = $('table#order-detail-table tbody');
							//console.log(typeof($('table#order-detail-table tr:last').data('row')));
							var id = (typeof($('table#order-detail-table tr:last').data('row'))!=='undefined')?parseInt($('table#order-detail-table tr:last').data('row')) + 1:0;
							var paidValue = $('#paid-value').val();
							//console.log(id);
							//$.each('#order-detail-table tbody tr')

							
								var newEl = '\
									<tr data-row="'+id+'" role="row">\
										<td class="sorting_1" style="text-align:center"><?php echo date('Y-m-d'); ?></td>\
										<td style="text-align:center">\
											'+paidValue+'<input class="form-control" name="paid_value['+id+']" type="hidden" value="'+paidValue+'">\
										</td>\
									</tr>';
								//newEl = $(newEl).addClass('fadeInUp animated');
								
								table.append(newEl);
								//console.log(newEl);

							

						//$(this).removeData();
						$('#new-product').val('');
						$('#paid-value').val('');


						});
					</script>
					{{Form::close()}}
				</div>
				</div>
				</div>
@endsection