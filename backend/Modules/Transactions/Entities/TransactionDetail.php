<?php

namespace Modules\Transactions\Entities;

use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{

	const BASIC = 'basic';
	const RETUR = 'retur';
    const BACKUP = 'backup';
	
    protected $fillable = [];
    protected $table = 'transaction_details';

    public function transaction(){
    	return $this->belongsTo('\Modules\Transactions\Entities\Transaction','transaction_id');
    }

    public function storage(){
    	return $this->belongsTo('\Modules\Storage\Entities\Storage','storage_id');
    }
}
