<?php

namespace Modules\Transactions\Entities;

use Illuminate\Database\Eloquent\Model;


class invoice extends Model
{
    const WAIT ='waiting for payment';
	const PAID ='paid';
    protected $fillable = [];

    public function customer(){
    	return $this->belongsTo('\Modules\Users\Entities\User','customer_id');
    }

    public function transaction(){
    	return $this->belongsTo('\Modules\Accountants\Entities\AccountantTransaction','transaction_id');
    }
}
