<?php

namespace Modules\Transactions\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Transactions\Entities\TransactionDetail;
use Modules\Storage\Entities\CustomProduct;
use Modules\Storage\Entities\Storage;
use Modules\Storage\Entities\StorageLog;
use Modules\Accountants\Entities\Accountant;
use Modules\Accountants\Entities\AccountantTransaction;
use Modules\Users\Entities\User;
use Modules\Users\Entities\Customer;

class Transaction extends Model
{
	const COMPLETED = 'completed';
	const PROCESSING = 'processing';
	const WAITINGPAYMENT= 'waiting for payment';
	const PENDING ='pending';
	const CANCELLED = 'cancelled';
	const BACKUP = 'backup';

	const BELI ='beli';
	const JUAL ='jual';

    protected $fillable = [];

    public function transactionDetails(){
    	return $this->hasMany('\Modules\Transactions\Entities\TransactionDetail','transaction_id');
    }

    public function accountTransaction(){
    	return $this->belongsTo('\Modules\Accountants\Entities\AccountantTransaction','account_trans_id');
    }

    public function customer(){
    	return $this->belongsTo('Modules\Users\Entities\Customer','customer_id');
    }

    public static function saveTransaction($id,$data){
    	session()->forget('success');
    	\DB::beginTransaction();
    	try {
	    	if(is_null($id)){
		    	$account = Accountant::where('name','=','penjualan')->first();
				$accountantTrans = new AccountantTransaction();
				//$accountantTrans->name = 'Penjualan Product';
				$accountantTrans->account_id = $account->id;
				$accountantTrans->save();

				$customerData = Customer::find($data['user_id']);
				//dd($customerData);

				$transaction = new Transaction();
				$transaction->customer_id = $data['user_id'];
				if(isset($customerData) ){
					$transaction->customer_name = $customerData->name;
				}
				$transaction->account_trans_id = $accountantTrans->id;
				$transaction->email = $data['email'];
				$transaction->address = $data['address'];
				$transaction->city = $data['city'];
				$transaction->phone = $data['phone'];
				$transaction->status = $data['status'];
				$transaction->dueDate = $data['dueDate'];
				$transaction->terbilang = $data['terbilang'];
				$transaction->type = Transaction::JUAL;
				$transaction->save();

				$transaction->transDate = $transaction->created_at;
				$transaction->save();

				$totalValue = 0;
				//dd($data['products']);
				$returnData['status'] = false;
				$x = 0;
				if(!isset($data['products'])){

					$returnData['message'] = 'No products added';
					return $returnData;
				}

				foreach($data['products'] as $product){
				
					$storage = Storage::with('customProduct','supplier')->find($product['storage_id']);
					/*if($storage->qty < $product['qty']){
						\DB::rollback();
						//dd('test');
						$returnData['message'] = 'Insufficient  stock';
						$insufficient = true;
						$returnData['productData'][$x] = $storage;
						$x++;
						//return $returnData;
					}*/
					$storage->qty = $storage->qty - $product['qty'];
					$storage->save();

					$storageLog = new StorageLog();
					$storageLog->storage_id = $storage->id;
					$storageLog->value = $product['qty'] * -1;
					$storageLog->save();

					$transDetail = new TransactionDetail();
					$transDetail->transaction_id = $transaction->id;
					$transDetail->storage_id = $product['storage_id'];
					$transDetail->product_name = $storage->customProduct->name;
					$transDetail->supplier_name = $storage->supplier->name;
					$transDetail->qty = $product['qty'];
					$transDetail->value = $product['price'];
					$transDetail->disc = $product['disc'];
					$transDetail->satuan = $product['satuan'];
					$transDetail->status = TransactionDetail::BASIC;
					$transDetail->save();

					$subTotalValue = $product['qty'] * $product['price'];
					$disc = $product['disc'] / 100 * $subTotalValue;
					$subTotalValue = $subTotalValue - $disc;	
					$totalValue = $totalValue + $subTotalValue;
				}
				
				if(isset($insufficient)){
					return $returnData;
				}

				$accountantTrans->name = 'Penjualan '.$storage->customProduct->name.' dengan id transaksi '.$transaction->id;
				$accountantTrans->value = $totalValue * -1;
				$accountantTrans->save();

				$kas = Accountant::where('name','=','kas')->first();
				$kasAccount = new AccountantTransaction();
				$kasAccount->name = 'tambah kas';
				$kasAccount->account_id = $kas->id;
				$kasAccount->value = $totalValue;
				$kasAccount->parent_id = $accountantTrans->id;
				$kasAccount->save();
			}else{
				$transaction = Transaction::find($id);
				$transaction->status = $data['status'];
				$transaction->dueDate = $data['dueDate'];
				$transaction->save();
			}
			//exit;
			
		}catch(Exception $e){
			\DB::rollback();
			dd($e->getMessage());
		}
		\DB::commit();
		$returnData['status'] = true;
		return $returnData;
    }

    public static function savePembelian($id, $data){
    	\DB::beginTransaction();
    	try{
    		if(is_null($id)){
				$returnData['status'] = false;
				$x = 0;

				if(!isset($data['products'])){
					$returnData['message'] = 'No products added';
					return $returnData;
				}

				foreach($data['products'] as $product){
					$storage = Storage::with('customProduct','supplier')->find($product['storage_id']);
					/*if($storage->qty < $product['qty']){
						\DB::rollback();
						//dd('test');
						$returnData['message'] = 'Insufficient  stock';
						$insufficient = true;
						$returnData['productData'][$x] = $storage;
						$x++;
						//return $returnData;
					}*/
					$storage->qty = $storage->qty + $product['qty'];
					$storage->save();

					$storageLog = new StorageLog();
					$storageLog->storage_id = $storage->id;
					$storageLog->value = $product['qty'];
					$storageLog->save();

					$account = Accountant::where('name','=','penjualan')->first();
					$accountantTrans = new AccountantTransaction();
					//$accountantTrans->name = 'Penjualan Product';
					$accountantTrans->account_id = $account->id;
					$accountantTrans->save();

					$transaction = new Transaction();
					$transaction->customer_name= $storage->supplier->name;
					$transaction->account_trans_id = $accountantTrans->id;
					$transaction->status = Transaction::PROCESSING;
					$transaction->type = Transaction::BELI;
					$transaction->save();

					$transaction->transDate = $transaction->created_at;
					$transaction->save();

					$transDetail = new TransactionDetail();
					$transDetail->transaction_id = $transaction->id;
					$transDetail->storage_id = $product['storage_id'];
					$transDetail->product_name = $storage->customProduct->name;
					$transDetail->supplier_name = $storage->supplier->name;
					$transDetail->qty = $product['qty'];
					$transDetail->value = $product['price'];
					$transDetail->disc = $product['disc'];
					$transDetail->satuan = $product['satuan'];
					$transDetail->status = TransactionDetail::BASIC;
					$transDetail->save();

					$subTotalValue = $product['qty'] * $product['price'];
					$disc = $product['disc'] / 100 * $subTotalValue;
					$subTotalValue = $subTotalValue - $disc;	
					//$totalValue = $totalValue + $subTotalValue;

					$accountantTrans->name = 'Pembelian '.$storage->customProduct->name.' dengan id transaksi '.$transaction->id;
					$accountantTrans->value = $subTotalValue;
					$accountantTrans->save();

					$kas = Accountant::where('name','=','kas')->first();
					$kasAccount = new AccountantTransaction();
					$kasAccount->name = 'kurangi kas';
					$kasAccount->account_id = $kas->id;
					$kasAccount->value = $subTotalValue * -1;
					$kasAccount->parent_id = $accountantTrans->id;
					$kasAccount->save();
				}
	    	}else{

	    	}

    	}catch(\Exception $e){
    		\DB::rollback();
    		dd($e->getMessage());
    	}
    	\DB::commit();
    	

    	$returnData['status'] = true;
		return $returnData;
    }
}
