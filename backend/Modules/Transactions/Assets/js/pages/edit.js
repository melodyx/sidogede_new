/**
 * JavaScript for Edit Order in admin
 **/

$('document').ready(function(){
    $('#order-detail-table').on('click','#delete-order-item',function(e){
        e.preventDefault();
        var element = $(this).parents('tr');
        element.addClass('fadeOutUp animated');
        setTimeout(
            function() {
                element.remove();
        }, 500);
    });
});