<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransactions extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('customer_id');
            $table->string('customer_name',255);
            $table->integer('account_trans_id');
            $table->string('email',255);
            $table->integer('phone');
            $table->longText('address');
            $table->string('city',255);
            $table->string('country',255);
            $table->string('state',255);
            $table->date('transDate');
            $table->date('dueDate');
            $table->longText('terbilang');
            $table->string('status',255);
            $table->string('type',255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transactions');
    }

}
