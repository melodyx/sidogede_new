<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransactionDetails extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_details', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('transaction_id');
            $table->integer('storage_id');
            $table->string('product_name',255);
            $table->string('supplier_name',255);
            $table->integer('qty');
            $table->string('satuan');
            $table->integer('value');
            $table->string('status',255);
            $table->decimal('disc',4,1);
            $table->string('faktur',255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transaction_details');
    }

}
