<?php
	use \Illuminate\Support\Facades\Input;
	if(!isset($method)){
		$method = 'post';
	}

?>

{{ Form::open(array('action' => $action, 'method' => $method, 'class' => 'form-vertical')) }}
	<?php if($type == \Modules\Transactions\Entities\Transaction::JUAL){?>
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">Order details</h5>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
						<li><a data-action="close"></a></li>
					</ul>
				</div>
			</div>

			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
						<fieldset>
							<legend class="text-semibold"><i class="icon-person position-left"></i> Shipping details</legend>

							<div class="row">
								<div class="col-md-12">
										{{Form::hidden('type',$type)}}
									<div class="form-group">
										{{ Form::label('shipping_name', 'Name') }}
	                                    @if(isset($transaction))
										    {{ Form::text('shipping_name', old('shipping_name', isset($transaction->customer->first_name)?$transaction->customer->first_name:null), array('class' => 'form-control', 'disabled' => 'disabled', 'required' => 'required')) }}
											{{ Form::hidden('user_id', old('user_id', isset($transaction->customer->id)?$transaction->customer->id:null), array('class' => 'form-control', 'disabled' => 'disabled')) }}
	                                    @else
	                                        {{ Form::text('shipping_name', old('shipping_name', isset($data['shipping_name'])?$data['shipping_name']:null), array('class' => 'form-control', 'required' => 'required')) }}
											{{ Form::hidden('user_id', old('user_id', isset($data['user_id'])?$data['user_id']:null), array('class' => 'form-control')) }}
	                                    @endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										{{ Form::label('address', 'Address') }}
	                                    @if(isset($transaction))
										    {{ Form::text('address', old('address', isset($transaction->address)?$transaction->address:null), array('class' => 'form-control', 'disabled' => 'disabled')) }}
	                                    @else
	                                        {{ Form::text('address', old('address', isset($data['address'])?$data['address']:null), array('class' => 'form-control')) }}
	                                    @endif
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										{{ Form::label('city', 'City') }}
	                                    @if(isset($transaction))
										    {{ Form::text('city', old('city', isset($transaction->city)?$transaction->city:null), array('class' => 'form-control', 'disabled' => 'disabled')) }}
	                                    @else
	                                        {{ Form::text('city', old('city', isset($data['city'])?$data['city']:null), array('class' => 'form-control')) }}
	                                    @endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										{{ Form::label('email', 'Email') }}
	                                    @if(isset($transaction))
										    {{ Form::text('email', old('email', isset($transaction->email)?$transaction->email:null), array('class' => 'form-control', 'disabled' => 'disabled')) }}
	                                    @else
	                                        {{ Form::text('email', old('email', isset($data['email'])?$data['email']:null), array('class' => 'form-control')) }}
	                                    @endif
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										{{ Form::label('phone', 'Phone') }}
	                                    @if(isset($transaction))
										    {{ Form::text('phone', old('phone', isset($transaction->phone)?$transaction->phone:null), array('class' => 'form-control', 'disabled' => 'disabled')) }}
	                                    @else
	                                        {{ Form::text('phone', old('phone', isset($data['phone'])?$data['phone']:null), array('class' => 'form-control')) }}
	                                    @endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										{{Form::label('Due Date')}}
										{{Form::date('dueDate',old('dueDate', isset($data['dueDate'])?$data['dueDate']:null),array('class'=>'form-control'))}}
									</div>
								</div>
							</div>
							<?php /*
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Country:</label>
										<select name="country" data-placeholder="Select your country" class="select" {{ (isset($order)?'disabled':'') }}>
											<option value="AF">Afghanistan</option>
											<option value="AX">Åland Islands</option>
											<option value="AL">Albania</option>
											<option value="DZ">Algeria</option>
											<option value="AS">American Samoa</option>
											<option value="AD">Andorra</option>
											<option value="AO">Angola</option>
											<option value="AI">Anguilla</option>
											<option value="AQ">Antarctica</option>
											<option value="AG">Antigua and Barbuda</option>
											<option value="AR">Argentina</option>
											<option value="AM">Armenia</option>
											<option value="AW">Aruba</option>
											<option value="AU">Australia</option>
											<option value="AT">Austria</option>
											<option value="AZ">Azerbaijan</option>
											<option value="BS">Bahamas</option>
											<option value="BH">Bahrain</option>
											<option value="BD">Bangladesh</option>
											<option value="BB">Barbados</option>
											<option value="BY">Belarus</option>
											<option value="BE">Belgium</option>
											<option value="BZ">Belize</option>
											<option value="BJ">Benin</option>
											<option value="BM">Bermuda</option>
											<option value="BT">Bhutan</option>
											<option value="BO">Bolivia, Plurinational State of</option>
											<option value="BQ">Bonaire, Sint Eustatius and Saba</option>
											<option value="BA">Bosnia and Herzegovina</option>
											<option value="BW">Botswana</option>
											<option value="BV">Bouvet Island</option>
											<option value="BR">Brazil</option>
											<option value="IO">British Indian Ocean Territory</option>
											<option value="BN">Brunei Darussalam</option>
											<option value="BG">Bulgaria</option>
											<option value="BF">Burkina Faso</option>
											<option value="BI">Burundi</option>
											<option value="KH">Cambodia</option>
											<option value="CM">Cameroon</option>
											<option value="CA">Canada</option>
											<option value="CV">Cape Verde</option>
											<option value="KY">Cayman Islands</option>
											<option value="CF">Central African Republic</option>
											<option value="TD">Chad</option>
											<option value="CL">Chile</option>
											<option value="CN">China</option>
											<option value="CX">Christmas Island</option>
											<option value="CC">Cocos (Keeling) Islands</option>
											<option value="CO">Colombia</option>
											<option value="KM">Comoros</option>
											<option value="CG">Congo</option>
											<option value="CD">Congo, the Democratic Republic of the</option>
											<option value="CK">Cook Islands</option>
											<option value="CR">Costa Rica</option>
											<option value="CI">Côte d'Ivoire</option>
											<option value="HR">Croatia</option>
											<option value="CU">Cuba</option>
											<option value="CW">Curaçao</option>
											<option value="CY">Cyprus</option>
											<option value="CZ">Czech Republic</option>
											<option value="DK">Denmark</option>
											<option value="DJ">Djibouti</option>
											<option value="DM">Dominica</option>
											<option value="DO">Dominican Republic</option>
											<option value="EC">Ecuador</option>
											<option value="EG">Egypt</option>
											<option value="SV">El Salvador</option>
											<option value="GQ">Equatorial Guinea</option>
											<option value="ER">Eritrea</option>
											<option value="EE">Estonia</option>
											<option value="ET">Ethiopia</option>
											<option value="FK">Falkland Islands (Malvinas)</option>
											<option value="FO">Faroe Islands</option>
											<option value="FJ">Fiji</option>
											<option value="FI">Finland</option>
											<option value="FR">France</option>
											<option value="GF">French Guiana</option>
											<option value="PF">French Polynesia</option>
											<option value="TF">French Southern Territories</option>
											<option value="GA">Gabon</option>
											<option value="GM">Gambia</option>
											<option value="GE">Georgia</option>
											<option value="DE">Germany</option>
											<option value="GH">Ghana</option>
											<option value="GI">Gibraltar</option>
											<option value="GR">Greece</option>
											<option value="GL">Greenland</option>
											<option value="GD">Grenada</option>
											<option value="GP">Guadeloupe</option>
											<option value="GU">Guam</option>
											<option value="GT">Guatemala</option>
											<option value="GG">Guernsey</option>
											<option value="GN">Guinea</option>
											<option value="GW">Guinea-Bissau</option>
											<option value="GY">Guyana</option>
											<option value="HT">Haiti</option>
											<option value="HM">Heard Island and McDonald Islands</option>
											<option value="VA">Holy See (Vatican City State)</option>
											<option value="HN">Honduras</option>
											<option value="HK">Hong Kong</option>
											<option value="HU">Hungary</option>
											<option value="IS">Iceland</option>
											<option value="IN">India</option>
											<option value="ID">Indonesia</option>
											<option value="IR">Iran, Islamic Republic of</option>
											<option value="IQ">Iraq</option>
											<option value="IE">Ireland</option>
											<option value="IM">Isle of Man</option>
											<option value="IL">Israel</option>
											<option value="IT">Italy</option>
											<option value="JM">Jamaica</option>
											<option value="JP">Japan</option>
											<option value="JE">Jersey</option>
											<option value="JO">Jordan</option>
											<option value="KZ">Kazakhstan</option>
											<option value="KE">Kenya</option>
											<option value="KI">Kiribati</option>
											<option value="KP">Korea, Democratic People's Republic of</option>
											<option value="KR">Korea, Republic of</option>
											<option value="KW">Kuwait</option>
											<option value="KG">Kyrgyzstan</option>
											<option value="LA">Lao People's Democratic Republic</option>
											<option value="LV">Latvia</option>
											<option value="LB">Lebanon</option>
											<option value="LS">Lesotho</option>
											<option value="LR">Liberia</option>
											<option value="LY">Libya</option>
											<option value="LI">Liechtenstein</option>
											<option value="LT">Lithuania</option>
											<option value="LU">Luxembourg</option>
											<option value="MO">Macao</option>
											<option value="MK">Macedonia, the former Yugoslav Republic of</option>
											<option value="MG">Madagascar</option>
											<option value="MW">Malawi</option>
											<option value="MY">Malaysia</option>
											<option value="MV">Maldives</option>
											<option value="ML">Mali</option>
											<option value="MT">Malta</option>
											<option value="MH">Marshall Islands</option>
											<option value="MQ">Martinique</option>
											<option value="MR">Mauritania</option>
											<option value="MU">Mauritius</option>
											<option value="YT">Mayotte</option>
											<option value="MX">Mexico</option>
											<option value="FM">Micronesia, Federated States of</option>
											<option value="MD">Moldova, Republic of</option>
											<option value="MC">Monaco</option>
											<option value="MN">Mongolia</option>
											<option value="ME">Montenegro</option>
											<option value="MS">Montserrat</option>
											<option value="MA">Morocco</option>
											<option value="MZ">Mozambique</option>
											<option value="MM">Myanmar</option>
											<option value="NA">Namibia</option>
											<option value="NR">Nauru</option>
											<option value="NP">Nepal</option>
											<option value="NL">Netherlands</option>
											<option value="NC">New Caledonia</option>
											<option value="NZ">New Zealand</option>
											<option value="NI">Nicaragua</option>
											<option value="NE">Niger</option>
											<option value="NG">Nigeria</option>
											<option value="NU">Niue</option>
											<option value="NF">Norfolk Island</option>
											<option value="MP">Northern Mariana Islands</option>
											<option value="NO">Norway</option>
											<option value="OM">Oman</option>
											<option value="PK">Pakistan</option>
											<option value="PW">Palau</option>
											<option value="PS">Palestinian Territory, Occupied</option>
											<option value="PA">Panama</option>
											<option value="PG">Papua New Guinea</option>
											<option value="PY">Paraguay</option>
											<option value="PE">Peru</option>
											<option value="PH">Philippines</option>
											<option value="PN">Pitcairn</option>
											<option value="PL">Poland</option>
											<option value="PT">Portugal</option>
											<option value="PR">Puerto Rico</option>
											<option value="QA">Qatar</option>
											<option value="RE">Réunion</option>
											<option value="RO">Romania</option>
											<option value="RU">Russian Federation</option>
											<option value="RW">Rwanda</option>
											<option value="BL">Saint Barthélemy</option>
											<option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
											<option value="KN">Saint Kitts and Nevis</option>
											<option value="LC">Saint Lucia</option>
											<option value="MF">Saint Martin (French part)</option>
											<option value="PM">Saint Pierre and Miquelon</option>
											<option value="VC">Saint Vincent and the Grenadines</option>
											<option value="WS">Samoa</option>
											<option value="SM">San Marino</option>
											<option value="ST">Sao Tome and Principe</option>
											<option value="SA">Saudi Arabia</option>
											<option value="SN">Senegal</option>
											<option value="RS">Serbia</option>
											<option value="SC">Seychelles</option>
											<option value="SL">Sierra Leone</option>
											<option value="SG">Singapore</option>
											<option value="SX">Sint Maarten (Dutch part)</option>
											<option value="SK">Slovakia</option>
											<option value="SI">Slovenia</option>
											<option value="SB">Solomon Islands</option>
											<option value="SO">Somalia</option>
											<option value="ZA">South Africa</option>
											<option value="GS">South Georgia and the South Sandwich Islands</option>
											<option value="SS">South Sudan</option>
											<option value="ES">Spain</option>
											<option value="LK">Sri Lanka</option>
											<option value="SD">Sudan</option>
											<option value="SR">Suriname</option>
											<option value="SJ">Svalbard and Jan Mayen</option>
											<option value="SZ">Swaziland</option>
											<option value="SE">Sweden</option>
											<option value="CH">Switzerland</option>
											<option value="SY">Syrian Arab Republic</option>
											<option value="TW">Taiwan, Province of China</option>
											<option value="TJ">Tajikistan</option>
											<option value="TZ">Tanzania, United Republic of</option>
											<option value="TH">Thailand</option>
											<option value="TL">Timor-Leste</option>
											<option value="TG">Togo</option>
											<option value="TK">Tokelau</option>
											<option value="TO">Tonga</option>
											<option value="TT">Trinidad and Tobago</option>
											<option value="TN">Tunisia</option>
											<option value="TR">Turkey</option>
											<option value="TM">Turkmenistan</option>
											<option value="TC">Turks and Caicos Islands</option>
											<option value="TV">Tuvalu</option>
											<option value="UG">Uganda</option>
											<option value="UA">Ukraine</option>
											<option value="AE">United Arab Emirates</option>
											<option value="GB">United Kingdom</option>
											<option value="US">United States</option>
											<option value="UM">United States Minor Outlying Islands</option>
											<option value="UY">Uruguay</option>
											<option value="UZ">Uzbekistan</option>
											<option value="VU">Vanuatu</option>
											<option value="VE">Venezuela, Bolivarian Republic of</option>
											<option value="VN">Viet Nam</option>
											<option value="VG">Virgin Islands, British</option>
											<option value="VI">Virgin Islands, U.S.</option>
											<option value="WF">Wallis and Futuna</option>
											<option value="EH">Western Sahara</option>
											<option value="YE">Yemen</option>
											<option value="ZM">Zambia</option>
											<option value="ZW">Zimbabwe</option>
										</select>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										{{ Form::label('state', 'State') }}
	                                    @if(isset($order))
										    {{ Form::text('state', old('state', isset($order->state)?$order->state:null), array('class' => 'form-control', 'disabled' => 'disabled')) }}
	                                    @else
	                                        {{ Form::text('state', old('state', isset($order->state)?$order->state:null), array('class' => 'form-control')) }}
	                                    @endif
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										{{ Form::label('zip', 'ZIP Code') }}
	                                    @if(isset($order))
										    {{ Form::text('zip', old('zip', isset($order->zip)?$order->zip:null), array('class' => 'form-control', 'disabled' => 'disabled')) }}
	                                    @else
	                                        {{ Form::text('zip', old('zip', isset($order->zip)?$order->zip:null), array('class' => 'form-control')) }}
	                                    @endif
									</div>
								</div>

								<div class="col-md-9">
									<div class="form-group">
										{{ Form::label('shipping_address', 'Address') }}
	                                    @if(isset($order))
	                                        {{ Form::text('shipping_address', old('city', isset($order->shipping_address)?$order->shipping_address:null), array('class' => 'form-control', 'disabled' => 'disabled')) }}
	                                    @else
	                                        {{ Form::text('shipping_address', old('city', isset($order->shipping_address)?$order->shipping_address:null), array('class' => 'form-control')) }}
	                                    @endif
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										{{ Form::label('city', 'City') }}
	                                    @if(isset($order))
	                                        {{ Form::text('city', old('city', isset($order->city)?$order->city:null), array('class' => 'form-control', 'disabled' => 'disabled')) }}
	                                    @else
	                                        {{ Form::text('city', old('city', isset($order->city)?$order->city:null), array('class' => 'form-control')) }}
	                                    @endif
									</div>
								</div>

								<div class="col-md-9">
									<div class="form-group">
										{{ Form::label('shipping_address2', 'Address 2') }}
	                                    @if(isset($order))
	                                        {{ Form::text('shipping_address2', old('city', isset($order->shipping_address2)?$order->shipping_address2:null), array('class' => 'form-control', 'disabled')) }}
	                                    @else
	                                        {{ Form::text('shipping_address2', old('city', isset($order->shipping_address2)?$order->shipping_address2:null), array('class' => 'form-control')) }}
	                                    @endif
									</div>
								</div>
							</div>*/?>
						</fieldset>

					</div>
					<div class="col-md-6">
						<fieldset>
							<legend class="text-semibold"><i class="icon-cart5 position-left"></i> Order Info</legend>
							<div class="col-md-12">
								<div class="form-group">
									{{ Form::label('status', 'Status') }}
	                                <?php
	                                if(isset($order)):
	                                    switch ($order->status){
	                                        case \Modules\Transactions\Entities\Transaction::COMPLETED :
	                                            echo '<span class="label label-success" style="float:right;">Completed</span>';
	                                            break;
	                                        case \Modules\Transactions\Entities\Transaction::PROCESSING :
	                                            echo '<span class="label label-success bg-primary-300" style="float:right;">Processing</span>';
	                                            break;
	                                        case \Modules\Transactions\Entities\Transaction::WAITINGPAYMENT :
	                                            echo '<span class="label label-warning" style="float:right;">Waiting Payment</span>';
	                                            break;
	                                        case \Modules\Transactions\Entities\Transaction::PENDING :
	                                            echo '<span class="label label-primary" style="float:right;">Pending</span>';
	                                            break;
	                                        case \Modules\Transactions\Entities\Transaction::CANCELLED :
	                                            echo '<span class="label label-danger" style="float:right;">Cancelled</span>';
	                                            break;
	                                        default:
	                                            echo '<span class="label label-danger" style="float:right;">Cancelled</span>';
	                                    };
	                                endif; ?>
									@if(!isset($order))
	                                	{{ Form::select('status', array(\Modules\Transactions\Entities\Transaction::COMPLETED => 'Completed', \Modules\Transactions\Entities\Transaction::PROCESSING => 'Processing',\Modules\Transactions\Entities\Transaction::WAITINGPAYMENT => 'Waiting Payment',\Modules\Transactions\Entities\Transaction::PENDING => 'Pending', \Modules\Transactions\Entities\Transaction::CANCELLED => 'Cancelled' ), old('status', isset($order->status)?$order->status:'pending'), array('class' => 'form-control')) }}
									@else
										{{ Form::select('status', array(\Modules\Transactions\Entities\Transaction::COMPLETED => 'Completed', \Modules\Transactions\Entities\Transaction::PROCESSING => 'Processing',\Modules\Transactions\Entities\Transaction::WAITINGPAYMENT => 'Waiting Payment',\Modules\Transactions\Entities\Transaction::PENDING => 'Pending', \Modules\Transactions\Entities\Transaction::CANCELLED => 'Cancelled' ), old('status', isset($order->status)?$order->status:'pending'), array('class' => 'form-control', 'disabled' => 'disabled')) }}
									@endif
								</div>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Order Items</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <div class="row">
            <div class="col-md-12">
                <fieldset>
                	<div class="row">
                		@if(empty($transaction))
						<div class="col-md-4" id="add_product_item_fieldset">
							<fieldset>
								<legend class="text-semibold"><i class="icon-cart-add position-left"></i> Add Order Item</legend>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											{{ Form::label('new_product', 'Products') }}
                                            @if(isset($order))
											    {{ Form::text('new_product', '', array('class' => 'form-control', 'id' => 'new-product', 'disabled' => 'disabled')) }}
                                            @else
                                                {{ Form::text('new_product', '', array('class' => 'form-control', 'id' => 'new-product')) }}
                                            @endif
										</div>
									</div>
								</div>
								<!--<div class="row">
									<div class="col-md-2">
										<div class="form-group">
											{{ Form::label('new_qty', 'Qty') }}
											{{ Form::text('new_qty', '', array('class' => 'form-control')) }}
										</div>
									</div>
								</div>-->

							</fieldset>
							<div class="text-right">
								<button type="button" class="btn btn-primary" id="add_order_item_button" {{ isset($order)?'disabled':'' }}>Add Product <i class="icon-arrow-right14 position-right"></i></button>
							</div>
						</div>
						@endif
                	</div>
                    <div class="row">
                        <div>
							<div class="row">
                                <legend class="text-semibold"><i class="icon-cart5 position-left"></i> Items List</legend>
								<div class="table-responsive">
									<table class="table table-hover table-framed" id="order-detail-table">
										<thead>
										<tr>
											<th class="col-xs-3">Product Name</th>
											<th class="col-xs-2">Product SKU</th>
											<th class="col-xs-2">Product Supplier</th>
											<th class="col-xs-2">Qty</th>
											<th class="col-xs-2">Price</th>
											<th class="col-xs-2">Satuan</th>
											<th class="col-xs-2">Discount</th>
											<th class="col-xs-2">Total</th>
											<th class="text-center col-xs-1">Actions</th>
										</tr>
										</thead>
										<tbody>
										<?php
										$i = 0;
                                        if(isset($transaction->transactionDetails)):
                                            foreach($transaction->transactionDetails as $detail){
                                            $i++; ?>
                                            <tr id="order_item_{{ $detail->id }}" data-row="{{ $i }}">
                                                <td>
                                                    <?php echo $detail->storage->customProduct->name; ?>
                                                </td>
                                                <td>
                                                    <?php echo $detail->storage->sku; ?>
                                                </td>
                                                <td>
                                                    <?php echo $detail->storage->supplier->name; ?>
                                                </td>
                                                <td>
                                                    <?php echo $detail->qty; ?>
                                                </td>
                                                <td id="order_detail_total">
                                                    <?php echo $detail->value;?>
                                                </td>
                                                <td>
                                                    <?php echo $detail->satuan; ?>
                                                </td>
                                                <td>
                                                    <?php echo $detail->disc; ?>
                                                </td>
                                                <td class="text-center">
													<?php 

													$total = $detail->qty * $detail->value;
													$disc = $detail->disc / 100 * $total;
													$total = $total - $disc;
													echo $total;; 
													?>
                                                </td>
                                                <td>
                                                	-
                                                </td>
                                            </tr>
                                            <?php }
                                        endif; ?>
										</tbody>
									</table>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 col-md-offset-6" style="margin-top:20px;">
									<fieldset>
										<legend class="text-semibold"><i class="icon-coin-dollar position-left"></i> Total</legend>
										<div class="row">
											<div class="col-md-8 text-right">
												Total
											</div>
											<div class="col-md-4 text-bold text-right" id="total_order_all">					
												{{ number_format(isset($order)?$order->total_amount:0,2) }}
											</div>
										</div>
									</fieldset>
								</div>
							</div>
                        </div>
                    </div>
                </fieldset>
            </div>
            </div>
            <div class="row">
			<div class="col-md-12">
				<div class="form-group">
					{{Form::label('Terbilang Total')}}
					{{Form::text('terbilang',old('terbilang', isset($data['terbilang'])?$data['terbilang']:null),array('class'=>'form-control'))}}
				</div>
			</div>
		</div>
        </div>
    </div>
    
</div>
@if(!isset($order))
<div class="text-right">
	<button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
</div>
@endif

<script type="text/javascript">
	Number.prototype.formatMoney = function(c, d, t){
		var n = this,
				c = isNaN(c = Math.abs(c)) ? 2 : c,
				d = d == undefined ? "." : d,
				t = t == undefined ? "," : t,
				s = n < 0 ? "-" : "",
				i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
				j = (j = i.length) > 3 ? j % 3 : 0;
		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	};

	$(function() {

	    // Add Product Auto Complete
		$("#new-product").autocomplete({
			source: "{{ route('storage.getCustomProducts') }}",
			minLength: 1,
			select: function( event, ui ) {
				if($(this).parent().parent().find('.supplier-wrapper')){
					$(this).parent().parent().find('.supplier-wrapper').remove();
				}
					$(this).parent().parent().append(
						'<div class="form-group supplier-wrapper">'+
						'<select name="supplier" class="form-control"></select>'+
						'</div>'
					);
					$.each(ui.item.data.storage, function(index,value){
						$('select[name="supplier"]').append('<option value="'+value.id+'">'+value.supplier.name+'</option>');
					});

					$('#add_order_item_button')
						.data('product_id', ui.item.data.id);
				
				
			}
		});

        // User Auto Complete
        $("input[name=shipping_name]").autocomplete({
            source: "{{ route('users.getAllUsers') }}",
            minLength: 2,
            select: function( event, ui ) {
            	
                //$('input[name=email]').val(ui.item.data.email);
                //$('input[name=phone]').val(ui.item.data.phone);
                $('input[name=user_id]').val(ui.item.data.id);
                $('input[name=address]').val(ui.item.data.address);
                $('input[name=city]').val(ui.item.data.city);
            }
        });

	});

	$('#add_product_item_fieldset').on('click','#add_order_item_button',function(e){
		e.preventDefault();
		if($(this).data('product_id')) {
			var table = $('table#order-detail-table tbody');
			var id = $('table#order-detail-table tr:last').data('row');
			var storageId = $('select[name="supplier"]').val();
			var productId = $(this).data('product_id');
			var product;

			//$.each('#order-detail-table tbody tr')

			$.get("<?php echo URL::to('/'); ?>/storage/searchProductSupplier/"+storageId+"/"+$(this).data('product_id'), function(data) {
				//console.log(data)
				product = data;

				console.log(product);
				if(id == 0)
					id++;
				if(!id)
					id = 0;
				var newEl = '\
					<tr data-row="' + id + '" role="row">\
						<td class="sorting_1">\
							'+product.name+' <input class="form-control" name="products['+id+'][name]" type="hidden" value="'+product.name+'">\
							<input name="products['+id+'][product_id]" type="hidden" value="'+productId+'">\
							<input name="products['+id+'][storage_id]" type="hidden" value="'+product.storage[0].id+'">\
						</td>\
						<td>\
							'+product.storage[0].sku+'<input class="form-control" name="products['+id+'][sku]" type="hidden" value="'+product.storage[0].sku+'">\
						</td>\
						<td>\
							'+product.storage[0].supplier.name+'<input class="form-control" name="products['+id+'][supplier]" type="hidden" value="'+product.storage[0].supplier.name+'">\
						</td>\
						<td>\
							<input class="form-control" name="products['+id+'][qty]" type="text" value="1" id="order_detail_qty">\
						</td>\
						<td>\
							'+<?php if($type == \Modules\Transactions\Entities\Transaction::JUAL){ ?>product.storage[0].price<?php }else{?>product.storage[0].buying_price<?php } ?>+'<input class="form-control" name="products['+id+'][price]" type="hidden" value="'+<?php if($type == \Modules\Transactions\Entities\Transaction::JUAL){ ?>product.storage[0].price<?php }else{?>product.storage[0].buying_price<?php } ?>+'" id="order_detail_price">\
						</td>\
						<td>\
							<select name="products['+id+'][satuan]">\
								<option value="LSM">LSM</option>\
								<option value="SET">SET</option>\
								<option value="PCS">PCS</option>\
							</select>\
						</td>\
						<td>\
							<input class="form-control" name="products['+id+'][disc]" type="text" value="0" id="order_detail_disc">\
						</td>\
						<td id="order_detail_total" class="order_tot">\
						</td>\
						<td class="text-center">\
							<ul class="icons-list">\
								<li><a href="#" id="delete-order-item"><i class="icon-cancel-circle2 red"></i> &nbsp;</a></li>\
							</ul>\
						</td>\
					</tr>';
				newEl = $(newEl).addClass('fadeInUp animated');
				newEl.find('#order_detail_qty').bind('input', function() {
					var total = $(this).val() * $(this).parents('tr').find('#order_detail_price').val();
					var disc = $(this).parents('tr').find('#order_detail_disc').val() / 100 * total;
					//console.log(disc);
					total = total - disc;

					$(this).parents('tr').find('#order_detail_total').text(total.formatMoney(2));
					var total = 0;
					$(this).parents('tbody').find('.order_tot').each(function(val){
						total = total + parseFloat($(this).text().replace(/\,/g,''));
						//console.log(total);
					})

					$('#total_order_all').empty();
					$('#total_order_all').html(total.formatMoney(2));
				});

				newEl.find('#order_detail_disc').bind('input', function() {
					var total = $(this).parents('tr').find('#order_detail_qty').val() * $(this).parents('tr').find('#order_detail_price').val();
					var disc = $(this).val() / 100 * total;

					total = total - disc;

					$(this).parents('tr').find('#order_detail_total').text(total.formatMoney(2));
					var total = 0;
					$(this).parents('tbody').find('.order_tot').each(function(val){
						total = total + parseFloat($(this).text().replace(/\,/g,''));
						// console.log('test');
					})
					$('#total_order_all').empty();
					$('#total_order_all').html(total.formatMoney(2));
				});

				newEl.find('#order_detail_total').text((newEl.find('#order_detail_qty').val() * newEl.find('#order_detail_price').val()).formatMoney(2))
				table.append(newEl);

				var total = 0;
				$('tbody').find('.order_tot').each(function(val){
					total = total + parseFloat($(this).text().replace(/\,/g,''));
					//console.log(total);
				})

				$('#total_order_all').empty();
				$('#total_order_all').html(total.formatMoney(2));
			});
			

		}
		$(this).removeData();
		$('select[name="supplier"]').parent().remove();
		$('#new-product').val('');


	});

	$('#order_detail_qty').bind('input', function() {
		var total = $(this).val() * $(this).parents('tr').find('#order_detail_price').val();
		$(this).parents('tr').find('#order_detail_total').text(total.formatMoney(2));
	});
</script>
{{ Form::close() }}