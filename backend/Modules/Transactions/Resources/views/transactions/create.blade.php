@extends('layouts.app')

@section('header')
    @parent
    <!-- JS Extra for form -->
 	
 	<link href="<?php echo asset('assets/css/extras/animate.min.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo Module::asset('transactions:css/jquery-ui.min.css'); ?>" rel="stylesheet" type="text/css">

	<script type="text/javascript" src="<?php echo Module::asset('transactions:js/jquery-ui.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo Module::asset('transactions:js/pages/edit.js'); ?>"></script>

<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
<?php session()->forget('success'); } ?>
 <style>
 	.clear{clear:both}

 </style>
@endsection
@section('content')
<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Transactions</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="{{route('transactions.index')}}">Transactions</a></li>
							<li class="active">Create</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<?php if(isset($storageCheck)){?>
					<div class="row">
						<div class="panel panel-flat">
							<div class="panel-body">
								<p>List of product stock</p>
								<ul>
									<?php foreach($storageCheck as $storageData){?>
									<li>{{$storageData->customProduct->name.' dari supplier '.$storageData->supplier->name.' stock yang tersisa '.$storageData->qty}}</li>
									<?php } ?>
								</ul>
							</div>
						</div>
					</div>
					<?php } ?>
					<div class="row">
						<div class="panel panel-flat">
							<div class="panel-body">
								<?php if($type == \Modules\Transactions\Entities\Transaction::JUAL){
									$action = '\Modules\Transactions\Http\Controllers\TransactionsController@store';
								}else{
									$action = '\Modules\Transactions\Http\Controllers\TransactionsController@storePembelian';
								}
								?>
								@include('transactions::transactions._form', ['action' => $action])
							</div>
						</div>
					</div>
				</div>
@endsection