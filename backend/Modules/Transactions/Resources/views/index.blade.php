@extends('layouts.app')
@section('header')

    @parent
	
    <link rel="stylesheet" type="text/css" href="<?php echo Module::asset('transactions:css/colorbox.css')?>">
    <script type="text/javascript" src="<?php echo Module::asset('transactions:js/jquery.colorbox-min.js'); ?>"></script>

	<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
	<?php session()->forget('success'); } ?>
	
	<script>
	jQuery(document).ready(function($){
		$('table.product').on('click','a.remove',function(e){
			// var rewardId = $(this).data('id');
			// $.ajax({
			// 	method: 'get',
			// 	url: '<?php echo route('dashboard'); ?>/eventpoints/eventrewarddestroy/'+rewardId
			// });
			var transactionId = $(this).data('id');
			var confirmation = confirm('Apakah anda yakin ingin melakukan penghapusan transaksi ini ?');
			if(confirmation == true){
				window.location.replace("{{route('dashboard')}}/transactions/destroyTransaction/"+transactionId);
			}
		});

		$('table.product').on('click','a.changeStatus',function(e){
			
			var productId = $(this).data('id');
			var status = $(this).data('status');
			var confirmation = confirm('Apakah anda yakin ingin melakukan pengubahan status transaksi ini ?');
			if(confirmation == true){
				window.location.replace("{{route('dashboard')}}/transactions/changeStatus/"+productId+"/"+status);
			}
		});


		$('html').on('click','a.summary',function(e){
			
			$.ajax({
			  url: "{{route('transactions.ajaxTotalValue')}}",
			  method:"GET"
			}).done(function(data){
				
				var html = '<div class="wrapper" style="padding:10px; overlay:auto; height:500px background-color:#fff; text-align:center;">'+
							'<table style="text-align:center;">'+
							'<thead>'+
							'<tr>'+
							'<th style="text-align:center;">Customer Name</th>'+
							'<th style="text-align:center;">Value</th>'+
							'</tr>'+
							'</thead>'+
							'<tbody>';
				$.each(data,function(index,value){
					if(index != 'totalAll'){
						html += '<tr><td style="width:150px; text-align:center; padding:7px 0">'+value.name+'</td><td style="text-align:center; padding:7px 0;">'+value.totalValue+'</td></tr>';
					}

				});
				//console.log(data.totalAll);
				html += '</tbody>'+
				'<tfoot style="border-top:1px solid #000">'+
				'<tr>'+
				'<td style="text-align:center">Total</td>'+
				'<td style="text-align:center">'+data.totalAll+'</td>'+
				'</tr>'+
				'</tfoot>'+
				'</table></div>';
				//html += '<div class="total-section" style="border-top:1px solid #000"></div>'

				$.colorbox({
					html : html,
					maxHeight:500,
					opacity : 0.5,
				});
			});




			

			
		});

	});
	</script>

	<style>
		.clear{clear:both;}
		ul.pagination{float:right; display:inline-block; margin-top:30px;}

		.filter-button{float:left; display:inline-block; margin:20px 0 20px 20px;}

		.search-section{float:right; margin:20px 20px 20px;}
		.search-section input{margin-left:20px;}
	</style>


@endsection
@section('content')
				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i>Transactions</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Transactions</li>
						</ul>

					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<div class="panel panel-flat">
					<?php if($type == Modules\Transactions\Entities\Transaction::JUAL && !isset($checkOutstanding)){?>
						<div class="filter-button">
							<a class="btn btn-primary" href="{{route('transactions.filterOutstanding')}}">Check Outstanding</a>
						</div>
					<?php }elseif($type == Modules\Transactions\Entities\Transaction::JUAL && isset($checkOutstanding)){?>
						<div class="filter-button">
							<a class="btn btn-primary summary">Summary</a>
						</div>
					<?php } ?>
					<div class="search-section">
						<?php if($type == \Modules\Transactions\Entities\Transaction::JUAL){?>
						{{Form::open(array('method' => 'post', 'action' => '\Modules\Transactions\Http\Controllers\TransactionsController@searchData'))}}
						<?php }else{ ?>
						{{Form::open(array('method' => 'post', 'action' => '\Modules\Transactions\Http\Controllers\TransactionsController@searchDataBeli'))}}
						<?php } ?>
						<label>Search:</label><input type="text" name="search">
						{{Form::close()}}
						<div class="clear"></div>
					</div>
					<table class="table product">
					<thead>
						<tr>
							<th style="text-align:center;">Id</th>
							<th style="text-align:center;"><?php if($type == \Modules\Transactions\Entities\Transaction::BELI){?>Supplier name<?php }else{ ?>Customer Name<?php } ?></th>
							<th style="text-align:center;">Product List</th>
							<th style="text-align:center;">Total value</th>
							<th style="text-align:center;">Transaction Date</th>
							<th style="text-align:center;">Status</th>
							<th style="text-align:center;">Action</th>
						</tr>
					</thead>
					<tbody>	
						<?php if(isset($transactions)){ ?>
							<?php foreach($transactions as $transaction){?>
							<tr>
								<td style="text-align:center;"><?php if(isset($transaction->id)){echo $transaction->id;}else{ echo '-';  }?></td>
								<td style="text-align:center;"><?php if(isset($transaction->customer)){echo $transaction->customer->first_name.' '.$transaction->customer->last_name;}elseif(isset($transaction->customer_name)){ echo $transaction->customer_name;  }else{ echo '-'; }?></td>
								<td style="text-align:center;"><?php if(isset($transaction->transactionDetails)){
										foreach($transaction->transactionDetails as $transDetail){
											if(isset($transDetail->storage)){
												$productName = $transDetail->storage->customProduct->name.' dari '.$transDetail->storage->supplier->name;
											}else{
												$productName = $transDetail->product_name;
											}
											echo $productName.'<br/>';
										}
									}else{ echo '-';  }?></td>
								<td style="text-align:center;"><?php if(isset($transaction->transactionDetails)){
										$totalValue = 0;
										foreach($transaction->transactionDetails as $transDetail){
											$value = $transDetail->qty * $transDetail->value;
											$disc = $transDetail->disc / 100 * $value;
											$value = $value - $disc;
											$totalValue = $totalValue + $value;
										}
										echo $totalValue;
									}else{ echo '-';  }?></td>
								<td style="text-align:center;"><?php echo $transaction->transDate; ?></td>
								<td style="text-align:center;"><?php if(isset($transaction->status)){ echo $transaction->status; } ?></td>	
								<td style="text-align:center;">
									<?php if($transaction->status != \Modules\Transactions\Entities\Transaction::BACKUP && $transaction->type != \Modules\Transactions\Entities\Transaction::BELI){?>
									<ul class="icons-list">
										<li >
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-cog7"></i>
												<span class="caret"></span>
											</a>
											<ul class="dropdown-menu dropdown-menu-right">
												<li class="dropdown-header">Options</li>
												<?php /*<li><a href="{{route('transactions.edit',$transaction->id)}}"><i class="icon-pencil7"></i>Edit</a></li>*/ ?>
												<li><a href="{{ route('transactions.createInvoice',$transaction->id) }}"><i class="icon-pencil7"></i>Make Invoice</a></li>
												<li><a href="{{ route('transactions.createSJ',$transaction->id) }}"><i class="icon-pencil7"></i>Make Surat Jalan</a></li>
													<!-- <li><a class="changeStatus" data-id ="<?php echo $transaction->id; ?>" data-status="{{\Modules\Transactions\Entities\Transaction::COMPLETED}}"><i class="icon-pencil7"></i>Paid</a></li> -->
													<?php if($transaction->type != \Modules\Transactions\Entities\Transaction::BELI){?>
													<li><a href="{{route('transactions.edit',$transaction->id)}}"><i class="icon-pencil7"></i>Edit transaction</a></li>
													<?php } ?>
													<li><a class="remove" data-id="<?php echo $transaction->id ?>"><i class="icon-pencil7"></i>Delete</a></li>
											</ul>
										</li>
									</ul>
									<?php }else{ echo '-'; } ?>
								</td>
							</tr>
							<?php } ?>
						<?php } ?>
					</tbody>
					</table>
					<ul class="pagination">
						<?php if(isset($checkOutstanding)){?>
							<li><a href="{{route('transactions.paginationOutstanding',1)}}"><<</a></li>
							<?php if(!isset($currentPage)){ $currentPage = 1; } if($totalPage > 10){$maxPage = $currentPage + 9;}else{$maxPage = $totalPage; } for($x = $currentPage; $x <= $maxPage; $x++){ ?>
							<li><a href="{{route('transactions.paginationOutstanding',$x)}}"><?php echo $x; ?></a></li>
							<?php } ?> 
							<li><a href="{{route('transactions.paginationOutstanding',$totalPage)}}">>></a></li>
						<?php }else{ ?>
							<li><a href="{{route('transactions.pagination',array(1,$type))}}"><<</a></li>
							<?php if(!isset($currentPage)){ $currentPage = 1; } if($totalPage > 10){$maxPage = $currentPage + 9;}else{$maxPage = $totalPage; } for($x = $currentPage; $x <= $maxPage; $x++){ ?>
							<li><a href="{{route('transactions.pagination',array($x,$type))}}"><?php echo $x; ?></a></li>
							<?php } ?> 
							<li><a href="{{route('transactions.pagination',array($totalPage,$type))}}">>></a></li>
						<?php } ?>
					</ul>		
					</div>
				</div>

@endsection