<?php $user = \Sentinel::getUser(); ?>
<li><a><i class="icon-pencil3"></i>Transactions</a>
	<ul>
		<li><a href="{{route('transactions.index')}}">Transactions List</a></li>
		<li><a href="{{route('transactions.create')}}">Add Transaction</a></li>
		<li><a href="{{route('transactions.createCustomInvoice')}}" target='_blank'>Create Custom Invoice</a></li>
		<li><a href="{{route('transactions.createCustomSJ')}}" target='_blank'>Create Custom SJ</a></li>
	</ul>
</li>
<li><a><i class="icon-pencil3"></i>Purchasing</a>
	<ul>
		<li><a href="{{route('transactions.listPembelian')}}">List Purchase</a></li>
		<li><a href="{{route('transactions.createPembelian')}}">Add Purchasing</a></li>
	</ul>
</li>
<li><a href="{{route('transactions.retur')}}"><i class="icon-pencil3"></i>Retur</a></li>


