@extends('layouts.app')
@section('header')

    @parent
	
	<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
	<?php session()->forget('success'); } ?>
	

	<style>
		.clear{clear:both;}
		ul.pagination{float:right; display:inline-block; margin-top:30px;}
		.search-section{float:right; margin:20px 20px 20px;}
		.search-section input{margin-left:20px;}
	</style>
@endsection
@section('content')
				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i>Retur</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Retur</li>
						</ul>

					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<div class="panel panel-flat">
					<?php /* <div class="search-section">
						{{Form::open(array('method' => 'post', 'action' => '\Modules\Transactions\Http\Controllers\TransactionsController@searchData'))}}
						<label>Search:</label><input type="text" name="search">
						{{Form::close()}}
						<div class="clear"></div>
					</div> */ ?>
					<table class="table datatable-basic product">
					<thead>
						<tr>
							<th style="text-align:center;">Id</th>
							<th style="text-align:center;">Customer name</th>
							<th style="text-align:center;">Product List</th>
							<th style="text-align:center;">Total value</th>
							<th style="text-align:center;">Status</th>
							<th style="text-align:center;">Action</th>
						</tr>
					</thead>
					<tbody>	
						<?php if(isset($transactions)){ ?>
							<?php foreach($transactions as $transaction){ ?>
							<tr>
								<td style="text-align:center;"><?php if($transaction->id){echo $transaction->id;}else{ echo '-';  }?></td>
								<td style="text-align:center;"><?php if(isset($transaction->customer_name)){echo $transaction->customer_name;}else{ echo '-';  }?></td>
								<td style="text-align:center;"><?php if(isset($transaction->transactionDetails)){
										foreach($transaction->transactionDetails as $transDetail){
											/*$productName = $transDetail->storage->customProduct->name.' dari '.$transDetail->storage->supplier->name;
											echo $productName.'<br/>';*/
											echo $transDetail->product_name;
										}
									}else{ echo '-';  }?></td>
								<td style="text-align:center;"><?php if($transaction->transactionDetails){
										$totalValue = 0;
										foreach($transaction->transactionDetails as $transDetail){
											$value = $transDetail->qty * $transDetail->value;
											$disc = $transDetail->disc / 100 * $value;
											$value = $value - $disc;
											$totalValue = $totalValue + $value;
										}
										echo $totalValue;
									}else{ echo '-';  }?></td>
								<td style="text-align:center;"><?php if($transaction->status){ echo $transaction->status; } ?></td>	
								<td style="text-align:center;">
									<ul class="icons-list">
										<li >
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-cog7"></i>
												<span class="caret"></span>
											</a>
											<ul class="dropdown-menu dropdown-menu-right">
												<li class="dropdown-header">Options</li>
												<li><a href="{{ route('transactions.editRetur',$transaction->id) }}"><i class="icon-pencil7"></i>Retur</a></li>
											</ul>
										</li>
									</ul>
								</td>
							</tr>
							<?php } ?>
						<?php } ?>
					</tbody>
					</table>
					<?php /*<ul class="pagination">
						<li><a href="{{route('transactions.pagination',1)}}"><<</a></li>
						<?php if(!isset($currentPage)){ $currentPage = 1; } if($totalPage > 10){$maxPage = $currentPage + 9;}else{$maxPage = $totalPage; } for($x = $currentPage; $x <= $maxPage; $x++){ ?>
						<li><a href="{{route('transactions.pagination',$x)}}"><?php echo $x; ?></a></li>
						<?php } ?> 
						<li><a href="{{route('transactions.pagination',$totalPage)}}">>></a></li>
					</ul> */ ?>		
					</div>
				</div>

@endsection