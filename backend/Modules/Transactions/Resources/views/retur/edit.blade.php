@extends('layouts.app')

@section('header')
    @parent
    <!-- JS Extra for form -->
 	
 	<link href="<?php echo asset('assets/css/extras/animate.min.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo Module::asset('transactions:css/jquery-ui.min.css'); ?>" rel="stylesheet" type="text/css">

	<script type="text/javascript" src="<?php echo Module::asset('transactions:js/jquery-ui.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo Module::asset('transactions:js/pages/edit.js'); ?>"></script>


 <style>
 	.clear{clear:both}

 </style>
@endsection
@section('content')
<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Retur</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Retur</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<div class="content">
				<div class="panel panel-flat">
				<div class="panel-body">
					@include('transactions::retur._form', ['action' => array('\Modules\Transactions\Http\Controllers\TransactionsController@updateRetur',$transaction->id),'method' => 'put'])
				</div>
				</div>
				</div>
@endsection