<?php
	use \Illuminate\Support\Facades\Input;
	if(!isset($method)){
		$method = 'post';
	}

?>

{{ Form::open(array('action' => $action, 'method' => $method, 'class' => 'form-vertical')) }}
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Order Items</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <div class="row">
            <div class="col-md-12">
                <fieldset>
                    <div class="row">
                        <div>
							<div class="row">
                                <legend class="text-semibold"><i class="icon-cart5 position-left"></i> Items List</legend>
								<div class="table-responsive">
									<table class="table table-hover table-framed" id="order-detail-table">
										<thead>
										<tr>
											<th class="col-xs-3">Product Name</th>
											<th class="col-xs-2">Product SKU</th>
											<th class="col-xs-2">Product Supplier</th>
											<th class="col-xs-2">Qty</th>
											<th class="col-xs-2">Price</th>
											<th class="col-xs-2">Satuan</th>
											<th class="col-xs-2">Discount</th>
											<th class="col-xs-2">Total</th>
											<th class="col-xs-2">Retur</th>
										</tr>
										</thead>
										<tbody>
										<?php
										$i = 0;
                                        if(isset($transaction->transactionDetails)): $allTotal = 0;
                                            foreach($transaction->transactionDetails as $detail){
                                            $i++; if($detail->status != \Modules\Transactions\Entities\TransactionDetail::RETUR){ ?>
                                            <tr id="order_item_{{ $detail->id }}" data-row="{{ $i }}">
                                                <td>
                                                    <?php echo $detail->storage->customProduct->name; ?>
                                                </td>
                                                <td>
                                                    <?php echo $detail->storage->sku; ?>
                                                </td>
                                                <td>
                                                    <?php echo $detail->storage->supplier->name; ?>
                                                </td>
                                                <td>
                                                    <?php echo $detail->qty; ?>
                                                </td>
                                                <td id="order_detail_total">
                                                    <?php echo $detail->value;?>
                                                </td>
                                                <td>
                                                    <?php echo $detail->satuan; ?>
                                                </td>
                                                <td>
                                                    <?php echo $detail->disc; ?>
                                                </td>
                                                <td class="text-center">
													<?php 

													$total = $detail->qty * $detail->value;
													$disc = $detail->disc / 100 * $total;
													$total = $total - $disc;
													echo $total; 

													$allTotal = $allTotal + $total;
													?>
                                                </td>
                                                <td>
                                                	<?php if($detail->qty > 0){?>
                                                	<input type="number" name="products[<?php echo $i; ?>][retur]" min="0" max="<?php echo $detail->qty;?>">
                                                	<input type="hidden" name="products[<?php echo $i; ?>][details_id]" value="<?php echo $detail->id; ?>">
                                                	<?php }else{
                                                		echo 'No product to retur';
                                                	}?>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                            <?php }
                                        endif; ?>
										</tbody>
									</table>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 col-md-offset-6" style="margin-top:20px;">
									<fieldset>
										<legend class="text-semibold"><i class="icon-coin-dollar position-left"></i> Total</legend>
										<div class="row">
											<div class="col-md-8 text-right">
												Total
											</div>
											<div class="col-md-4 text-bold text-right" id="total_order_all">					
												<!-- {{ number_format(isset($order)?$order->total_amount:0,2) }} -->
												{{$allTotal}}
											</div>
										</div>
									</fieldset>
								</div>
							</div>
                        </div>
                    </div>
                </fieldset>
            </div>
            </div>
            <div class="row">
			<div class="col-md-12">
				<div class="form-group">
					{{Form::label('Terbilang Total')}}
					{{Form::text('terbilang',old('terbilang', isset($data['terbilang'])?$data['terbilang']:null),array('class'=>'form-control'))}}
				</div>
			</div>
		</div>
        </div>
    </div>
    
</div>
@if(!isset($order))
<div class="text-right">
	<button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
</div>
@endif


{{ Form::close() }}