<?php

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Transactions\Http\Controllers'], function()
{
	//Route::get('/', 'TransactionsController@index');
	Route::get('transactions/createInvoice/{id}','TransactionsController@createInvoice')->name('transactions.createInvoice');
	Route::post('transactions/storeInvoice','TransactionsController@storeInvoice')->name('transactions.storeInvoice');

	Route::get('transactions/createSJ/{id}','TransactionsController@createSJ')->name('transactions.createSJ');
	Route::get('transactions/createCustomSJ','TransactionsController@createCustomSJ')->name('transactions.createCustomSJ');

	Route::get('transactions/createCustomInvoice','TransactionsController@createCustomInvoice')->name('transactions.createCustomInvoice');
	Route::any('transactions/displayCustomInvoice','TransactionsController@displayCustomInvoice')->name('transactions.displayCustomInvoice');

	Route::get('transactions/changeStatus/{id}/{status}','TransactionsController@changeStatus')->name('transactions.changeStatus');

	Route::get('transactions/destroyTransaction/{id}','TransactionsController@destroyTransaction')->name('transactions.destroyTransaction');

	Route::get('transactions/pagination/{currentPage}/{type}','TransactionsController@pagination')->name('transactions.pagination');
	Route::post('transactions/searchData','TransactionsController@searchData')->name('transactions.searchData');
	Route::get('transactions/filterOutstanding','TransactionsController@filterOutstanding')->name('transactions.filterOutstanding');
	Route::get('transactions/paginationOutstanding/{currentPage}','TransactionsController@paginationOutstanding')->name('transactions.paginationOutstanding');
	Route::get('transactions/ajaxTotalValue','TransactionsController@ajaxTotalValue')->name('transactions.ajaxTotalValue');

	Route::post('transactions/searchDataBeli','TransactionsController@searchDataBeli')->name('transactions.searchDataBeli');

	Route::get('transactions/retur','TransactionsController@retur')->name('transactions.retur');
	Route::get('transactions/editRetur/{id}','TransactionsController@editRetur')->name('transactions.editRetur');
	Route::put('transactions/{id}/updateRetur','TransactionsController@updateRetur')->name('transactions.updateRetur');

	
	Route::get('transactions/importJual','TransactionsController@importJual')->name('transaction.importJual');
	Route::post('transactions/uploadJual','TransactionsController@uploadJual')->name('transactions.uploadJual');

	Route::get('transactions/importBeli','TransactionsController@importBeli')->name('transaction.importBeli');
	Route::post('transactions/uploadBeli','TransactionsController@uploadBeli')->name('transactions.uploadBeli');

	Route::get('transactions/listPembelian','TransactionsController@listPembelian')->name('transactions.listPembelian');
	Route::get('transactions/createPembelian','TransactionsController@createPembelian')->name('transactions.createPembelian');
	Route::post('transactions/storePembelian','TransactionsController@storePembelian')->name('transactions.storePembelian');

	Route::resource('transactions','TransactionsController');
});