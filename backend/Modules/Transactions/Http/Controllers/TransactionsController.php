<?php

namespace Modules\Transactions\Http\Controllers;

use Nwidart\Modules\Routing\Controller;
use Modules\Transactions\Entities\Transaction;
use Modules\Transactions\Entities\TransactionDetail;
use Modules\Storage\Entities\CustomProduct;
use Modules\Storage\Entities\Storage;
use Modules\Storage\Entities\StorageLog;
use Modules\Accountants\Entities\Accountant;
use Modules\Accountants\Entities\AccountantTransaction;
use Modules\Users\Entities\User;
use Modules\Users\Entities\Customer;
use Illuminate\Http\Request;
use PDF;


class TransactionsController extends Controller {

	public function index()
	{
		$transactions = Transaction::with(array('accountTransaction','customer','transactionDetails'=> function($query){
			$query->where('status','<>',TransactionDetail::RETUR);
		},'transactionDetails.storage.customProduct','transactionDetails.storage.supplier'))->where('type','=', Transaction::JUAL)->orderBy('id','desc')->paginate(15);
		//dd($transactions[0]->accountTransaction);
		$totalTransactions = Transaction::where('type','=', Transaction::JUAL)->count();
		$totalPage = (int)ceil($totalTransactions / 15);
		return view('transactions::index',['transactions' => $transactions,'totalPage' => $totalPage, 'type' => \Modules\Transactions\Entities\Transaction::JUAL]);
	}

	public function create(){
		$type = Transaction::JUAL;
		return view('transactions::transactions.create',['type' => $type]);
	}

	public function store(Request $request){

		$data = $request->all();
		//dd($data);
		$transaction = Transaction::saveTransaction(null,$data);
		if($transaction['status'] == false){
			//return redirect()->route('transactions.create')->with('success',$transaction['message']);
			session(['success' => $transaction['message']]);
			if(isset($transaction['productData'])){
				return view('transactions::transactions.create',['data' => $data, 'storageCheck' => $transaction['productData']]);
			}else{
				return view('transactions::transactions.create',['data' => $data]);
			}
		}
		//dd($transaction);
		return redirect()->route('transactions.index')->with('success','Successfully add new transaction');
	}

	public function edit($id){
		$transaction = Transaction::with('transactionDetails.storage.customProduct','transactionDetails.storage.supplier','customer')->find($id);
		$type = Transaction::JUAL;
		return view('transactions::transactions.edit',['transaction'=>$transaction,'type'=>$type]);

	}

	public function update($id, Request $request){
		$data = $request->all();
		$transaction = Transaction::saveTransaction($id,$data);

		return redirect()->route('transactions.index')->with('success','Successfully edit transaction');
	}

	public function delete($id){

	}

	public function searchData(Request $request){
		$data = $request->all();
		//dd($data);
		$transactions = array();

		$transactionDatas = Transaction::with(array('accountTransaction','customer'=> function($query) use ($data){
			$query->where('first_name','like','%'.$data['search'].'%')->orWhere('last_name','like','%'.$data['search'].'%');
		},'transactionDetails.storage.customProduct'=> function($query) use ($data){
			$query->where('name','like','%'.$data['search'].'%');
		},'transactionDetails.storage.supplier' => function($query) use ($data){
			$query->where('name','like','%'.$data['search'].'%');
		}))->where('type','=',Transaction::JUAL)->get();

		$x = 0;
		foreach($transactionDatas as $transaction){
			if(isset($transaction->customer) && $transaction->customer->count() > 0 ){
				$transactionId[$x] = $transaction->id;
				$x++;
			}else{
				foreach($transaction->transactionDetails as $transactionDetail){
					if(isset($transactionDetail->storage->customProduct) && $transactionDetail->storage->customProduct->count() > 0){
						$transactionId = $transaction->id;
						$x++;
						break;
					}elseif(isset($transactionDetail->storage->supplier) && $transactionDetail->storage->supplier->count() > 0){
						$transactionId = $transaction->id;
						$x++;
						break;
					}
				}
			}
		}
		
		if(!isset($transactionId)){
			return redirect()->route('transactions.index')->with('success','Data tidak ditemukan');
		}
		$transactions = Transaction::with('accountTransaction','customer','transactionDetails.storage.customProduct','transactionDetails.storage.supplier')->whereIn('id', $transactionId)->paginate(15);

		$totalTransactions = Transaction::count();
		$totalPage = (int)ceil($totalTransactions / 15);
		if($transactions->count() > 0){
			return view('transactions::index',['transactions' => $transactions, 'totalPage' => $totalPage, 'type' => \Modules\Transactions\Entities\Transaction::JUAL]);
		}else{
			$transactions = Transaction::with('accountTransaction','customer','transactionDetails.storage.customProduct','transactionDetails.storage.supplier')->paginate(15);
			return view('transactions::index',['transactions' => $transactions, 'totalPage' => $totalPage, 'type' => \Modules\Transactions\Entities\Transaction::JUAL]);
		}
	}
	
	public function searchDataBeli(Request $request){
		$data = $request->all();
		//dd($data);
		$transactions = array();

		$transactionDatas = Transaction::with(array('accountTransaction','customer'=> function($query) use ($data){
			$query->where('first_name','like','%'.$data['search'].'%')->orWhere('last_name','like','%'.$data['search'].'%');
		},'transactionDetails.storage.customProduct'=> function($query) use ($data){
			$query->where('name','like','%'.$data['search'].'%');
		},'transactionDetails.storage.supplier' => function($query) use ($data){
			$query->where('name','like','%'.$data['search'].'%');
		}))->where('type','=',Transaction::BELI)->get();

		$x = 0;
		foreach($transactionDatas as $transaction){
			if(isset($transaction->customer) && $transaction->customer->count() > 0 ){
				$transactionId[$x] = $transaction->id;
				$x++;
			}else{
				foreach($transaction->transactionDetails as $transactionDetail){
					if(isset($transactionDetail->storage->customProduct) && $transactionDetail->storage->customProduct->count() > 0){
						$transactionId[$x] = $transaction->id;
						$x++;
						break;
					}elseif(isset($transactionDetail->storage->supplier) && $transactionDetail->storage->supplier->count() > 0){
						$transactionId[$x] = $transaction->id;
						$x++;
						break;
					}
				}
			}
		}
		if(!isset($transactionId)){
			return redirect()->route('transactions.listPembelian')->with('success','Data tidak ditemukan');
		}
		$transactions = Transaction::with('accountTransaction','customer','transactionDetails.storage.customProduct','transactionDetails.storage.supplier')->whereIn('id', $transactionId)->paginate(15);

		$totalTransactions = Transaction::count();
		$totalPage = (int)ceil($totalTransactions / 15);
		if($transactions->count() > 0){
			return view('transactions::index',['transactions' => $transactions, 'totalPage' => $totalPage, 'type' => \Modules\Transactions\Entities\Transaction::BELI]);
		}else{
			$transactions = Transaction::with('accountTransaction','customer','transactionDetails.storage.customProduct','transactionDetails.storage.supplier')->paginate(15);
			return view('transactions::index',['transactions' => $transactions, 'totalPage' => $totalPage, 'type' => \Modules\Transactions\Entities\Transaction::BELI]);
		}
	}

	public function pagination($currentPage,$type){
		$x = 1;
		$i = 1;
		$z = 15;
		$a = 0;
		$transactions = Transaction::with(array('accountTransaction','customer','transactionDetails'=> function($query){
			$query->where('status','<>',TransactionDetail::RETUR);
		},'transactionDetails.storage.customProduct','transactionDetails.storage.supplier'))->where('type','=', $type)->orderBy('id','desc')->get();

		$transactionDatas = array();
		foreach($transactions as $transaction){
			if($x == $currentPage){
				$transactionDatas[$a] = $transaction;
				$a++;
			}

			if($i == $z){
				$x++;
				$z = $z + 15;
			}
			$i++;
		}
		//dd($productDatas);
		$totalTransactions = Transaction::where('type','=', $type)->count();
		$totalPage = (int)ceil($totalTransactions / 15);

		return view('transactions::index',['transactions' => $transactionDatas,'totalPage' => $totalPage,'currentPage' => $currentPage, 'type' => \Modules\Transactions\Entities\Transaction::JUAL]);
	}

	public function destroyTransaction($id){
		$transaction = Transaction::with('accountTransaction.transactionChild','transactionDetails.storage')->find($id);
		//dd($transaction);
		foreach($transaction->transactionDetails as $transactionDetail){
			$transactionDetail->storage->qty = $transactionDetail->storage->qty + $transactionDetail->qty;
			$transactionDetail->storage->save();
			$transactionDetail->delete();
		}
		$transaction->accountTransaction->transactionChild[0]->delete();
		$transaction->accountTransaction->delete();
		$transaction->delete();

		return redirect()->route('transactions.index')->with('success','successfully delete transactions');
	}

	public function filterOutstanding(){
		$transactions = Transaction::with(array('accountTransaction','customer','transactionDetails'=> function($query){
			$query->where('status','<>',TransactionDetail::RETUR);
		},'transactionDetails.storage.customProduct','transactionDetails.storage.supplier'))->where('type','=', Transaction::JUAL)->where('status','<>',Transaction::COMPLETED)->where('status','<>',Transaction::BACKUP)->orderBy('id','desc')->paginate(15);
		//dd($transactions[0]->accountTransaction);
		$totalTransactions = Transaction::where('type','=', Transaction::JUAL)->where('status','<>',Transaction::COMPLETED)->where('status','<>',Transaction::BACKUP)->count();

		$totalPage = (int)ceil($totalTransactions / 15);

		return view('transactions::index',['transactions' => $transactions,'totalPage' => $totalPage, 'type' => \Modules\Transactions\Entities\Transaction::JUAL, 'checkOutstanding' => true]);
	}

	public function paginationOutstanding($currentPage){
		$x = 1;
		$i = 1;
		$z = 15;
		$a = 0;
		$transactions = Transaction::with(array('accountTransaction','customer','transactionDetails'=> function($query){
			$query->where('status','<>',TransactionDetail::RETUR);
		},'transactionDetails.storage.customProduct','transactionDetails.storage.supplier'))->where('type','=', Transaction::JUAL)->where('status','<>',Transaction::COMPLETED)->where('status','<>',Transaction::BACKUP)->orderBy('id','desc')->get();

		$transactionDatas = array();
		foreach($transactions as $transaction){
			if($x == $currentPage){
				$transactionDatas[$a] = $transaction;
				$a++;
			}

			if($i == $z){
				$x++;
				$z = $z + 15;
			}
			$i++;
		}
		//dd($productDatas);
		$totalTransactions = Transaction::where('type','=', Transaction::JUAL)->where('status','<>',Transaction::COMPLETED)->where('status','<>',Transaction::BACKUP)->count();
		$totalPage = (int)ceil($totalTransactions / 15);

		return view('transactions::index',['transactions' => $transactionDatas,'totalPage' => $totalPage,'currentPage' => $currentPage, 'type' => \Modules\Transactions\Entities\Transaction::JUAL, 'checkOutstanding' => true]);
	}

	public function ajaxTotalValue(){
		$transactions = Transaction::with(array('accountTransaction','customer','transactionDetails'=> function($query){
			$query->where('status','<>',TransactionDetail::RETUR);
		},'transactionDetails.storage.customProduct','transactionDetails.storage.supplier'))->where('type','=', Transaction::JUAL)->where('status','<>',Transaction::COMPLETED)->where('status','<>',Transaction::BACKUP)->orderBy('id','desc')->get();

		$datas = array();
		$x = 0;
		$totalAll = 0;
		foreach($transactions as $transaction){
			$totalValue = 0;
			foreach($transaction->transactionDetails as $transactionDetail){
				$subTotal = $transactionDetail->qty * $transactionDetail->value;
				$totalValue = $totalValue + $subTotal;
			}
			$datas[$x]['name'] = $transaction->customer_name;
			$datas[$x]['totalValue'] = $totalValue;
			$totalAll = $totalAll + $totalValue;
			$x++;
		}
		$datas['totalAll'] = $totalAll;
		return $datas;
	}


	public function changeStatus($id,$status){
		$transaction = Transaction::find($id);
		$transaction->status = $status;
		$transaction->save();

		return redirect()->route('transactions.index')->with('transaction succesfully paid');
	}

	public function createInvoice($id){
		$transaction = Transaction::with('transactionDetails.storage.customProduct','transactionDetails.storage.supplier','customer')->find($id);
		//dd($transaction);
		$html = '<html>
				<head>
					<style>
						.clear{clear:both;}
						.top-header{with:100%; position:relative; text-align:center; display:block;}
						.top-header h2{font-size:18px; letter-spacing:2px; text-transform:uppercase; margin:0 0 15px;}
						.top-header h3{font-size:16px; text-transfomr:uppercase; margin:0 0 15px;}
						.top-header	p{font-size:12px; margin:0 0 15px; text-transform:uppercase;}
						.top-header .coloumn{width:30%; display:inline-block;}
						.top-header .coloumn .first{float:left;}
						.top-header .coloumn .second{position:absolute; left:50%; transform:translateX(-60%); padding-top:25px;}
						.top-header .coloumn .third{text-align:left; width:150px;}
						.top-header .coloumn .third label{width:100px; display:inline-block;}
						
						.table-desc{display:block; width:100%;}
						.table-desc p{font-size:12px; display:inline-block; margin:0 100px 0 0; width:auto; float:left; padding:0; line-height:1;}
						.table-desc p{text-align:left}
						.table-desc p label{width:70px; display:inline-block;}
						
						table{width:100%;}
						table thead th{ border-top:1px dashed #000; border-bottom:1px dashed #000; padding:10px 0;}
						.bottom-notice{position:absolute; bottom:0; left:0; padding-top:10px; border-top:1px dashed #000;}

						.bottom-notice .allTotal{float:right; }
						.bottom-notice .allTotal p{line-height:1}
						.bottom-notice .allTotal p label{width:80px; display:inline-block; }
						.bottom-notice .allTotal p i{width:40px; display:inline-block; font-style:none;}
						.bottom-notice .allTotal p a{width:40px;  display:inline-block; }
					</style>
				</head>
				<body>
					<div class="top-header">
						<div class="coloumn first">
							<h2>Sido Gede</h2>
							<p>Telp. (031) 3814619</p>
							<p>Surabaya</p>
						</div>
						<div class="coloumn second">
							<h3>Faktur Penjualan</h3>
							<h3>0317-0063</h3>
						</div>
						<div class="coloumn third">
							<p>Surabaya, '.date('d-m-Y').' / '.$transaction->customer->id.'</p>
							<p><label>Kpd.yth</label>: '.$transaction->customer->first_name.'</p>
							<p><label>Alamat</label>: '.$transaction->address.'</p>
							<p><label>Kota</label>: '.$transaction->city.'</p>
						</div>
						<div class="clear"></div>
					</div>
					<div class="table-desc">
						<p><label>No.Order</label>: '.$transaction->id.'</p>
						<p><label>Pembayaran</label>: '.date('d-m-Y',strtotime($transaction->dueDate)).'</p>
						<div class="clear"></div>
					</div>
					<table>
						<thead>
							<tr>
								<th style="text-align:center">No</th>
								<th style="text-align:center">Nama-Barang</th>
								<th style="text-align:center">Qty</th>
								<th style="text-align:center">Sat</th>
								<th style="text-align:center">Harga</th>
								<th style="text-align:center">Dis(%)</th>
								<th style="text-align:center">Total (RP)</th>
							</tr>
						</thead>
						<tbody>';

			$centerHtml = '';
			$x = 1;
			$totalAll = 0;
			foreach($transaction->transactionDetails as $detail){
				//dd($detail);
				$total = 0;
				$centerHtml .= '<tr>';

				$centerHtml .= '<td style="text-align:center">'.$x.'</td>';
				$centerHtml .= '<td style="text-align:center">'.$detail->storage->customProduct->name.'</td>';
				$centerHtml .= '<td style="text-align:center">'.$detail->qty.'</td>';
				$centerHtml .= '<td style="text-align:center">'.$detail->satuan.'</td>';
				$centerHtml .= '<td style="text-align:center">'.$detail->value.'</td>';
				$centerHtml .= '<td style="text-align:center">'.$detail->disc.'</td>';

				$total = $detail->qty * $detail->value;
				$disc = $detail->disc / 100 * $total;
				$total = $total - $disc;

				$centerHtml .= '<td style="text-align:center">'.$total.'</td>';

				$centerHtml .= '</tr>';

				$totalAll = $totalAll + $total;

			$x++;
			}

			$html .= $centerHtml;
			$html .= '</tbody></table>';
			$html .= '<div class="bottom-notice">
			<div class="total section">
				<div class="allTotal">
					<p><label>Jumlah</label><i>:</i><a>'.$totalAll.'</a></p>
					<p><label>TOTAL</label><i>:</i><a>'.$totalAll.'</a></p>
				</div>
			</div>
			<p>'.$transaction->terbilang.'</p>
			<p>Transfer Ke</p>
			<p>POETRI GLORIA SAMATHA</p>
			<p>BCA Kapas Krampung-Surabaya</p>
			<p>No. A/C 101-0374-072</p>
			</div>
			</body></html>';
			
		return PDF::loadHTML($html)->stream();
		//return view('transactions::invoices.create',['transaction'=>$transaction]);	
	}


	public function createSJ($id){
		$transaction = Transaction::with('transactionDetails.storage.customProduct','transactionDetails.storage.supplier','customer')->find($id);
		$html = '<html>
				<head>
					<style>
						.clear{clear:both;}
						.top-header{with:100%; position:relative; text-align:center; display:block;}
						.top-header h2{font-size:18px; letter-spacing:2px; text-transform:uppercase; margin:0 0 15px;}
						.top-header h3{font-size:16px; text-transfomr:uppercase; margin:0 0 15px;}
						.top-header	p{font-size:12px; margin:0 0 15px; text-transform:uppercase;}
						.top-header .coloumn{width:30%; display:inline-block;}
						.top-header .coloumn .first{float:left;}
						.top-header .coloumn .second{position:absolute; left:50%; transform:translateX(-60%); padding-top:25px;}
						.top-header .coloumn .third{text-align:left; width:150px;}
						.top-header .coloumn .third label{width:100px; display:inline-block;}
						
						.table-desc{display:block; width:100%;}
						.table-desc tbody{height:800px; display:inline-block;}
						.table-desc p{font-size:12px; display:inline-block; margin:0 100px 0 0; width:auto; float:left; padding:0; line-height:1;}
						.table-desc p{text-align:left}
						.table-desc p label{width:70px; display:inline-block;}
						
						table{width:100%;}
						table thead th{ border-top:1px dashed #000; border-bottom:1px dashed #000; padding:10px 0;}
						table tbody{border-bottom:1px dashed #000;}
						table tr td{padding:20px 0;}

						.bottom-notice{position:absolute; bottom:0; left:0; padding-top:10px; border-top:1px dashed #000;}

						.bottom-notice .allTotal{float:right; }
						.bottom-notice .allTotal p{line-height:1}
						.bottom-notice .allTotal p label{width:80px; display:inline-block; }
						.bottom-notice .allTotal p i{width:40px; display:inline-block; font-style:none;}
						.bottom-notice .allTotal p a{width:40px;  display:inline-block; }
					</style>
				</head>
				<body>
					<div class="top-header">
						<div class="coloumn first">
							<h2>Sido Gede</h2>
							<p>Telp. (031) 3814619</p>
							<p>Surabaya</p>
						</div>
						<div class="coloumn second">
							<h3>Surat Jalan</h3>
							<h3>1216-0029</h3>
						</div>
						<div class="coloumn third">
							<p>Surabaya, '.date('d-m-Y').' / '.$transaction->customer->id.'</p>
							<p><label>Kpd.yth</label>: '.$transaction->customer->first_name.'</p>
							<p><label>Alamat</label>: '.$transaction->address.'</p>
							<p><label>Kota</label>: '.$transaction->city.'</p>
						</div>
						<div class="clear"></div>
					</div>
					<table>
						<thead>
							<tr>
								<th style="text-align:center">No</th>
								<th style="text-align:center">Colly</th>
								<th style="text-align:center">Item</th>
								<th style="text-align:center">Qty</th>
								<th style="text-align:center">Keterangan</th>
							</tr>
						</thead>
						<tbody>';

		$x = 1;
		$totalQty = 0;
		foreach($transaction->transactionDetails as $detail){
			if($detail->status != TransactionDetail::RETUR){ $totalQty = $totalQty + $detail->qty;
				$html .='<tr>
							<td style="text-align:center">'.$x.'</td>
							<td style="text-align:center">'.$detail->qty.' '.$detail->satuan.'</td>
							<td style="text-align:center">'.$detail->storage->customProduct->name.'</td>
							<td style="text-align:center">'.$detail->qty.'</td>
							<td style="text-align:center"></td>
						</tr>';
			}
		}

		$html .= '</tbody>
				<tfoot>
					<tr><td colspan=5></td></tr>
					<tr><td colspan=5></td></tr>
					<tr>
						<td style="text-align:center"></td>
						<td style="text-align:center"></td>
						<td style="text-align:center">Tanda Terima</td>
						<td style="text-align:center"></td>
						<td style="text-align:center">Hormat Kami</td>
					</tr>
				</tfoot>
				</table>
				</body>';
							
		return PDF::loadHTML($html)->stream();
	}

	public function createCustomInvoice(){
		//return view('transactions::invoices.create');
		$html = '<html>
				<head>
					<style>
						.clear{clear:both;}
						.top-header{with:100%; position:relative; text-align:center; display:block;}
						.top-header h2{font-size:18px; letter-spacing:2px; text-transform:uppercase; margin:0 0 15px;}
						.top-header h3{font-size:16px; text-transfomr:uppercase; margin:0 0 15px;}
						.top-header	p{font-size:12px; margin:0 0 15px; text-transform:uppercase;}
						.top-header .coloumn{width:30%; display:inline-block;}
						.top-header .coloumn .first{float:left;}
						.top-header .coloumn .second{position:absolute; left:50%; transform:translateX(-60%); padding-top:25px;}
						.top-header .coloumn .third{text-align:left; width:150px;}
						.top-header .coloumn .third label{width:100px; display:inline-block;}
						
						.table-desc{display:block; width:100%;}
						.table-desc p{font-size:12px; display:inline-block; margin:0 100px 0 0; width:auto; float:left; padding:0; line-height:1;}
						.table-desc p{text-align:left}
						.table-desc p label{width:70px; display:inline-block;}
						
						table{width:100%;}
						table thead th{ border-top:1px dashed #000; border-bottom:1px dashed #000; padding:10px 0;}
						.bottom-notice{position:absolute; bottom:0; left:0; padding-top:10px; border-top:1px dashed #000;}

						.bottom-notice .allTotal{float:right; margin-right:125px;}
						.bottom-notice .allTotal p{line-height:1}
						.bottom-notice .allTotal p label{width:80px; display:inline-block; }
						.bottom-notice .allTotal p i{width:40px; display:inline-block; font-style:none;}
						.bottom-notice .allTotal p a{width:40px;  display:inline-block; }
					</style>
				</head>
				<body>
					<div class="top-header">
						<div class="coloumn first">
							<h2>Sido Gede</h2>
							<p>Telp. (031) 3814619</p>
							<p>Surabaya</p>
						</div>
						<div class="coloumn second">
							<h3>Faktur Penjualan</h3>
							<h3>0317-0063</h3>
						</div>
						<div class="coloumn third">
							<p>Surabaya, '.date('d-m-Y').' / </p>
							<p><label>Kpd.yth</label>: </p>
							<p><label>Alamat</label>: </p>
							<p><label>Kota</label>: </p>
						</div>
						<div class="clear"></div>
					</div>
					<div class="table-desc">
						<p><label>No.Order</label>: </p>
						<p><label>Pembayaran</label>: </p>
						<div class="clear"></div>
					</div>
					<table>
						<thead>
							<tr>
								<th style="text-align:center">No</th>
								<th style="text-align:center">Nama-Barang</th>
								<th style="text-align:center">Qty</th>
								<th style="text-align:center">Sat</th>
								<th style="text-align:center">Harga</th>
								<th style="text-align:center">Dis(%)</th>
								<th style="text-align:center">Total (RP)</th>
							</tr>
						</thead>
						<tbody>';
		$html .= '</tbody></table>';
		$html .= '<div class="bottom-notice">
		<div class="total section">
			<div class="allTotal">
				<p><label>Jumlah</label>:</p>
				<p><label>TOTAL</label>:</p>
			</div>
		</div>
		<p style="margin-top:100px; display:inline-block;">Transfer Ke</p>
		<p>POETRI GLORIA SAMATHA</p>
		<p>BCA Kapas Krampung-Surabaya</p>
		<p>No. A/C 101-0374-072</p>
		</div>
		</body></html>';
			
		return PDF::loadHTML($html)->stream();
	}

	public function createCustomSJ(){
		$html = '<html>
				<head>
					<style>
						.clear{clear:both;}
						.top-header{with:100%; position:relative; text-align:center; display:block;}
						.top-header h2{font-size:18px; letter-spacing:2px; text-transform:uppercase; margin:0 0 15px;}
						.top-header h3{font-size:16px; text-transfomr:uppercase; margin:0 0 15px;}
						.top-header	p{font-size:12px; margin:0 0 15px; text-transform:uppercase;}
						.top-header .coloumn{width:30%; display:inline-block;}
						.top-header .coloumn .first{float:left;}
						.top-header .coloumn .second{position:absolute; left:50%; transform:translateX(-60%); padding-top:25px;}
						.top-header .coloumn .third{text-align:left; width:150px;}
						.top-header .coloumn .third label{width:100px; display:inline-block;}
						
						.table-desc{display:block; width:100%;}
						.table-desc p{font-size:12px; display:inline-block; margin:0 100px 0 0; width:auto; float:left; padding:0; line-height:1;}
						.table-desc p{text-align:left}
						.table-desc p label{width:70px; display:inline-block;}
						
						table{width:100%;}
						table thead th{ border-top:1px dashed #000; border-bottom:1px dashed #000; padding:10px 0;}
						.bottom-notice{position:absolute; bottom:0; left:0; padding-top:10px; border-top:1px dashed #000;}

						.bottom-notice .allTotal{float:right; }
						.bottom-notice .allTotal p{line-height:1}
						.bottom-notice .allTotal p label{width:80px; display:inline-block; }
						.bottom-notice .allTotal p i{width:40px; display:inline-block; font-style:none;}
						.bottom-notice .allTotal p a{width:40px;  display:inline-block; }
					</style>
				</head>
				<body>
					<div class="top-header">
						<div class="coloumn first">
							<h2>Sido Gede</h2>
							<p>Telp. (031) 3814619</p>
							<p>Surabaya</p>
						</div>
						<div class="coloumn second">
							<h3>Surat Jalan</h3>
							<h3>1216-0029</h3>
						</div>
						<div class="coloumn third">
							<p>Surabaya, '.date('d-m-Y').' / </p>
							<p><label>Kpd.yth</label>: </p>
							<p><label>Alamat</label>: </p>
							<p><label>Kota</label>: </p>
						</div>
						<div class="clear"></div>
					</div>
					<table>
						<thead>
							<tr>
								<th style="text-align:center">No</th>
								<th style="text-align:center">Colly</th>
								<th style="text-align:center">Item</th>
								<th style="text-align:center">Qty</th>
								<th style="text-align:center">Keterangan</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
				</body>';

				return PDF::loadHTML($html)->stream();
	}

	public function displayCustomInvoice(Request $request){
		$data = $request->all();
		//dd($data);
		$customer = \Sentinel::findById($data['user_id']);
		$html = '<html>
				<head>
					<style>
						.clear{clear:both;}
						.top-header{with:100%; position:relative; text-align:center; display:block;}
						.top-header h2{font-size:18px; letter-spacing:2px; text-transform:uppercase; margin:0 0 15px;}
						.top-header h3{font-size:16px; text-transfomr:uppercase; margin:0 0 15px;}
						.top-header	p{font-size:12px; margin:0 0 15px; text-transform:uppercase;}
						.top-header .coloumn{width:30%; display:inline-block;}
						.top-header .coloumn .first{float:left;}
						.top-header .coloumn .second{position:absolute; left:50%; transform:translateX(-60%); padding-top:25px;}
						.top-header .coloumn .third{text-align:left; width:150px;}
						.top-header .coloumn .third label{width:100px; display:inline-block;}
						
						.table-desc{display:block; width:100%;}
						.table-desc p{font-size:12px; display:inline-block; margin:0 100px 0 0; width:auto; float:left; padding:0; line-height:1;}
						.table-desc p{text-align:left}
						.table-desc p label{width:70px; display:inline-block;}
						
						table{width:100%;}
						table thead th{ border-top:1px dashed #000; border-bottom:1px dashed #000; padding:10px 0;}
					</style>
				</head>
				<body>
					<div class="top-header">
						<div class="coloumn first">
							<h2>Sido Gede</h2>
							<p>Telp. (031) 3814619</p>
							<p>Surabaya</p>
						</div>
						<div class="coloumn second">
							<h3>Faktur Penjualan</h3>
							<h3>0317-0063</h3>
						</div>
						<div class="coloumn third">
							<p>Surabaya, '.date('d-m-Y').' / '.$customer->id.'</p>
							<p><label>Kpd.yth</label>: '.$customer->first_name.'</p>
							<p><label>Alamat</label>: '.$data['address'].'</p>
							<p><label>Kota</label>: '.$data['city'].'</p>
						</div>
						<div class="clear"></div>
					</div>
					<div class="table-desc">
						<p><label>No.Order</label>: </p>
						<p><label>Pembayaran</label>: '.date('d-m-Y',strtotime($data['dueDate'])).'</p>
						<div class="clear"></div>
					</div>
					<table>
						<thead>
							<tr>
								<th style="text-align:center">No</th>
								<th style="text-align:center">Nama-Barang</th>
								<th style="text-align:center">Qty</th>
								<th style="text-align:center">Sat</th>
								<th style="text-align:center">Harga</th>
								<th style="text-align:center">Dis(%)</th>
								<th style="text-align:center">Total (RP)</th>
							</tr>
						</thead>
						<tbody>';

			$centerHtml = '';
			$x = 1;
			foreach($data['products'] as $product){
				//dd($detail);
				$total = 0;
				$centerHtml .= '<tr>';

				$centerHtml .= '<td style="text-align:center">'.$x.'</td>';
				$centerHtml .= '<td style="text-align:center">'.$product['name'].'</td>';
				$centerHtml .= '<td style="text-align:center">'.$product['qty'].'</td>';
				$centerHtml .= '<td style="text-align:center">'.$product['satuan'].'</td>';
				$centerHtml .= '<td style="text-align:center">'.$product['price'].'</td>';
				$centerHtml .= '<td style="text-align:center">'.$product['disc'].'</td>';

				$total = $product['qty'] * $product['price'];
				$disc = $product['disc'] / 100 * $total;
				$total = $total - $disc;

				$centerHtml .= '<td style="text-align:center">'.$total.'</td>';

				$centerHtml .= '</tr>';

			$x++;
			}

			$html .= $centerHtml;
			$html .= '</tbody></table></body></html>';

			return PDF::loadHTML($html)->stream();
	}


	public function retur(){
		$transactions = Transaction::with(array('accountTransaction','customer','transactionDetails'=> function($query){
			$query->where('status','<>',TransactionDetail::RETUR);
		},'transactionDetails.storage.customProduct','transactionDetails.storage.supplier'))->get();
		//dd($transactions[0]->accountTransaction);
		$totalTransactions = Transaction::count();
		$totalPage = (int)ceil($totalTransactions / 15);
		return view('transactions::retur.index',['transactions' => $transactions,'totalPage' => $totalPage]);
	}

	public function editRetur($id){
		$transaction = Transaction::with('transactionDetails.storage.customProduct','transactionDetails.storage.supplier','customer')->find($id);
		$x = 0;
		foreach($transaction->transactionDetails as $transactionDetail){
			if($transactionDetail->status == TransactionDetail::RETUR){
				$returData[$x]['transaction_id'] = $transactionDetail->transaction_id;
				$returData[$x]['storage_id'] = $transactionDetail->storage_id;
				$returData[$x]['qty'] = $transactionDetail->qty;
			}
		}

		if(isset($returData)){
			foreach($transaction->transactionDetails as &$transactionDetail){
				foreach($returData as $retur){
					if($transactionDetail->transaction_id == $retur['transaction_id'] && $transactionDetail->storage_id == $retur['storage_id']){
						$transactionDetail->qty = $transactionDetail->qty - $retur['qty'];
					}
				}
				
			}
		}
		return view('transactions::retur.edit',['transaction' => $transaction]);
	}

	public function updateRetur($id, Request $request){
		$data = $request->all();
		//dd($data);
		$transaction = Transaction::find($id);

		foreach($data['products'] as $product){
			if($product['retur'] > 0){

				$transaction->status = TransactionDetail::RETUR;
				$transaction->save();

				$transactionDetail = TransactionDetail::with('storage')->find($product['details_id']);

				$transactionDetail->storage->qty = $transactionDetail->storage->qty + $product['retur'];
				$transactionDetail->storage->save();
				
				$newTransactionDetail = new TransactionDetail();
				$newTransactionDetail->transaction_id = $transactionDetail->transaction_id;
				$newTransactionDetail->storage_id = $transactionDetail->storage_id;
				$newTransactionDetail->product_name = $transactionDetail->product_name;
				$newTransactionDetail->supplier_name = $transactionDetail->supplier_name;
				$newTransactionDetail->qty = $product['retur'];
				$newTransactionDetail->satuan = $transactionDetail->satuan;
				$newTransactionDetail->value = $transactionDetail->value;
				$newTransactionDetail->status = TransactionDetail::RETUR;
				$newTransactionDetail->disc = $transactionDetail->disc;
				$newTransactionDetail->save();

				$totalValue = $newTransactionDetail->value * $newTransactionDetail->qty;
				$account = Accountant::where('name','=','retur')->first();
				$kasAccount = Accountant::where('name','=','kas')->first();

				$accountantTransaction = new AccountantTransaction();
				$accountantTransaction->name = 'Retur '.$newTransactionDetail->product_name.' dari '.$transaction->customer_name;
				$accountantTransaction->desc = "Pengembalian barang";
				$accountantTransaction->account_id = $account->id;
				$accountantTransaction->value = $totalValue;
				$accountantTransaction->save();

				$kasAccountTransaction = new AccountantTransaction();
				$kasAccountTransaction->name = 'kurangi kas';
				$kasAccountTransaction->desc = 'Pengurangan kas';
				$kasAccountTransaction->account_id = $kasAccount->id;
				$kasAccountTransaction->parent_id = $accountantTransaction->id;
				$kasAccountTransaction->value = $totalValue * -1;
				$kasAccountTransaction->save();
			}
		}
	
	return redirect()->route('transactions.retur')->with('success','Successfully retur barang');	
	}

	public function importJual(){
		return view('transactions::transactions.uploadJual');
	}

	public function uploadJual(Request $request){
		$data = $request->all();
		if(isset($data['csv_file'])){
			$handle = fopen($data['csv_file'], 'r');
		}
		$csvArray = array();
		if(isset($handle)){
			$x = 0;
			while (($csv = fgetcsv($handle, 1000, ",")) !== FALSE) {
				//print_r($csv);
				if($x > 0 ){
					//dd($csv);
					$customer = Customer::where('kode_customer','=',$csv[23])->first();

					$transaction = new Transaction();
					if(isset($customer)){
						$transaction->customer_id = $customer->id;
						$transaction->customer_name = $customer->name;
						//$transaction->customer_name = $csv[22];
					}
					$transaction->transDate = date('Y-m-d',strtotime($csv[5]));
					$transaction->dueDate = date('Y-m-d',strtotime($csv[6]));
					$transaction->address = $csv[24];
					$transaction->city = $csv[25];
					$transaction->status = Transaction::BACKUP;
					$transaction->type = Transaction::JUAL;
					$transaction->save();

					$transactionDetail = new TransactionDetail();
					$transactionDetail->transaction_id = $transaction->id;
					$transactionDetail->product_name = $csv[1];
					$transactionDetail->qty = $csv[3];
					$transactionDetail->value = $csv[4];
					$transactionDetail->status = TransactionDetail::BACKUP;
					$transactionDetail->save();

					$totalValue = $transactionDetail->qty * $transactionDetail->value;

					$account = Accountant::where('name','=','pembelian')->first();
					$accountantTrans = new AccountantTransaction();
					$accountantTrans->account_id = $account->id;
					$accountantTrans->name = 'Penjualan '.$csv[1].' dengan id transaksi '.$transaction->id;
					$accountantTrans->value = $totalValue * -1;
					$accountantTrans->save();

					$kas = Accountant::where('name','=','kas')->first();
					$kasAccount = new AccountantTransaction();
					$kasAccount->name = 'tambah kas';
					$kasAccount->account_id = $kas->id;
					$kasAccount->value = $totalValue;
					$kasAccount->parent_id = $accountantTrans->id;
					$kasAccount->save();

					$transaction->account_trans_id = $accountantTrans->id;
					$transaction->save();

				}
			$x++;
			}
			
		}
		return redirect()->route('transactions.index')->with('success','successfully upload transaction');
	}

	public function importBeli(){
		return view('transactions::transactions.uploadBeli');
	}

	public function uploadBeli(Request $request){
		$data = $request->all();
		if(isset($data['csv_file'])){
			$handle = fopen($data['csv_file'], 'r');
		}
		//dd($data['csv_file']);
		$csvArray = array();
		if(isset($handle)){
			$x = 0;
			while (($csv = fgetcsv($handle, 1000, ",")) !== FALSE) {
				if($x > 0 ){
					//print_r($csv); exit;
					$transaction = new Transaction();
					$transaction->customer_name = $csv[2];
					$transaction->transDate = date('Y-m-d',strtotime($csv[7]));
					$transaction->dueDate = date('Y-m-d',strtotime($csv[8]));
					// $transaction->address = $csv[24];
					// $transaction->city = $csv[25];
					$transaction->status = Transaction::BACKUP;
					$transaction->type = Transaction::BELI;
					$transaction->save();

					$transactionDetail = new TransactionDetail();
					$transactionDetail->transaction_id = $transaction->id;
					$transactionDetail->product_name = $csv[1];
					$transactionDetail->qty = $csv[13];
					$transactionDetail->value = $csv[4];
					$transactionDetail->faktur = $csv[10];
					$transactionDetail->status = TransactionDetail::BACKUP;
					$transactionDetail->save();

					$totalValue = $transactionDetail->qty * $transactionDetail->value;

					$account = Accountant::where('name','=','pembelian')->first();
					$accountantTrans = new AccountantTransaction();
					$accountantTrans->account_id = $account->id;
					$accountantTrans->name = 'Pembelian '.$csv[1].' dengan id transaksi '.$transaction->id;
					$accountantTrans->value = $totalValue;
					$accountantTrans->save();

					$kas = Accountant::where('name','=','kas')->first();
					$kasAccount = new AccountantTransaction();
					$kasAccount->name = 'kurangi kas';
					$kasAccount->account_id = $kas->id;
					$kasAccount->value = $totalValue * -1;
					$kasAccount->parent_id = $accountantTrans->id;
					$kasAccount->save();

					$transaction->account_trans_id = $accountantTrans->id;
					$transaction->save();

				}
			$x++;
			}
		}
		return redirect()->route('transactions.index')->with('success','successfully upload transaction');
	}

	public function listPembelian(){
		$transactions = Transaction::with(array('accountTransaction','customer','transactionDetails'=> function($query){
			$query->where('status','<>',TransactionDetail::RETUR);
		},'transactionDetails.storage.customProduct','transactionDetails.storage.supplier'))->where('type','=', Transaction::BELI)->paginate(15);
		//dd($transactions[0]->accountTransaction);
		$totalTransactions = Transaction::where('type','=', Transaction::BELI)->count();
		$totalPage = (int)ceil($totalTransactions / 15);
		//dd($totalPage);
		return view('transactions::index',['transactions' => $transactions,'totalPage' => $totalPage, 'type' => \Modules\Transactions\Entities\Transaction::BELI]);
	}

	public function createPembelian(){
		$type = Transaction::BELI;
		return view('transactions::transactions.create',['type' => $type]);
	}

	public function storePembelian(Request $request){
		$data = $request->all();
		//dd($data);
		$transaction = Transaction::savePembelian(null,$data);
		if($transaction['status'] == false){
			return redirect()->route('transactions.create')->with('success','fail add pembelian');
			session(['success' => $transaction['message']]);
			if(isset($transaction['productData'])){
				return view('transactions::transactions.create',['data' => $data, 'storageCheck' => $transaction['productData']]);
			}else{
				return view('transactions::transactions.create',['data' => $data,'type' => Transaction::BELI]);
			}
		}
		//dd($transaction);
		return redirect()->route('transactions.listPembelian')->with('success','Successfully add new transaction');
	}

}
