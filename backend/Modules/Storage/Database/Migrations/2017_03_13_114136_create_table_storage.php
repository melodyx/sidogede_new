<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStorage extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('storage', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('product_id');
            $table->integer('supplier_id');
            $table->integer('sku');
            $table->decimal('buying_price',15,2);
            $table->decimal('price',15,2);
            $table->integer('qty');
            $table->integer('author_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('storage');
    }

}
