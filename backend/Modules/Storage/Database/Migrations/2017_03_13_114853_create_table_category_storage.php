<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCategoryStorage extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name',255);
            $table->string('slug',255);
            $table->string('address',255);
            $table->string('phone',255);
            $table->string('email',255);
            $table->integer('hutang');
            $table->longtext('feat_image');
            $table->integer('author_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('supplier');
    }

}
