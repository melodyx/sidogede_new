<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCustomProducts extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_products', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name',255);
            //$table->longtext('sku');
            $table->longtext('description');
            $table->longtext('short_description');
            $table->longtext('feat_image');
            $table->integer('author_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('custom_products');
    }

}
