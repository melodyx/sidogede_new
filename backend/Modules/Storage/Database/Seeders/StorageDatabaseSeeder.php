<?php

namespace Modules\Storage\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class StorageDatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		/*\DB::table('custom_products')->insert([
			'name' => 'dummy a',
			'description' => 'lorem ipsum',
			'short_description' => 'lorem ipsum',
		]);*/

		\DB::table('supplier')->insert([
			'name' => 'Global Supplier',
			'slug' => 'global_supplier',
			'email' => 'global_supplier@gmail.com',
		]);

		// \DB::table('supplier')->insert([
		// 	'name' => 'supplier A',
		// 	'slug' => 'supplier_a',
		// 	'address' => 'dummy street',
		// 	'phone' => '123321',
		// 	'email' => 'dummy_supplier@gmail.com',
		// ]);

		// \DB::table('supplier')->insert([
		// 	'name' => 'supplier B',
		// 	'slug' => 'supplier_b',
		// 	'address' => 'dummy street b',
		// 	'phone' => '12332100',
		// 	'email' => 'dummy_supplierb@gmail.com',
		// ]);
		
		$this->call("\Modules\Storage\Database\Seeders\AccountDatabaseSeederTableSeeder");
	}
}
