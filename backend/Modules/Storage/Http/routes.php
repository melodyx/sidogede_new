<?php

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Storage\Http\Controllers'], function()
{
	//Route::get('/', 'StorageController@index');
	Route::any('storage/getAllSuppliers','StorageController@getAllSuppliers')->name('storage.getAllSuppliers');

	Route::any('storage/get/customProducts','StorageController@getCustomProducts')->name('storage.getCustomProducts');
	Route::get('storage/searchProductSupplier/{storage_id}/{product_id}','StorageController@searchProductSupplier')->name('storage.searchProductSupplier');
	Route::get('storage/destroyCustomProducts/{id}','StorageController@destroyCustomProducts')->name('storage.destroyCustomProducts');
	Route::get('storage/pagination/{currentPage}','StorageController@pagination')->name('storage.pagination');
	Route::any('storage/searchData','StorageController@searchData')->name('storage.searchData');

	Route::get('storage/importCustomProducts','StorageController@importCustomProducts')->name('storage.importCustomProducts');
	Route::post('storage/uploadCustomProducts','StorageController@uploadCustomProducts')->name('storage.uploadCustomProducts');
	

	Route::get('storage/importSupplier','StorageController@importSupplier')->name('storage.importCustomProducts');
	Route::post('storage/uploadSupplier','StorageController@uploadSupplier')->name('storage.uploadSupplier');
	//Route::get('storage/exportSupplier','StorageController@exportSupplier')->name('storage.exportSupplier');



	Route::get('storage/supplier','StorageController@supplierIndex')->name('storage.supplier');
	Route::get('storage/addSupplier','StorageController@addSupplier')->name('storage.addSupplier');
	Route::post('storage/storeSupplier','StorageController@storeSupplier')->name('storage.storeSupplier');
	Route::get('storage/editSupplier/{id}','StorageController@editSupplier')->name('storage.editSupplier');
	Route::put('storage/{id}/updateSupplier','StorageController@updateSupplier')->name('storage.updateSupplier');
	Route::get('storage/destroySupplier/{id}','StorageController@destroySupplier')->name('storage.destroySupplier');

	Route::get('storage/transactions/{id}','StorageController@transactions')->name('storage.transactions');
	Route::put('storage/{id}/updateTransactions','StorageController@updateTransactions')->name('storage.updateTransactions');
	Route::resource('storage','StorageController');
});