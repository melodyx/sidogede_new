<?php

namespace Modules\Storage\Http\Controllers;

use Nwidart\Modules\Routing\Controller;
use Modules\Storage\Entities\CustomProduct;
use Modules\Storage\Entities\Storage;
use Modules\Storage\Entities\StorageLog;
use Modules\Accountants\Entities\Accountant;
use Modules\Accountants\Entities\AccountantTransaction;
use Modules\Accountants\Entities\Invoice;
use Modules\Storage\Entities\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;


class StorageController extends Controller {

	public function getAllSuppliers(Request $request){
		$term = $request['term'];
        //$users = User::where('first_name', 'like', '%'.$term.'%')->orWhere('last_name', 'like', '%'.$term.'%')->get();
        $suppliers = Supplier::where('name', 'like', '%'.$term.'%')->get();
        $return_array = array();
        foreach($suppliers as $supplier) {
            $return_array[] = array('value' => $supplier->name , 'data' =>$supplier );
        }
        return Response::json($return_array);
	}

	public function index()
	{
		$current_user_role = $this->checkRole();
		if($current_user_role == 'notLogin'){
			return redirect()->route('login')->with('success','you dont have authorization for access this page');
		}
		$products = array();
		$products = CustomProduct::with('storage.supplier')->paginate(15);
		$totalProducts = CustomProduct::count();
		$totalPage = (int)ceil($totalProducts / 15);
		return view('storage::index',['products' => $products, 'current_user_role' => $current_user_role, 'totalPage' => $totalPage]);
	}

	public function create(){
		$supplier = Supplier::all();

		if(!$supplier->isEmpty()){
			return view('storage::storage.create',['supplier' => $supplier]);
		}else{
			return redirect()->route('storage.addSupplier')->with('success','you need to add supplier first');
		}
	}

	public function store(Request $request){
		$data = $request->all();
		$storage = CustomProduct::saveProduct(null,$data);
		return redirect()->route('storage.index')->with('success','success save product');
	}

	public function edit($id){

		$product = CustomProduct::with('storage.supplier')->find($id);
		//dd($product);
		$supplier = Supplier::all();
		return view('storage::storage.edit',['product' => $product, 'supplier' => $supplier]);
	}

	public function update($id, Request $request){
		$data = $request->all();
		$storage = CustomProduct::saveProduct($id,$data);
		return redirect()->route('storage.index')->with('success','success edit product');
	}

	public function destroy($id){

	}

	public function searchData(Request $request){
		$data = $request->all();
		//dd($data);
		$products = array();
		$products = CustomProduct::where('name','like','%'.$data['search'].'%')->paginate(15);
		$totalProducts = CustomProduct::count();
		$totalPage = (int)ceil($totalProducts / 15);
		if($products->count() > 0){
			return view('storage::index',['products' => $products, 'totalPage' => $totalPage]);
		}else{
			$products = CustomProduct::with('storage.supplier')->paginate(15);
			return view('storage::index',['products' => $products, 'totalPage' => $totalPage]);
		}
	}

	public function pagination($currentPage){
		$x = 1;
		$i = 1;
		$z = 15;
		$a = 0;
		$products = CustomProduct::with('storage.supplier')->get();

		foreach($products as $product){
			if($x == $currentPage){
				$productDatas[$a] = $product;
				$a++;
			}

			if($i == $z){
				$x++;
				$z = $z + 15;
			}
			$i++;
		}
		//dd($productDatas);
		$totalProducts = CustomProduct::count();
		$totalPage = (int)ceil($totalProducts / 15);

		return view('storage::index',['products' => $productDatas,'totalPage' => $totalPage,'currentPage' => $currentPage]);
	}

	public function importCustomProducts(){
		return view('storage::storage.uploadProduct');
	}

	public function uploadCustomProducts(Request $request){
		$data = $request->all();
		if(isset($data['csv_file'])){
			$handle = fopen($data['csv_file'], 'r');
		}
		$csvArray = array();
		if(isset($handle)){
			$x = 0;
			while (($csv = fgetcsv($handle, 1000, ",")) !== FALSE) {
				if($x > 0 ){
					if(!empty($csv[3])){
						$product = CustomProduct::where('name','=',$csv[1])->first();
						if(!isset($product)){
							$product = new CustomProduct();
							$product->name = $csv[1];
							$product->save();
						}
						
						$checkSupplier = Supplier::where('name','=',$csv[3])->first();
						if(isset($checkSupplier) && $checkSupplier->count() > 0){
							$storage = new Storage();
							$storage->product_id = $product->id;
							$storage->supplier_id = $checkSupplier->id;
							$storage->sku = $csv[0];
							$storage->buying_price = $csv[5];
							$storage->price = $csv[6];
							$storage->qty = $csv[9];
							$storage->save();
							unset($checkSupplier);
						}else{
							$storage = new Storage();
							$storage->product_id = $product->id;
							$storage->supplier_id = 1;
							$storage->sku = $csv[0];
							$storage->qty = $csv[9];
							$storage->buying_price = $csv[5];
							$storage->price = $csv[6];
							$storage->save();
						}
					}
				}
			$x++;
			}
			
		}
	return redirect()->route('dashboard');
	}

	
	public function getCustomProducts(Request $request){
		$term = $request['term'];
        $products = CustomProduct::with('storage.supplier')->where('name', 'like', '%'.$term.'%')->get();
        $return_array = array();
        foreach($products as $product) {
            $return_array[] = array('value' => $product->name, 'data' =>$product);
        }
        return Response::json($return_array);
	}

	public function searchProductSupplier($storage_id,$product_id){
		$product = CustomProduct::with(array('storage' => function($query) use ($storage_id){
			$query->find($storage_id);
		}, 'storage.supplier'))->find($product_id);
		return $product;
	}

	public function destroyCustomProduct($id){
		$product = CustomProduct::find($id);
		$product->delete();
		return redirect()->route('storage.index')->with('success','success delete product');
	}

	public function transactions($id){
		$product = CustomProduct::with('storage.supplier')->find($id);
		return view('storage::storage.purchasing_selling',['product'=>$product]);
	}

	public function updateTransactions($id,Request $request){
		$data = $request->all();
		$current_user = \Sentinel::getUser();
		$current_user_role = $this->checkRole();
		/*if($current_user_role == 'customer'){
			$data['qty'] = $data['qty'] * -1;
		}*/
		$product = CustomProduct::with(array('storage' => function ($query) use($data){
			$query->where('supplier_id','=',$data['supplier_id']);
		},'storage.supplier'))->find($id);
		//dd($product);
		if($data['qty'] < 0){ 
			$qty = $data['qty'] * -1;
			if($product->storage[0]->qty < $qty){
				return redirect()->route('storage.index')->with('success','Insufficient stock');
			} 
		}
		
		$totalValue = $data['qty'] * $product->storage[0]->price;
		$newQty = $data['qty'] + $product->storage[0]->qty;
		
		$product->storage[0]->qty = $newQty;
		$product->storage[0]->save();
		if($data['qty'] != 0){
			$accountantTransaction = new AccountantTransaction();
			if($data['qty'] > 0){
				$accountantTransaction->name = 'pembelian '.$product->name;
				$account = Accountant::where('name','=','pembelian')->first();
			}else{
				$accountantTransaction->name = 'penjualan '.$product->name;
				$account = Accountant::where('name','=','penjualan')->first();
			}
			$accountantTransaction->account_id = $account->id;
			$accountantTransaction->value = $totalValue;
			$accountantTransaction->save();

			/*if($current_user_role == 'customer'){
				$invoice = new Invoice();
				$invoice->customer_id = $current_user->id;
				$invoice->transaction_id = $accountantTransaction->id;
				$invoice->status = Invoice::WAIT;
				$invoice->save();
			}*/

			$kas = Accountant::where('name','=','kas')->first();
			$kasTransaction = new AccountantTransaction();
			if($data['qty'] > 0){
				$kasTransaction->name = 'kurangi kas';
			}else{
				$kasTransaction->name = 'tambah kas';
			}
			$kasTransaction->account_id = $kas->id;
			$kasTransaction->value = $totalValue * -1;
			$kasTransaction->save();

			$storageLog = new StorageLog();
			$storageLog->storage_id = $product->storage[0]->id;
			//$storageLog->transaction_id = $accountantTransaction->id;
			$storageLog->author_id = $current_user->id;
			$storageLog->value = $data['qty'];
			$storageLog->save();
		}


		return redirect()->route('storage.index')->with('success',$accountantTransaction->name);

	}

	public function supplierIndex(){
		$suppliers = Supplier::all();
		return view('storage::supplier.index',['suppliers'=>$suppliers]);
	}

	public function addSupplier(){
		return view('storage::supplier.create');
	}

	public function storeSupplier(Request $request){
		$data = $request->all();
		$supplier = Supplier::saveSupplier(null,$data);
		return redirect()->route('storage.supplier')->with('success','Successfully Add New Supplier');
	}

	public function editSupplier($id){
		$supplier = Supplier::find($id);
		return view('storage::supplier.edit',['supplier' => $supplier]);
	}

	public function updateSupplier($id, Request $request){
		$data = $request->all();
		$supplier = Supplier::saveSupplier($id,$data);
		return redirect()->route('storage.supplier')->with('success','Succesfully Edit Supplier');
	}

	public function destroySupplier($id){
		$supplier = Supplier::find($id);
		$supplier->delete();
		return redirect()->route('storage.supplier')->with('success','Succesfully Delete Supplier');
	}

	public function importSupplier(){
		return view('storage::supplier.uploadSupplier');
	}

	public function uploadSupplier(Request $request){
		$data = $request->all();
		if(isset($data['csv_file'])){
			$handle = fopen($data['csv_file'], 'r');
		}
		$csvArray = array();
		if(isset($handle)){
			$x = 0;
			while (($csv = fgetcsv($handle, 1000, ",")) !== FALSE) {
				if($x > 0 ){
					$supplier = new Supplier();
					$supplier->name = $csv[1];
					$supplier->address = $csv[2];
					$supplier->phone = $csv[4];
					$supplier->hutang = $csv[7];
					$supplier->save();
				}
				$x++;
			}
		}

		return redirect()->route('dashboard');

	}

	public function checkRole(){
		$user = \Sentinel::getUser();
		if($user){
			$current_user['superadmin'] = \Sentinel::inRole('superadmin');
			$current_user['admin'] = \Sentinel::inRole('admin');
			$current_user['customer'] = \Sentinel::inRole('customer');
			
			if($current_user['superadmin'] == true){
				return 'superadmin';
			}elseif($current_user['admin'] == true){
				return 'admin';
			}elseif($current_user['customer'] == true){
				return 'customer';
			}
		}else{
			return 'notLogin';
		}
	}

}
