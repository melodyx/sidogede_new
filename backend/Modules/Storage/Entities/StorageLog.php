<?php

namespace Modules\Storage\Entities;

use Illuminate\Database\Eloquent\Model;

class StorageLog extends Model
{
    protected $fillable = [];
    protected $table = 'storage_log';


    public function storage(){
    	return $this->belongsTo('\Modules\Storage\Entities\Storage','storage_id');
    }

    // public function transaction(){
    // 	return $this->belongsTo('\Modules\Accountants\Entities\AccountantTransaction','transaction_id');
    // }

    public function author(){
    	 return $this->belongsTo('\Modules\Users\Entities\User','author_id');
    }
}
