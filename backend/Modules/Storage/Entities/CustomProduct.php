<?php

namespace Modules\Storage\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Storage\Entities\Storage;
use Modules\Storage\Entities\StorageLog;
use Modules\Accountants\Entities\Accountant;
use Modules\Accountants\Entities\AccountantTransaction;
use Modules\Storage\Entities\Supplier;

class CustomProduct extends Model
{
    protected $fillable = [];
    protected $table = 'custom_products';

    public static function saveProduct($id,$data){
    	\DB::beginTransaction();
    	try {
    		$currentUser = \Sentinel::getUser();
    		unset($data['supplier']['x']);
    		//dd($data);
	    	if(is_null($id)){
	    		$product = new CustomProduct();
	    		$product->name = $data['name'];
		    	$product->description = $data['description'];
		    	$product->short_description = $data['short_description'];
		    	$product->author_id = $currentUser->id;
		    	$product->save();

		    	foreach($data['supplier'] as $supplier){
		    		$supplierData = Supplier::find($supplier['supplier_id']);
		    		$productStorage = new Storage();

		    		//ADD ACCOUNT
		    		/*$account = new Accountant();
		    		$account->name = $product->name.' '.$supplierData->name;
		    		$account->code = $supplier['code'];
		    		$account->save();*/


		    		$productStorage->product_id = $product->id;
		    		$productStorage->supplier_id = $supplier['supplier_id'];
		    		$productStorage->sku = $supplier['code'];
		    		$productStorage->buying_price = $supplier['buying_price'];
		    		$productStorage->price = $supplier['price'];
		    		$productStorage->qty = $supplier['qty'];
		    		$productStorage->author_id = $currentUser->id;
		    		$productStorage->save();

		    		// ADD TO TRANSACTION JOURNAL
		    		/*$kas = Accountant::where('name','=','kas')->first();
		    		if($supplier['qty'] > 0){
		    			$totalValue = $supplier['qty'] * $supplier['price'];
		    			$accountantTransaction = new AccountantTransaction();
		    			$accountantTransaction->name = 'tambah '.$product->name;
		    			$accountantTransaction->account_id = $account->id;
		    			$accountantTransaction->value = $totalValue;
		    			$accountantTransaction->save();

		    			$kasTransaction = new AccountantTransaction();
	    				$kasTransaction->name = 'kurangi kas';
	    				$kasTransaction->account_id = $kas->id;
	    				$kasTransaction->value = -$totalValue;
	    				$kasTransaction->save();
		    		}
					*/
		    		// STORAGE LOG
		    		$storageLog = new StorageLog();
		    		$storageLog->storage_id = $productStorage->id;
		    		$storageLog->author_id = $currentUser->id;
		    		/*if(isset($accountantTransaction)){
		    			$storageLog->transaction_id = $accountantTransaction->id;
		    		}*/
		    		$storageLog->value = $supplier['qty'];
		    		$storageLog->save();
		    	}
	    	}else{
	    		$product = CustomProduct::find($id);
	    		$product->name = $data['name'];
		    	$product->description = $data['description'];
		    	$product->short_description = $data['short_description'];
		    	$product->author_id = $currentUser->id;
		    	$product->save();

		    	foreach($data['supplier'] as $supplier){
		    		$supplierData = Supplier::find($supplier['supplier_id']);
		    		if(isset($supplier['storage_id'])){
		    			$productStorage = Storage::find($supplier['storage_id']);

		    			// STORAGE LOG
		    			if($productStorage->qty != $supplier['qty']){
		    				$newStock = $supplier['qty'] - $productStorage->qty;
		    				$storageLog = new StorageLog();
		    				$storageLog->storage_id = $productStorage->id;
				    		$storageLog->author_id = $currentUser->id;
				    		$storageLog->value = $newStock;
				    		

				    		//ADD TO TRANSACTION JOURNAL
			    			/*$kas = Accountant::where('name','=','kas')->first();
			    			$totalValue = $newStock * $supplier['price'];

			    			$accountantTransaction = new AccountantTransaction();
			    			if($productStorage->qty < $supplier['qty']){
			    				$accountantTransaction->name = 'tambah '.$product->name;
			    				$kasTransaction = new AccountantTransaction();
			    				$kasTransaction->name = 'kurangi kas';
			    				$kasTransaction->account_id = $kas->id;
			    				$kasTransaction->value = -$totalValue;
			    				$kasTransaction->save();

			    			}else{
			    				$accountantTransaction->name = 'kurangi '.$product->name;
			    				$kasTransaction = new AccountantTransaction();
			    				$kasTransaction->name = 'tambah kas';
			    				$kasTransaction->account_id = $kas->id;
			    				$kasTransaction->value = -$totalValue;
			    				$kasTransaction->save();
			    			}

			    			$accountantTransaction->account_id = $productStorage->account->id;
			    			$accountantTransaction->value = $totalValue;
			    			$accountantTransaction->save();

			    			$storageLog->transaction_id = $accountantTransaction->id;*/
			    			$storageLog->save();
				    		
		    			}

		    			$productStorage->buying_price = $supplier['buying_price'];
			    		$productStorage->price = $supplier['price'];
			    		$productStorage->qty = $supplier['qty'];
			    		$productStorage->sku = $supplier['code'];
			    		$productStorage->save();

			    		/*$productStorage->account->code = $supplier['code'];
			    		$productStorage->account->save();*/

		    			
		    			

		    		}else{
		    			$productStorage = new Storage();

		    			//ADD ACCOUNT
			    		/*$account = new Accountant();
			    		$account->name = $product->name.' '.$supplierData->name;
			    		$account->code = $supplier['code'];
			    		$account->save();*/

			    		//ADD STORAGE
		    			$productStorage->product_id = $product->id;
		    			$productStorage->author_id = $currentUser->id;
		    			$productStorage->sku = $supplier['code'];
		    			$productStorage->supplier_id = $supplier['supplier_id'];
			    		$productStorage->price = $supplier['price'];
			    		$productStorage->qty = $supplier['qty'];
			    		$productStorage->save();

			    		// ADD TO TRANSACTION JOURNAL
			    		/*if($supplier['qty'] > 0){
			    			$totalValue = $supplier['qty'] * $supplier['price'];
			    			$accountantTransaction = new AccountantTransaction();
			    			$accountantTransaction->name = 'tambah '.$product->name;
			    			$accountantTransaction->account_id = $account->id;
			    			$accountantTransaction->value = $totalValue;
			    			$accountantTransaction->save();
			    		}*/

		    			// STORAGE LOG
		    			$storageLog = new StorageLog();
			    		$storageLog->storage_id = $productStorage->id;
			    		$storageLog->author_id = $currentUser->id;
			    		//$storageLog->transaction_id = $accountantTransaction->id;
			    		$storageLog->value = $supplier['qty'];
			    		$storageLog->save();
		    		}
		    		
		    	}
	    	}
    	} catch (\Exception $e) {
    		\DB::rollback();
    		dd($e->getMessage());
    	}
    	\DB::commit();

    	// update activity log
		$current_user = \Sentinel::getUser();
		if(is_null($id)){
			$log = array(
				'desc' => 'Adding new product in storage with custom product id: '.$product->id.' '
			);
    	}else{
    		$log = array(
				'desc' => ''
			);
    	}
		event(new \App\Events\UpdateData($current_user, $log));

    	$products['product'] = $product;
    	$products['storage'] = $productStorage;
    	return $products;
    }

    public function storage(){
    	return $this->hasMany('\Modules\Storage\Entities\Storage','product_id');
    }
}
