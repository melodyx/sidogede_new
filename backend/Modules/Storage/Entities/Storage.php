<?php

namespace Modules\Storage\Entities;

use Illuminate\Database\Eloquent\Model;

class Storage extends Model
{
    protected $fillable = [];
    protected $table = 'storage';

    

    public function customProduct(){
    	return $this->belongsTo('\Modules\Storage\Entities\CustomProduct','product_id');
    }

    public function supplier(){
    	return $this->belongsTo('\Modules\Storage\Entities\Supplier','supplier_id');
    }

    public function transactionDetails(){
        return $this->hasMany('\Modules\Transactions\Entities\TransactionDetail','storage_id');
    }

    public function storageLog(){
    	return $this->hasMany('\Modules\Storage\Entities\StorageLog','storage_id');
    }

    public function author(){
        return $this->belongsTo('\Modules\Users\Entities\User','author_id');
    }

    // public function account(){
    //     return $this->belongsto('\Modules\Accountants\Entities\Accountant','account_id');
    // }

}
