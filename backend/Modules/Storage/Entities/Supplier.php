<?php

namespace Modules\Storage\Entities;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $fillable = [];
    protected $table = 'supplier';

    public function storage(){
    	return $this->hasMany('\Modules\Storage\Entities\Storage','supplier_id');
    }

    public function payable(){
        return $this->hasMany('\Modules\Debts\Entities\Payable','supplier_id');
    }

    public static function saveSupplier($id,$data){
    	\DB::beginTransaction();
    	try {
    		if(is_null($id)){
    			$supplier = new Supplier();
	    	}else{
	    		$supplier = Supplier::find($id);
	    	}

	    	$supplier->name = $data['name'];
	    	$supplier->address = $data['address'];
	    	$supplier->phone = $data['phone'];
	    	$supplier->email = $data['email'];
	    	$supplier->save();
    	} catch (\Exception $e) {
    		\DB::rollback();
    		dd($e->getMessage());
    	}
    	\DB::commit();

		// update activity log
		$current_user = \Sentinel::getUser();
		if(is_null($id)){
			$log = array(
				'desc' => 'Adding new supplier with supplier id: '.$supplier->id.' '
			);
    	}else{
    		$log = array(
				'desc' => 'Editing supplier with supplier id: '.$supplier->id.' '
			);
    	}
		event(new \App\Events\UpdateData($current_user, $log));

    	return $supplier;
    }
}
