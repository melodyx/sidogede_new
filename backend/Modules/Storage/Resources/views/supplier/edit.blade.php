@extends('layouts.app')

@section('header')

    @parent
 <style>
 	.clear{clear:both}
 </style>
@endsection

@section('content')
<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i>Supplier</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="{{route('storage.supplier')}}">Supplier</a></li>
							<li class="active">edit</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<div class="content">
				<div class="panel panel-flat">
				<div class="panel-body">
					@include('storage::supplier._form', ['action' => array('\Modules\Storage\Http\Controllers\StorageController@updateSupplier', $supplier->id), 'method' => 'PUT'])
				</div>
				</div>
				</div>
@endsection