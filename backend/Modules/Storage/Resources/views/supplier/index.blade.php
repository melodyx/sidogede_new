@extends('layouts.app')
@section('header')

    @parent
	
	<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
	<?php session()->forget('success'); } ?>
	
	<script>
	jQuery(document).ready(function($){
		$('table.product').on('click','a.remove',function(e){
			// var rewardId = $(this).data('id');
			// $.ajax({
			// 	method: 'get',
			// 	url: '<?php echo route('dashboard'); ?>/eventpoints/eventrewarddestroy/'+rewardId
			// });
			var supplierId = $(this).data('id');
			var confirmation = confirm('Apakah anda yakin ingin melakukan penghapusan produk ini ?');
			if(confirmation == true){
				window.location.replace("{{route('dashboard')}}/storage/destroySupplier/"+supplierId);
			}
		});
	});
	</script>
@endsection
@section('content')
				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i>Suppliers</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Supplier</li>
						</ul>

					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<div class="panel panel-flat">
					<table class="table datatable-basic product">
					<thead>
						<tr>
							<th style="text-align:center;">Id</th>
							<th style="text-align:center;">Supplier Name</th>
							<th style="text-align:center;">Supplier Address</th>
							<th style="text-align:center;">Phone</th>
							<th style="text-align:center;">Email</th>
							<th style="text-align:center;">Action</th>
						</tr>
					</thead>
					<tbody>	
						<?php if(isset($suppliers)){ ?>
							<?php foreach($suppliers as $supplier){?>
							<tr>
								<td style="text-align:center;"><?php if($supplier->id){echo $supplier->id;}else{ echo '-';  }?></td>
								<td style="text-align:center;"><?php if($supplier->name){echo $supplier->name;}else{ echo '-';  }?></td>
								<td style="text-align:center;"><?php if($supplier->address){echo $supplier->address;}else{ echo '-';  }?></td>
								<td style="text-align:center;"><?php if($supplier->phone){echo $supplier->phone;}else{ echo '-';  }?></td>
								<td style="text-align:center;"><?php if($supplier->email){echo $supplier->email;}else{ echo '-';  }?></td>
								<td>
									<ul class="icons-list">
										<li >
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-cog7"></i>
												<span class="caret"></span>
											</a>
											<ul class="dropdown-menu dropdown-menu-right">
												<li class="dropdown-header">Options</li>
												<li><a href="{{route('storage.editSupplier',$supplier->id)}}"><i class="icon-pencil7"></i>Edit entry</a></li>
												<li><a class="remove" data-id="<?php echo $supplier->id?>"><i class="icon-pencil7"></i>Delete</a></li>
											</ul>
										</li>
									</ul>
								</td>
							</tr>
							<?php } ?>
						<?php } ?>
					</tbody>
					</table>	
					</div>
				</div>

@endsection