@extends('layouts.app')

@section('header')
    @parent
    <!-- JS Extra for form -->
 
	<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
	<?php session()->forget('success'); } ?>


 <style>
 	.clear{clear:both}

 </style>
@endsection
@section('content')
<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Suppliers</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="{{route('storage.supplier')}}">Suppliers</a></li>
							<li class="active">Create</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<div class="content">
				<div class="panel panel-flat">
				<div class="panel-body">
					@include('storage::supplier._form', ['action' => '\Modules\Storage\Http\Controllers\StorageController@storeSupplier'])
				</div>
				</div>
				</div>
@endsection