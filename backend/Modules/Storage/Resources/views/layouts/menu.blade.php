<?php $user = \Sentinel::getUser(); ?>

<?php if($user->hasAccess(['storage.product'])){ ?>
<li><a><i class="icon-pencil3"></i>Products</a>
	<ul>
		<li><a href="{{route('storage.index')}}">Products List</a></li>
		<li><a href="{{route('storage.create')}}">Add Product</a></li>
	</ul>
</li>
<?php }else{ ?>
<li><a href="{{route('storage.index')}}"><i class="icon-pencil3"></i>Product List</a></li>
<?php } ?>
<?php if($user->hasAccess(['storage.supplier'])){ ?>
<li><a><i class="icon-pencil3"></i>Supplier</a>
	<ul>
		<li><a href="{{route('storage.supplier')}}">Supplier List</a></li>
		<li><a href="{{route('storage.addSupplier')}}">Add Supplier</a></li>
	</ul>
</li>
<?php } ?>


