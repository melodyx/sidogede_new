@extends('layouts.app')

@section('header')

    @parent
    <!-- JS Extra for form -->
 <script>
 	jQuery(document).ready(function($){
 		$('html').on('click','a.addMoreSupplier',function(e){
 			var x = $('.supplier .form-group').size();
 			x = x - 1;
 			
 			var html = '<div class="form-group">'+$('.supplier .form-group').html()+'</div>';
 			var re = new RegExp("[x]", 'g');
			htmlModif = html.replace(/\[x\]/g, '['+x+']');
 			

 			$('.supplier').append(htmlModif);
 		});
 	});
 </script>

 <style>
 	.clear{clear:both}

 </style>
@endsection

@section('content')
<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Datatables</span> - Products</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="{{route('storage.index')}}">Product</a></li>
							<li class="active">edit</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<div class="content">
				<div class="panel panel-flat">
				<div class="panel-body">
					@include('storage::storage._form', ['action' => array('\Modules\Storage\Http\Controllers\StorageController@update', $product->id), 'method' => 'PUT'])
				</div>
				</div>
				</div>
@endsection