@extends('layouts.app')
@section('header')

    @parent
	
	<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
	<?php session()->forget('success'); } ?>
	
	<script>
	jQuery(document).ready(function($){
		$('table.product').on('click','a.remove',function(e){
			// var rewardId = $(this).data('id');
			// $.ajax({
			// 	method: 'get',
			// 	url: '<?php echo route('dashboard'); ?>/eventpoints/eventrewarddestroy/'+rewardId
			// });
			var productId = $(this).data('id');
			var confirmation = confirm('Apakah anda yakin ingin melakukan penghapusan produk ini ?');
			if(confirmation == true){
				window.location.replace("{{route('dashboard')}}/storage/destroyCustomProduct/"+productId);
			}
		});

		$('html').on('focusout','input.qty',function(){
			if($(this).val() < 0 ){
				$(this).val(0);
			}
		});
	});
	</script>
	<style>
		.clear{clear:both;}
	</style>
@endsection
@section('content')
				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i>Product</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Purchasing & Selling</li>
						</ul>

					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<div class="panel panel-flat">
						<div class="panel-body">
							{{ Form::open(array('method' => 'put', 'action' => array('\Modules\Storage\Http\Controllers\StorageController@updateTransactions',$product->id), 'class' => 'form-horizontal')) }}
								{{ Form::hidden('product_id',$product->id,array('class'=>'form_control'))}}
								<div class="form-group">
									<label class="control-label col-lg-2">Supplier :</label>
									<div class="col-lg-10">
										<?php foreach($product->storage as $storage){
										$supplier[$storage->id] = $storage->supplier->name;
										}?>
										{{ Form::select('supplier_id',$supplier,null,array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="clear"></div>
								<div class="form-group">
									<label class="control-label col-lg-2">Total Purchasing / selling:</label>
									<div class="col-lg-10">
										<?php 
										$option['class'] = 'form-control';
										if(Sentinel::inRole('customer')){
											$option['class'] = 'form-control qty';
											$option['min'] = '0';
										}
										?>
										{{ Form::number('qty',null,$option)}}
									</div>
								</div>
								<div class="clear"></div>
								<div class="form-group">
									<input type="submit" class="btn btn-primary">
								</div>
							{{Form::close()}}
						</div>
					</div>
				</div>

@endsection