<?php
	use \Illuminate\Support\Facades\Input;
	if(!isset($method)){
		$method = 'post';
	}

?>
<script type="text/javascript" src="<?php echo asset('assets/js/plugins/forms/styling/uniform.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo asset('assets/js/pages/form_layouts.js') ?>"></script>


{{ Form::open(array('action' => $action, 'method' => $method, 'class' => 'form-horizontal')) }}
<fieldset class="content-group">
							<legend class="text-bold"><?php if(isset($product)){ echo 'Edit product'; }else{ echo 'Create product';}?></legend>
								<div class="form-group">
									<label class="control-label col-lg-2">Product name</label>
									<div class="col-lg-10">
										{{Form::text('name', isset($product->name)?$product->name:null, array('class' => 'form-control','required' => 'required'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Description</label>
									<div class="col-lg-10">
										{{Form::textarea('description', isset($product->description)?$product->description:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Short Description</label>
									<div class="col-lg-10">
										{{Form::text('short_description', isset($product->short_description)?$product->short_description:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<?php /*<div class="supplier">
									<div class="form-group">
										{{Form::hidden('supplier[0][supplier_id]',isset($supplier[0]->id)?$supplier[0]->id:null, array('class' => 'form-control',))}}
										<label class="control-label col-lg-2" style="margin-bottom:25px;">Price</label>
										<div class="col-lg-10">
											{{Form::number('supplier[0][price]', isset($storage->price)?$storage->price:null, array('class' => 'form-control','required' => 'required','min'=>0))}}
										</div>
										<div class="clear"></div>
										<label class="control-label col-lg-2" style="margin-bottom:25px;">Quantity</label>
										<div class="col-lg-10">
											{{Form::number('supplier[0][qty]', isset($storage->qty)?$storage->qty:null, array('class' => 'form-control','required' => 'required','min' => 0))}}
										</div>
										<div class="clear"></div>
									</div>
								</div> */?>
								<div class="supplier">
									<div class="form-group" style="display:none;">
										<legend>Supplier</legend>
										<label class="control-label col-lg-2" style="margin-bottom:25px;">Supplier Name</label>
										<div class="col-lg-10">
											<?php foreach($supplier as $supply){
												$row[$supply->id] = $supply->name;
											}?>
											{{Form::select('supplier[x][supplier_id]', $row, isset($supplierRow->supplier_id)?$supplierRow->supplier_id:null, array('class' => 'form-control'))}}
										</div>
										<div class="clear"></div>
										<label class="control-label col-lg-2" style="margin-bottom:25px;">Base Price</label>
										<div class="col-lg-10">
											{{Form::number('supplier[x][buying_price]',null, array('class' => 'form-control','min'=>0))}}
										</div>
										<div class="clear"></div>
										<label class="control-label col-lg-2" style="margin-bottom:25px;">Price</label>
										<div class="col-lg-10">
											{{Form::number('supplier[x][price]',null, array('class' => 'form-control','min'=>0))}}
										</div>
										<div class="clear"></div>
										<label class="control-label col-lg-2" style="margin-bottom:25px;">Quantity</label>
										<div class="col-lg-10">
											{{Form::number('supplier[x][qty]',null, array('class' => 'form-control','min' => 0))}}
										</div>
										<div class="clear"></div>
										<label class="control-label col-lg-2">SKU</label>
										<div class="col-lg-10">
											{{Form::text('supplier[x][code]',null, array('class' => 'form-control'))}}
										</div>
										<div class="clear"></div>
									</div>
								<?php if(isset($product->storage)){ $x = 0;?>
									<?php foreach($product->storage as $supplierRow){ ?>
										<div class="form-group">
											<input type="hidden" value="<?php echo $supplierRow->id; ?>" name="supplier[<?php echo $x; ?>][storage_id]">
											<legend>Supplier</legend>
											<label class="control-label col-lg-2" style="margin-bottom:25px;">Supplier Name</label>
											<div class="col-lg-10">
												<?php foreach($supplier as $supply){
													$row[$supply->id] = $supply->name;
												}?>
												{{Form::select("supplier[$x][supplier_id]", $row, isset($supplierRow->supplier_id)?$supplierRow->supplier_id:null, array('class' => 'form-control','readonly' => 'readonly'))}}
											</div>
											<div class="clear"></div>
											<label class="control-label col-lg-2" style="margin-bottom:25px;">Base Price</label>
											<div class="col-lg-10">
												{{Form::number("supplier[$x][buying_price]", isset($supplierRow->buying_price)?$supplierRow->buying_price:null, array('class' => 'form-control','required' => 'required','min'=>0))}}
											</div>
											<div class="clear"></div>
											<label class="control-label col-lg-2" style="margin-bottom:25px;">Price</label>
											<div class="col-lg-10">
												{{Form::number("supplier[$x][price]", isset($supplierRow->price)?$supplierRow->price:null, array('class' => 'form-control','required' => 'required','min'=>0))}}
											</div>
											<div class="clear"></div>
											<label class="control-label col-lg-2" style="margin-bottom:25px;">Quantity</label>
											<div class="col-lg-10">
												{{Form::number("supplier[$x][qty]", isset($supplierRow->qty)?$supplierRow->qty:null, array('class' => 'form-control','required' => 'required','min' => 0))}}
											</div>
											<div class="clear"></div>
											<label class="control-label col-lg-2">SKU</label>
											<div class="col-lg-10">
												{{Form::text("supplier[$x][code]",isset($supplierRow->sku)?$supplierRow->sku:null, array('class' => 'form-control'))}}
											</div>
											<div class="clear"></div>
										</div>
									<?php $x++; } ?>
								<?php }else{ ?>
									<div class="form-group">
										<legend>Supplier</legend>
										<label class="control-label col-lg-2" style="margin-bottom:25px;">Supplier Name</label>
										<div class="col-lg-10">
											<?php foreach($supplier as $supply){
												$row[$supply->id] = $supply->name;
											}?>
											{{Form::select('supplier[0][supplier_id]', $row, isset($supplierRow->supplier_id)?$supplierRow->supplier_id:null, array('class' => 'form-control','required' => 'required'))}}
										</div>
										<div class="clear"></div>
										<label class="control-label col-lg-2" style="margin-bottom:25px;">Base Price</label>
										<div class="col-lg-10">
											{{Form::number('supplier[0][buying_price]', isset($supplierRow->buying_price)?$supplierRow->buying_price:null, array('class' => 'form-control','required' => 'required','min'=>0))}}
										</div>
										<div class="clear"></div>
										<label class="control-label col-lg-2" style="margin-bottom:25px;">Price</label>
										<div class="col-lg-10">
											{{Form::number('supplier[0][price]', isset($supplierRow->price)?$supplierRow->price:null, array('class' => 'form-control','required' => 'required','min'=>0))}}
										</div>
										<div class="clear"></div>
										<label class="control-label col-lg-2" style="margin-bottom:25px;">Quantity</label>
										<div class="col-lg-10">
											{{Form::number('supplier[0][qty]', isset($supplierRow->qty)?$supplierRow->qty:null, array('class' => 'form-control','required' => 'required','min' => 0))}}
										</div>
										<div class="clear"></div>
										<label class="control-label col-lg-2">SKU</label>
										<div class="col-lg-10">
											{{Form::text('supplier[0][code]',isset($supplierRow->sku)?$supplierRow->sku:null, array('class' => 'form-control'))}}
										</div>
										<div class="clear"></div>
									</div>
								<?php } ?>
								</div>
								<a class="btn btn-primary addMoreSupplier">Add more supplier</a>
								<div class="text-right">
									<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
								</div>
						</fieldset>
{{ Form::close() }}