@extends('layouts.app')
@section('header')

    @parent
	
	<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
	<?php session()->forget('success'); } ?>
	
	<script>
	jQuery(document).ready(function($){
		$('table.product').on('click','a.remove',function(e){
			// var rewardId = $(this).data('id');
			// $.ajax({
			// 	method: 'get',
			// 	url: '<?php echo route('dashboard'); ?>/eventpoints/eventrewarddestroy/'+rewardId
			// });
			var productId = $(this).data('id');
			var confirmation = confirm('Apakah anda yakin ingin melakukan penghapusan produk ini ?');
			if(confirmation == true){
				window.location.replace("{{route('dashboard')}}/storage/destroyCustomProduct/"+productId);
			}
		});
	});
	</script>

	<style>
		.clear{clear:both;}
		ul.pagination{float:right; display:inline-block; margin-top:30px;}
		.search-section{float:right; margin:20px 20px 20px;}
		.search-section input{margin-left:20px;}
	</style>
@endsection
@section('content')
				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i>Products</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home </a></li>
							<li class="active">Supplier</li>
						</ul>

					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<div class="panel panel-flat">
					<div class="search-section">
						{{Form::open(array('method' => 'post', 'action' => '\Modules\Storage\Http\Controllers\StorageController@searchData'))}}
						<label>Search:</label><input type="text" name="search">
						{{Form::close()}}
						<div class="clear"></div>
					</div>
					<table class="table product">
					<thead>
						<tr>
							<th style="text-align:center;">Product ID</th>
							<th style="text-align:center;">Name</th>
							<th style="text-align:center;">Total Stock</th>
							<th style="text-align:center;">Beli</th>
							<th style="text-align:center;">Jual</th>
							<th style="text-align:center;">Short Description</th>
							<th style="text-align:center;">Action</th>
						</tr>
					</thead>
					<tbody>	
						<?php if(isset($products)){ ?>
							<?php foreach($products as $product){?>
							<tr>
								<td style="text-align:center;"><?php if($product->id){echo $product->id;}else{ echo '-';  }?></td>
								<td style="text-align:center;"><?php if($product->name){echo $product->name;}else{ echo '-';  }?></td>
								<td style="text-align:center;">
								<?php 
									if($product->storage){
										$totalStock = 0;
										foreach($product->storage as $storage){
											$totalStock = $totalStock + $storage->qty;
										}
										echo $totalStock;
									}
								?>
								</td>
								<td style="text-align:center;">
									<?php 
									if($product->storage){
										foreach($product->storage as $storage){
											echo '<p style="display:block; text-align:center;">'.$storage->supplier->name.' : '.$storage->buying_price.'</p>';
										}
									}
									?>
								</td>
								<td style="text-align:center;">
									<?php 
									if($product->storage){
										foreach($product->storage as $storage){
											echo '<p style="display:block; text-align:center;">'.$storage->supplier->name.' : '.$storage->price.'</p>';
										}
									}
									?>
								</td>
								<td style="text-align:center;"><?php if($product->short_description){echo $product->short_description;}else{ echo '-';  }?></td>
								<td>
									<ul class="icons-list">
										<li >
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-cog7"></i>
												<span class="caret"></span>
											</a>
											<ul class="dropdown-menu dropdown-menu-right">
												<li class="dropdown-header">Options</li>
												<?php $user = \Sentinel::getUser(); ?>
												<!-- <li><a href="{{route('storage.transactions',$product->id)}}"><i class="icon-pencil7"></i>Tambahkan / Kurangi</a></li> -->
												<?php if($user->hasAccess(['storage.product'])){?>
													<li><a href="{{route('storage.edit',$product->id)}}"><i class="icon-pencil7"></i>Edit entry</a></li>
													<li><a class="remove" data-id="<?php echo $product->id?>"><i class="icon-pencil7"></i>Delete</a></li>
												<?php } ?>
											</ul>
										</li>
									</ul>
								</td>
							</tr>
							<?php } ?>
						<?php } ?>
					</tbody>
					</table>
					<div class="clear"></div>
					<ul class="pagination">
						<li><a href="{{route('storage.pagination',1)}}"><<</a></li>
						<?php if(!isset($currentPage)){ $currentPage = 1; } $maxPage = $currentPage + 9; for($x = $currentPage; $x <= $maxPage; $x++){ ?>
						<li><a href="{{route('storage.pagination',$x)}}"><?php echo $x; ?></a></li>
						<?php } ?> 
						<li><a href="{{route('storage.pagination',$totalPage)}}">>></a></li>
					</ul>	
					</div>
					<div class="clear"></div>
				</div>

@endsection